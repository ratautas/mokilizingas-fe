export const throttle = (callback: Function) => {
  let isRunning: Boolean = false;
  if (isRunning) return;
  isRunning = true;
  window.requestAnimationFrame(() => {
    callback();
    isRunning = false;
  });
};
