import easing from './easing';

interface IEventEndObject {
  [key: string]: string;
}

interface ICommon {
  tranEv: string;
  animEv: string;
  isMS: boolean;
  isMobile: boolean;
  isIOS: boolean;
  isSafari: boolean;
  isAndroid: boolean;
}

const msie: boolean = navigator.userAgent.indexOf('MSIE') !== -1;
const trident: boolean = !!navigator.userAgent.match(/Trident.*rv\:11\./);
const edge: boolean = navigator.userAgent.indexOf('Edge') !== -1;
const mobRegex: RegExp = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|CriOS/i;

function transitionEvent() {
  const el: HTMLElement = document.createElement('fakeelement');
  const transitions: IEventEndObject = {
    transition: 'transitionend',
    OTransition: 'oTransitionEnd',
    MozTransition: 'transitionend',
    WebkitTransition: 'webkitTransitionEnd',
  };

  let transition = '';

  Object.keys(transitions).forEach((t: any) => {
    if (el.style[t] !== undefined) {
      transition = transitions[t];
    }
  });

  return transition;
}

function animationEvent() {
  const el: HTMLElement = document.createElement('fakeelement');
  const animations: IEventEndObject = {
    animation: 'animationend',
    OAnimation: 'oAnimationEnd',
    MozAnimation: 'animationend',
    WebkitAnimation: 'webkitAnimationEnd',
  };

  let animation = '';

  Object.keys(animations).forEach((a: any) => {
    if (el.style[a] !== undefined) {
      animation = animations[a];
    }
  });

  return animation;
}

export const isAnd = navigator.userAgent.toLowerCase().indexOf('android') > -1;
export const tranEv = transitionEvent();
export const animEv = animationEvent();
export const isMS = msie || trident || edge;
export const isMobile = mobRegex.test(navigator.userAgent);
export const isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !(window as any).MSStream;
export const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
export const isAndroid = navigator.userAgent.toLowerCase().indexOf('android') > -1;
export const unibody = document.documentElement || document.body.parentNode || document.body;

export function scrollTo(where: number, timing: number = 500, element: any = unibody) {
  const header: HTMLElement | null = document.querySelector('[data-header]');
  const newTop: number = where - (header ? header.offsetHeight : 0);
  const currentTop: number = element.scrollTop;
  const change: number = newTop - currentTop;
  const increment: number = timing / 60;

  let currentTime: number = 0;

  const animateScroll = () => {
    currentTime += increment;
    const currentChange: number = change * (currentTime / timing);
    const percent: number = easing.easeInOutQuint(currentChange / change);
    element.scrollTop = currentTop + change * percent;

    if (currentTime < timing) {
      setTimeout(animateScroll, increment);
    } else {
      element.scrollTop = newTop;
    }
  };

  animateScroll();
}
