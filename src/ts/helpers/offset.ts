export const offset = ($el: HTMLElement) => {
  const r = $el.getBoundingClientRect();
  const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return { top: r.top + scrollTop, left: r.left + scrollLeft };
};
