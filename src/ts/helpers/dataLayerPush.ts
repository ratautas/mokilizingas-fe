interface WindowWithDataLayer extends Window {
  dataLayer: any;
}

export default function dataLayerPush(data: object) {
  (window as WindowWithDataLayer).dataLayer.push({ ...data });
}
