import * as GoogleMapsLoader from 'google-maps';
import choicesJs from 'choices.js';

export class PartnersMap {
  curentPosition: any;
  $partnersMap: HTMLElement = document.querySelector('[data-partners-map]');
  $partnersSelect: HTMLInputElement = document.querySelector('[data-partners-select]');
  $partnersLocate: HTMLElement = document.querySelector('[data-partners-locate]');
  partnersMap: google.maps.Map;
  styles: Object[] = [
    {
      featureType: 'all',
      stylers: [
        {
          saturation: 35,
        },
        {
          hue: '#f0f5f9',
        },
      ],
    },
    {
      featureType: 'road',
      stylers: [
        {
          saturation: -70,
        },
      ],
    },
    {
      featureType: 'transit',
      stylers: [
        {
          visibility: 'off',
        },
      ],
    },
    {
      featureType: 'poi',
      stylers: [
        {
          visibility: 'off',
        },
      ],
    },
    {
      featureType: 'water',
      stylers: [
        {
          visibility: 'simplified',
        },
        {
          saturation: -60,
        },
      ],
    },
  ];
  partners = (window as any).partners;
  regions = (window as any).regions;

  constructor() {
    if (this.$partnersMap) this.init();
  }

  private init(): void {
    const gLoader = GoogleMapsLoader;
    gLoader['KEY'] = 'AIzaSyCIqmRyWTM_nDdgcV1N6jdoTn4pWw6fEJo';
    gLoader['VERSION'] = '3.34';
    gLoader['LIBRARIES'] = ['geometry', 'places'];

    const selectChoices: any = new choicesJs(this.$partnersSelect, {
      searchEnabled: false,
      removeItemButton: false,
      shouldSort: false,
      itemSelectText: '',
      choices: this.regions,
    });
    selectChoices.containerOuter.element.addEventListener('keydown', (event: any) => {
      if (event.which === 32) {
        event.preventDefault();
      }
    });

    gLoader.load((google) => {
      this.partnersMap = new google.maps.Map(this.$partnersMap, {
        zoom: 6.75,
        center: { lat: 55.1, lng: 23.85 },
        styles: this.styles,
      });

      this.partnersMap.data.addGeoJson(this.partners);
      this.partnersMap.data.forEach((element) => {
        this.partnersMap.data.setStyle((element) => {
          return {
            icon: {
              path: `M40,20.0909a20,20,0,1,0-40,0C0,31.22,20,52,20,52S40,31.22,40,20.0909ZM10.6863,
                19.697A9.3138,9.3138,0,1,1,20,29.053,9.3944,9.3944,0,0,1,10.6863,19.697Z`,
              fillColor: '#113c6a',
              fillOpacity: 1,
              anchor: new google.maps.Point(0, 0),
              strokeWeight: 0,
              scale: 1,
            },
          };
        });
      });

      this.$partnersLocate.addEventListener('click', () => {
        this.locateClosest();
      });

      this.$partnersSelect.addEventListener('choice', (e: any) => {
        this.centerMap(
          e.detail.choice.customProperties.zoom,
          e.detail.choice.customProperties.lat,
          e.detail.choice.customProperties.lng,
        );
      });
    });
  }

  private locateClosest() {
    const partners = this.partners.features;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((p) => {
        this.curentPosition = p.coords;
        const currentLatLng = new google.maps.LatLng(p.coords['latitude'], p.coords['longitude']);
        let minDistance: number = 1000000;
        // let closestLocation: unknown;
        for (let i = 0; i < partners.length; i += 1) {
          const markerLatLng = new google.maps.LatLng(
            partners[i]['geometry']['coordinates'][1],
            partners[i]['geometry']['coordinates'][0],
          );

          const distance: number =
            google.maps.geometry.spherical.computeDistanceBetween(currentLatLng, markerLatLng) /
            1000;

          if (distance < minDistance) {
            const lat = partners[i]['geometry']['coordinates'][1];
            const lng = partners[i]['geometry']['coordinates'][0];
            minDistance = distance;
            // closestLocation = {
            //   lat,
            //   lng,
            //   distance,
            //   marker: this.partners.features[i],
            // };
            this.centerMap(15, lat, lng);
          }
        }
      });
    }
  }

  private centerMap(zoom: number, lat: number, lng: number) {
    this.partnersMap.panTo(new google.maps.LatLng(lat, lng));
    this.partnersMap.setZoom(zoom);
  }
}
