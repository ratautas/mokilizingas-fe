import * as noUiSlider from 'noUiSlider';
import perfectScrollbar from 'perfect-scrollbar';
import urijs from 'urijs';
import { Former } from '../utilities/former';
import dataLayerPush from '../helpers/dataLayerPush';

interface IObject {
  [key: string]: any;
}

interface IWizardType {
  type: string;
  id: string;
  label: string;
  image: string;
  min: number;
  max: number;
  defaultAmount: number;
  terms: number[];
  radioTerms: number[];
  defaultTerm: number;
}

export class Wizard {
  wizard: IWizardType;
  types: IObject = {};
  amount: number;
  term: number;
  euroSuffix: string = '\xa0\u20AC';
  monthSuffix: string = '\xa0mėn';
  formController: any;
  $form: HTMLFormElement;
  $select: HTMLSelectElement;
  $amount: HTMLInputElement;
  $termInput: HTMLInputElement;
  $$termRadios: NodeListOf<HTMLInputElement>;
  $range: any;
  $rangeMin: HTMLElement;
  $rangeMax: HTMLElement;
  $rangeHandle: HTMLElement;
  $rangeHandleIcon: HTMLElement = document.createElement('i');
  $updateTermRange: any;
  $updateTermRangeMin: HTMLElement;
  $updateTermRangeMax: HTMLElement;
  $updateTermRangeHandle: HTMLElement;
  $updateTermRangeHandleIcon: HTMLElement = document.createElement('i');
  $termRange: any;
  $termRangeMin: HTMLElement;
  $termRangeMax: HTMLElement;
  $termRangeHandle: HTMLElement;
  $termRangeHandleIcon: HTMLElement = document.createElement('i');
  $$termMin: NodeListOf<HTMLElement>;
  $$termMax: NodeListOf<HTMLElement>;
  $$types: NodeListOf<HTMLElement>;
  $activeStep: HTMLElement;
  $resultTrigger: HTMLElement;
  $updateTrigger: HTMLElement;
  $summaryType: HTMLElement;
  $summaryAmount: HTMLElement;
  $summaryTerm: HTMLElement;
  $summaryMonthly: HTMLElement;
  $exampleAmount: HTMLElement;
  $exampleTerm: HTMLElement;
  $exampleYearly: HTMLElement;
  $exampleFee: HTMLElement;
  $exampleBvkkmn: HTMLElement;
  $exampleTotal: HTMLElement;
  $exampleMonthly: HTMLElement;
  $ps: HTMLElement;

  $typeHiddenInput: HTMLInputElement;
  $termHiddenInput: HTMLInputElement;
  $amountHiddenInput: HTMLInputElement;

  rangeSlider: any;
  updateTermRangeSlider: any;
  termRangeSlider: any;

  activeStep: number = 1;

  fee: number = 0;
  ps: perfectScrollbar;

  constructor($w: HTMLElement) {
    if ($w) this.init($w);
  }

  private init($w) {
    this.$form = $w.querySelector('[data-wizard-form]');
    this.$select = $w.querySelector('[data-wizard-select]');
    this.$amount = $w.querySelector('[data-wizard-amount]');
    this.$$termRadios = $w.querySelectorAll('[data-wizard-term-radio]');
    this.$termInput = $w.querySelector('[data-wizard-term-input]');

    this.$range = $w.querySelector('[data-wizard-range]');
    this.$rangeMin = $w.querySelector('[data-wizard-range-min]');
    this.$rangeMax = $w.querySelector('[data-wizard-range-max]');

    this.$termRange = $w.querySelector('[data-wizard-term-range]');
    this.$termRangeMin = $w.querySelector('[data-wizard-term-range-min]');
    this.$termRangeMax = $w.querySelector('[data-wizard-term-range-max]');

    this.$updateTermRange = $w.querySelector('[data-wizard-update-term-range]');
    this.$updateTermRangeMin = $w.querySelector('[data-wizard-update-term-range-min]');
    this.$updateTermRangeMax = $w.querySelector('[data-wizard-update-term-range-max]');

    this.$$termMin = $w.querySelectorAll('[data-wizard-term-min]');
    this.$$termMax = $w.querySelectorAll('[data-wizard-term-max]');
    this.$$types = $w.querySelectorAll('[data-wizard-type]');

    this.$activeStep = $w.querySelector('[data-wizard-step].is-active');
    this.$resultTrigger = $w.querySelector('[data-wizard-result-trigger]');
    this.$updateTrigger = $w.querySelector('[data-wizard-update-trigger]');
    this.$summaryType = $w.querySelector('[data-wizard-summary-type]');
    this.$summaryAmount = $w.querySelector('[data-wizard-summary-amount]');
    this.$summaryTerm = $w.querySelector('[data-wizard-summary-term]');
    this.$summaryMonthly = $w.querySelector('[data-wizard-summary-monthly]');
    this.$exampleAmount = $w.querySelector('[data-wizard-example-amount]');
    this.$exampleTerm = $w.querySelector('[data-wizard-example-term]');
    this.$exampleYearly = $w.querySelector('[data-wizard-example-yearly]');
    this.$exampleFee = $w.querySelector('[data-wizard-example-fee]');
    this.$exampleBvkkmn = $w.querySelector('[data-wizard-example-bvkkmn]');
    this.$exampleTotal = $w.querySelector('[data-wizard-example-total]');
    this.$exampleMonthly = $w.querySelector('[data-wizard-example-monthly]');

    this.$typeHiddenInput = $w.querySelector('[name="type"]');
    this.$termHiddenInput = $w.querySelector('[name="term"]');
    this.$typeHiddenInput = $w.querySelector('[name="amount"]');

    this.$ps = $w.querySelector('[data-wizard-scroll]');

    const currentType: string = this.$select.value;
    const eur = this.euroSuffix;
    const month = this.monthSuffix;
    const rangeStep = 1;

    this.formController = new Former(this.$form as HTMLFormElement, (e: Event) => {
      // console.log('do a regular submit B-)');
      e.preventDefault();
      this.wizardSubmit(e);
    });

    this.$resultTrigger.addEventListener('click', (e: Event) => {
      this.calculateResult();
    });

    this.$updateTrigger.addEventListener('click', (e: Event) => {
      this.updateResult();
    });

    this.$$types.forEach(($type: HTMLElement) => {
      const data: IObject = { ...$type.dataset };
      const name: string = data['wizardType'];
      const radioTerms = data['wizardRadioTerms'] ? data['wizardRadioTerms'] : [];

      Object.keys(data).map(() => {
        return (this.types[name] = {
          type: name,
          id: data['wizardId'],
          image: data['wizardImage'],
          label: data['wizardTypeLabel'],
          min: parseInt(data['wizardMin'], 10),
          max: parseInt(data['wizardMax'], 10),
          defaultAmount: parseInt(data['wizardDefaultAmount'], 10),
          terms: data['wizardTerms']
            .split(',')
            .reduce((acc, term) => (acc.includes(term) ? acc : [...acc, term]), [])
            .map(term => Number(term))
            .sort((a, b) => a - b),
          radioTerms:
            radioTerms.length &&
            radioTerms
              .split(',')
              .reduce((acc, term) => (acc.includes(term) ? acc : [...acc, term]), [])
              .map(term => Number(term))
              .sort((a, b) => a - b),
          defaultTerm: parseInt(data['wizardDefaultTerm'], 10),
        });
      });

      if (name === currentType) {
        this.wizard = this.types[name];
        this.amount = parseInt(data['wizardDefaultAmount'], 10);
        this.$amount.value = String(this.amount);
        if (this.$rangeMin) {
          this.$rangeMin.innerText = parseInt(data['wizardMin'], 10) + this.euroSuffix;
        }
        if (this.$rangeMax) {
          this.$rangeMax.innerText = parseInt(data['wizardMax'], 10) + this.euroSuffix;
        }
      }
    });

    if (this.$range) {
      this.rangeSlider = noUiSlider.create(this.$range, {
        step: rangeStep,
        start: [this.wizard.defaultAmount],
        range: {
          min: this.wizard.min,
          max: this.wizard.max,
        },
        connect: false,
        tooltips: true,
        format: {
          to(val: number) {
            return Math.floor(val) + eur;
          },
          from(val: string) {
            return val.replace(eur, '');
          },
        },
      });

      this.rangeSlider.on('update', (values: any) => {
        const val = values[0].replace(this.euroSuffix, '');
        this.amount = parseInt(val, 10);
        this.$amount.value = val;
        this.$form.fields['wizard-amount-input'].$input.value = val;
      });

      this.$rangeHandle = this.$range.querySelector('.noUi-handle');

      this.$range.querySelector('.noUi-tooltip').classList.add('noUi-tooltip--wizard');

      this.$rangeHandleIcon.classList.add('noUi-handle-icon');

      this.$rangeHandleIcon.style.backgroundImage = `url(${this.wizard.image})`;

      this.$rangeHandle.appendChild(this.$rangeHandleIcon);

      this.$rangeHandle.addEventListener('keydown', (e: any) => {
        const value = Number(this.$range.noUiSlider.get().replace(this.euroSuffix, ''));
        if (e.which === 37 || e.which === 40) {
          e.preventDefault();
          this.$range.noUiSlider.set(value - rangeStep);
        } else if (e.which === 38 || e.which === 39) {
          e.preventDefault();
          this.$range.noUiSlider.set(value + rangeStep);
        }
      });
    }

    if (this.$termRange) {
      this.termRangeSlider = noUiSlider.create(this.$termRange, {
        start: [this.wizard.defaultTerm],
        range: this.calculateTermRange(),
        connect: false,
        tooltips: false,
        format: {
          to(val: number) {
            return Math.floor(val);
          },
          from(val: number) {
            return Math.floor(val);
          },
        },
      });

      this.termRangeSlider.on('update', (values: any) => {
        this.setTerm(values[0], 'termRangeSlider');
      });

      this.$termRangeHandle = this.$termRange.querySelector('.noUi-handle');

      this.$termRangeHandleIcon.classList.add('noUi-handle-icon');

      this.$termRangeHandleIcon.style.backgroundImage = `url(${this.wizard.image})`;

      this.$termRangeHandle.appendChild(this.$termRangeHandleIcon);

      this.$termRangeHandle.addEventListener('keydown', (e: any) => {
        const value = Number(this.$termRange.noUiSlider.get().replace(this.euroSuffix, ''));
        if (e.which === 37 || e.which === 40) {
          e.preventDefault();
          this.$termRange.noUiSlider.set(value - rangeStep);
        } else if (e.which === 38 || e.which === 39) {
          e.preventDefault();
          this.$termRange.noUiSlider.set(value + rangeStep);
        }
      });
    }

    if (this.$updateTermRange) {
      this.updateTermRangeSlider = noUiSlider.create(this.$updateTermRange, {
        start: [this.wizard.defaultTerm],
        range: this.calculateTermRange(),
        connect: false,
        tooltips: false,
        format: {
          to(val: number) {
            return Math.floor(val);
          },
          from(val: number) {
            return Math.floor(val);
          },
        },
      });

      this.updateTermRangeSlider.on('change', (values: any) => {
        this.setTerm(values[0], 'updateTermRangeSlider');
        this.calculateResult();
      });

      this.$updateTermRangeHandle = this.$updateTermRange.querySelector('.noUi-handle');

      this.$updateTermRangeHandle.classList.add('noUi-handle--update-term');
      this.$updateTermRangeHandle.removeAttribute('tabindex');

      this.$updateTermRangeHandleIcon.classList.add(
        'noUi-handle-icon',
        'noUi-handle-icon--update-term',
      );

      this.$updateTermRangeHandleIcon.style.backgroundImage = `url(${this.wizard.image})`;

      this.$updateTermRangeHandle.appendChild(this.$updateTermRangeHandleIcon);

      this.$updateTermRangeHandle.addEventListener('keydown', (e: any) => {
        const value = Number(this.$updateTermRange.noUiSlider.get().replace(this.euroSuffix, ''));
        if (e.which === 37 || e.which === 40) {
          e.preventDefault();
          this.$updateTermRange.noUiSlider.set(value - rangeStep);
        } else if (e.which === 38 || e.which === 39) {
          e.preventDefault();
          this.$updateTermRange.noUiSlider.set(value + rangeStep);
        }
      });
    }

    this.$select.addEventListener('change', (e: CustomEvent) => this.setType(e.detail.value));

    this.$form.querySelectorAll('.choices').forEach($choices => $choices.classList.add('is-blue'));

    this.$form.fields['wizard-amount-input'].$input.addEventListener('keyup', (e: CustomEvent) => {
      const val: number = Number(this.$form.fields['wizard-amount-input'].$input.value);
      if (val >= this.wizard.min && val <= this.wizard.max) {
        this.rangeSlider.set(val);
      }
    });

    this.$form.fields['wizard-amount-input'].$input.value = this.amount;

    this.$summaryType.innerText = String(this.wizard.label);

    if (this.$ps) this.ps = new perfectScrollbar(this.$ps);

    this.$$termRadios.forEach(($radio: HTMLInputElement) => {
      $radio.addEventListener('change', () => {
        this.setTerm($radio.value, '$radio');
        this.$$termRadios.forEach(($r: HTMLInputElement) => {
          $r.classList.remove('has-value');
          $r.parentElement.classList.remove('has-value');
        });
        $radio.classList.add('has-value');
        $radio.parentElement.classList.add('has-value');
      });
    });

    if (this.$termInput) {
      this.$termInput.addEventListener('keyup', (e: any) => {
        this.setTerm(e.target.value, '$termInput');
      });

      this.$termInput.addEventListener('blur', () => {
        this.$termInput.value = this.term.toString();
      });
    }
    this.setRadios();
  }

  private setType(type) {
    this.wizard = this.types[type];
    this.rangeSlider.updateOptions(
      {
        tooltips: false,
        start: [this.wizard.defaultAmount],
        range: {
          min: this.wizard.min,
          max: this.wizard.max,
        },
      },
      true,
    );
    if (typeof this.termRangeSlider !== 'undefined') {
      this.termRangeSlider.updateOptions(
        {
          start: [this.wizard.defaultTerm],
          range: this.calculateTermRange(),
        },
        true,
      );
    }

    this.updateTermRangeSlider.updateOptions(
      {
        start: [this.wizard.defaultTerm],
        range: this.calculateTermRange(),
      },
      true,
    );

    this.setTerm(this.wizard.defaultTerm, 'termRangeSlider');

    this.$form.fields['wizard-amount-input'].$input.value = this.wizard.defaultAmount;

    if (this.$rangeMin) this.$rangeMin.innerText = this.wizard.min + this.euroSuffix;
    if (this.$rangeMax) this.$rangeMax.innerText = this.wizard.max + this.euroSuffix;

    if (this.$rangeHandleIcon) {
      this.$rangeHandleIcon.style.backgroundImage = `url(${this.wizard.image})`;
    }

    if (this.$termRangeHandleIcon) {
      this.$termRangeHandleIcon.style.backgroundImage = `url(${this.wizard.image})`;
    }

    if (this.$updateTermRangeHandleIcon) {
      this.$updateTermRangeHandleIcon.style.backgroundImage = `url(${this.wizard.image})`;
    }

    if (this.$$termMin[0]) {
      [].forEach.call(this.$$termMin, ($min: HTMLElement) => {
        $min.innerText = this.wizard.terms[0].toString();
      });
    }

    if (this.$$termMax[0]) {
      [].forEach.call(this.$$termMax, ($max: HTMLElement) => {
        $max.innerText = this.wizard.terms[this.wizard.terms.length - 1].toString();
      });
    }

    this.setRadios();
  }

  private setTerm(term, emitter) {
    this.term = this.wizard.terms.reduce((prev, curr) => {
      return Math.abs(curr - Number(term)) < Math.abs(prev - Number(term)) ? curr : prev;
    });

    if (this.$termInput && emitter !== '$termInput') {
      this.$termInput.value = this.term.toString();
    }

    if (this.$$termRadios.length) {
      this.$$termRadios.forEach(($radio: HTMLInputElement) => {
        const isTarget: boolean = Boolean(Number($radio.value) === this.term);
        $radio.checked = isTarget;
        isTarget ? $radio.setAttribute('checked', 'checked') : $radio.removeAttribute('checked');
        $radio.classList[isTarget ? 'add' : 'remove']('has-value');
        $radio.parentElement.classList[isTarget ? 'add' : 'remove']('has-value');
      });
    }

    if (
      this.$termRange &&
      typeof this.termRangeSlider !== 'undefined' &&
      emitter !== 'termRangeSlider'
    ) {
      this.termRangeSlider.set(Number(term));
    }

    if (
      this.$updateTermRange &&
      typeof this.updateTermRangeSlider !== 'undefined' &&
      // emitter !== 'termRangeSlider' &&
      emitter !== 'updateTermRangeSlider'
    ) {
      this.updateTermRangeSlider.set(Number(term));
    }
  }

  private setRadios() {
    this.$$termRadios.forEach(($radio: HTMLInputElement) => {
      // const inTerms = Boolean(this.wizard.terms.indexOf(Number($radio.value)) > -1);
      const inTerms = Boolean(this.wizard.radioTerms.indexOf(Number($radio.value)) > -1);
      const isFeatured = Number($radio.value) === this.wizard.defaultTerm;
      $radio.classList[!inTerms ? 'add' : 'remove']('is-disabled');
      $radio.parentElement.classList[!inTerms ? 'add' : 'remove']('is-disabled');
      $radio.classList[isFeatured ? 'add' : 'remove']('is-featured');
      $radio.parentElement.classList[isFeatured ? 'add' : 'remove']('is-featured');
      $radio.checked = isFeatured;
      isFeatured ? $radio.setAttribute('checked', 'checked') : $radio.removeAttribute('checked');
      $radio.classList[isFeatured ? 'add' : 'remove']('has-value');
      $radio.parentElement.classList[isFeatured ? 'add' : 'remove']('has-value');
      if (isFeatured) {
        this.term = Number($radio.value);
        this.$termInput.value = $radio.value;
      }
    });
    if (this.$ps) this.ps.update();
  }

  private calculateTermRange() {
    const terms = [...this.wizard.terms];
    const range = {};
    terms.forEach((term, i) => {
      let label = `${(i / (terms.length - 1)) * 100}%`;
      if (i === 0) label = 'min';
      if (i === terms.length - 1) label = 'max';
      range[label] = [term, terms[i + 1]];
    });

    return range;
  }

  private calculateResult() {
    this.$form.isValid = true;
    this.$form.isDirty = true;
    this.$form.$firstFocus = null;

    if (this.$typeHiddenInput) this.$typeHiddenInput.value = this.wizard.id;
    if (this.$termHiddenInput) this.$termHiddenInput.value = this.term.toString();
    if (this.$amountHiddenInput) this.$amountHiddenInput.value = this.amount.toString();

    Object.keys(this.$form.fields).forEach((name: string) => {
      this.formController.validateField(name);
    });

    if (!this.$form.isValid) {
      this.$form.$firstFocus.focus();
    } else {
      const endpoint = urijs(this.$form.url).query({
        type: this.wizard.id,
        term: this.term.toString(),
        amount: this.amount.toString(),
        redirect: 0,
      });
      const formData: FormData = new FormData();
      formData.append('type', this.wizard.id);
      formData.append('term', this.term.toString());
      formData.append('amount', this.amount.toString());
      fetch(endpoint as any)
        .then((r: Response) => r.text())
        .then((response: any) => {
          if (response === 'error') {
            this.$form.classList.add('has-error');
          } else {
            const responseData = JSON.parse(response);
            this.$activeStep.classList.remove('is-active');
            this.$summaryType.innerText = String(this.wizard.label);
            this.$summaryAmount.innerText = String(this.amount);
            this.$summaryTerm.innerText = String(this.term);
            this.$summaryMonthly.innerText = responseData.installment_avg_amount;
            this.$exampleAmount.innerText = responseData.total_amount;
            this.$exampleTerm.innerText = responseData.term;
            this.$exampleYearly.innerText = responseData.intr_yearly;
            this.$exampleFee.innerText = responseData.tariff_admin_fee;
            this.$exampleBvkkmn.innerText = responseData.apr;
            this.$exampleTotal.innerText = responseData.installment_total_amount;
            this.$exampleMonthly.innerText = responseData.installment_avg_amount;
            this.activeStep = 2;
            this.$activeStep = this.$form.querySelector('[data-wizard-step="2"]');
            this.$activeStep.classList.add('is-active');
            dataLayerPush({
              calculatorName: this.wizard.label,
              calculatorAmount: this.amount || 0,
              calculatorDuration: this.term || 0,
              calculatorPayment: responseData.installment_avg_amount,
              event: 'calculatorInteraction',
            });
          }
        })
        .catch((error: any) => {
          this.$form.classList.add('has-error');
        });
    }
  }

  private updateResult() {
    this.$activeStep.classList.remove('is-active');
    this.activeStep = 1;
    this.$activeStep = this.$form.querySelector('[data-wizard-step="1"]');
    this.$activeStep.classList.add('is-active');
  }

  private wizardSubmit(e: Event) {
    const endpoint = urijs(this.$form.url).query({
      type: this.wizard.id,
      term: this.term.toString(),
      amount: this.amount.toString(),
      redirect: 1,
    });

    fetch(endpoint as any)
      .then((r: Response) => r.text())
      .then((response: any) => {
        if (response === 'error') {
          this.$form.classList.add('has-error');
        } else {
          // this.$form.classList.add('has-success');
          window.location.href = response;
          // window.open(response);
        }
      })
      .catch((error: any) => this.$form.classList.add('has-error'));
  }
}
