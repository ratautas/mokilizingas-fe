import '../scss/index.scss';
import cssVarsPonyfill from 'css-vars-ponyfill';
import * as Stickyfill from 'stickyfilljs';
// import * as elementClosest from 'element-closest';
// import { elementClosest } from 'element-closest';
import * as scrollMonitor from 'scrollmonitor';
import urijs from 'urijs';

import { Cookiebar } from './utilities/cookiebar';
import { Former } from './utilities/former';
import { Offcanvas } from './utilities/offcanvas';
import { Sliders } from './utilities/sliders';
import { Tabs } from './utilities/tabs';
import { Togglers } from './utilities/togglers';
import { TrackScroll } from './utilities/track-scroll';

import { Assoc } from './components/assoc';
import { PartnersMap } from './components/partners-map';
import { Proffer } from './components/proffer';
import { Reviews } from './components/reviews';
import { Faq } from './components/faq';
import { LinkedinToggle } from './components/linkedinToggle';
import { Maps } from './components/maps';
import { Sequence } from './components/sequence';
import { Sitemap } from './components/sitemap';
import { Toast } from './components/toast';
import { Wizard } from './components/wizard';
// import Wizard from '../js/Wizard';

(() => {
  if (typeof (NodeList as any).prototype.forEach !== 'function') {
    (NodeList as any).prototype.forEach = Array.prototype.forEach;
  }
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      (Element.prototype as any)['msMatchesSelector'] || Element.prototype.webkitMatchesSelector;
  }
  return false;
})();

window.addEventListener('load', () => {
  if (document.querySelector('[data-sticky]')) {
    Stickyfill.addOne(document.querySelector('[data-sticky]') as HTMLElement);
  }
  cssVarsPonyfill();

  // elementClosest(window);

  document.querySelectorAll('form').forEach(($form: HTMLFormElement) => {
    if (!$form.matches('.custom-submit')) new Former($form);
    if ($form.matches('[data-user-form]')) {
      new Former($form as HTMLFormElement, (e: Event) => {
        e.preventDefault();
        const query = {};
        Object.keys($form.fields).forEach(
          field => (query[field] = $form.fields[field]['$input'].value),
        );
        const endpoint = urijs($form.url).query({ ...query });

        fetch(endpoint as any, {
          method: 'post',
        })
          .then((r: Response) => r.text())
          .then((response: any) => {
            if (response === 'error') {
              $form.classList.add('has-error');
            } else {
              // $form.classList.add('has-success');
              // window.location.href = response;
              window.open(response);
            }
          })
          .catch((error: any) => $form.classList.add('has-error'));
      });
    }
  });
  if (window.innerWidth > 768) {
    document.querySelectorAll('[data-sequence]').forEach(($sequence: HTMLElement) => {
      $sequence['sequence'] = new Sequence($sequence);
      if (!$sequence.closest('[data-swiper]')) {
        const watcher = scrollMonitor.create($sequence);
        if (watcher.isFullyInViewport) {
          setTimeout(() => {
            $sequence['sequence'].play();
          }, 1000);
        }
        watcher.fullyEnterViewport(() => {
          if ($sequence['sequence'].animation) $sequence['sequence'].play();
        });
      }
    });
  }

  new Cookiebar();
  new Offcanvas();
  new Sliders();
  new Tabs();
  new Togglers();
  new TrackScroll();

  new Assoc();
  new Faq();
  new LinkedinToggle();
  new Maps();
  new Proffer();
  new Reviews();
  new PartnersMap();
  new Sitemap();
  new Toast();

  if (document.querySelector('[data-wizard]')) {
    // document.querySelectorAll('[data-wizard]').forEach($wizard => new Wizard({ target: $wizard }));
    document
      .querySelectorAll('[data-wizard]')
      .forEach(($wizard: HTMLElement) => new Wizard($wizard));
  }

  setTimeout(() => document.body.classList.remove('preload'), 1);
});

const isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !(window as any).MSStream;

document.body.classList.add(isIOS ? 'is-ios' : 'no-ios');
