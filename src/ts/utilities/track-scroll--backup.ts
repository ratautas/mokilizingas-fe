// import { throttle } from '../utilities/throttle';

export class TrackScroll {
  isSwping: any = false;
  touchStarted: any = false;
  pos: number = window.scrollY;
  speed: number = 0;
  lastPos: number = null;
  headerUp: boolean = window.scrollY > 1;
  headerUpTreshold: number = 50;
  goingUp: boolean = false;
  headerNarrow: boolean = false;
  scrollTimer: any;
  startY: number = null;

  constructor() {
    this.init();
  }

  public init(): void {
    window.ontouchstart = (e) => {
      this.isSwping = true;
      this.startY = e.touches[0].clientY;
    };

    window.ontouchend = (e) => {
      this.isSwping = false;
      this.goingUp = e.changedTouches[0].clientY - this.startY > 0;
      // this.toggleHeaderUpDown();
    };

    window.ontouchmove = (e) => {
      if (this.isSwping) {
        this.isSwping = false;
        setTimeout(() => {
          this.goingUp = e.changedTouches[0].clientY - this.startY > 0;
          this.startY = e.changedTouches[0].clientY;
          this.toggleHeaderUpDown();
          this.isSwping = true;
        }, 60);
      }
    };

    window.onscroll = (e) => {
      if (this.lastPos !== null) this.speed = this.pos - this.lastPos;
      this.lastPos = this.pos;
      this.goingUp = this.speed <= 0 || this.pos <= 0;
      this.pos = window.scrollY;
      this.toggleHeaderUpDown();
      if (!this.headerNarrow && this.pos > 1) {
        this.headerNarrow = true;
        window.requestAnimationFrame(() => {
          document.body.classList.add('header-narrow');
        });
      } else if (this.headerNarrow && this.pos <= 1) {
        this.headerNarrow = false;
        window.requestAnimationFrame(() => {
          document.body.classList.remove('header-narrow');
        });
      }
    };
  }
  public toggleHeaderUpDown() {
    if (
      (this.headerUp && !this.goingUp && !this.isSwping) ||
      (this.headerUp && this.pos < this.headerUpTreshold)
    ) {
      this.headerUp = false;
      window.requestAnimationFrame(() => {
        document.body.classList.remove('header-up');
      });
    } else if (
      (!this.headerUp && this.goingUp && !this.isSwping) ||
      (!this.headerUp && this.pos >= this.headerUpTreshold)
    ) {
      this.headerUp = true;
      window.requestAnimationFrame(() => {
        document.body.classList.add('header-up');
      });
    }
  }
}
