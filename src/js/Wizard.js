// Wizard proxy, because TS doesn't understand how to handle .svelte files :(
import Wizard from '../svelte/Wizard';
export default Wizard;
