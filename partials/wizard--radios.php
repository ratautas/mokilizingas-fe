<div class="wizard__wrapper" data-wizard="">
  <form target="_blank" class="wizard__form form custom-submit" data-wizard-form
    action="http://mokilizingas-be.devprojects.lt/endpoint/loan" novalidate="true">
    <div class="form__content wizard__step wizard__step--1 is-active" data-wizard-step="1">
      <div class="wizard__content">
        <div data-wizard-type="1" data-wizard-id="2" data-wizard-type-label="Paskola automobiliui"
          data-wizard-min="500" data-wizard-max="15000"
          data-wizard-terms="12,15,9,6,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,72"
          data-wizard-default-term="12" data-wizard-default-amount="500"></div>
        <div data-wizard-type="2" data-wizard-id="1" data-wizard-type-label="Vartojimo paskola"
          data-wizard-min="300" data-wizard-max="15000"
          data-wizard-terms="24,27,30,33,6,36,39,42,45,48,51,54,57,60,63,66,69,60,63,66,69,36,39,42,45,12,72,48,51,54,57,24,27,30,33,12,3,6,9,72,9,15,18,21,15,18,21,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72"
          data-wizard-default-term="12" data-wizard-default-amount="300"></div>
        <div data-wizard-type="3" data-wizard-id="3" data-wizard-type-label="Paskola būsto remontui"
          data-wizard-min="300" data-wizard-max="15000"
          data-wizard-terms="24,18,21,15,12,9,6,3,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72"
          data-wizard-default-term="12" data-wizard-default-amount="300"></div>
        <div class="form__row">
          <div class="form__item">
            <select name="wizard-type" class="form__select" data-wizard-select data-dropdown>
              <option value="1">Paskola būsto remontui</option>
              <option value="2">Vartojimo paskola</option>
              <option value="3">Paskola automobiliui</option>
            </select>
            <span class="form__error">Error</span>
          </div>
        </div>
        <div class="form__row">
          <div class="form__item">
            <span class="form__label">Įveskite norimą paskolos sumą</span>
            <div class="form__help">
              <span data-wizard-range-min="">500&nbsp;€</span>
              &nbsp;-&nbsp;
              <span data-wizard-range-max="">15000&nbsp;€</span>
            </div>
            <input type="number" inputmode="decimal" pattern="[0-9]*" name="wizard-amount-input"
              required="" class="form__input">
            <span class="form__error"></span>
          </div>
        </div>
        <div class="form__row">
          <div class="form__item">
            <div class="form__range form__range--wizard range">
              <div class="range__target" data-wizard-range></div>
            </div>
            <input type="hidden" name="wizard-amount" data-wizard-amount="" tabindex="-1"
              value="500">
          </div>
        </div>
        <div class="form__row">
          <div class="form__term term">
            <span class="form__label">Pasirinkite grąžinimo terminą</span>
            <label class="term__custom form__item">
              <span class="term__enter">Įrašyti savo terminą</span>
              <span class="term__placeholder">mėn.</span>
              <input type="number" inputmode="decimal" pattern="[0-9]*" name="wizard-term"
                data-wizard-term-input="" class="term__input form__input" min="1">
            </label>
            <div class="term__wrapper ps ps--active-y" data-wizard-scroll="">
              <div class="term__scroller">
                <label class="term__item is-disabled">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native is-disabled" value="3">
                  <span class="term__label">3 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="6">
                  <span class="term__label">6 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="9">
                  <span class="term__label">9 mėn.</span>
                </label>
                <label class="term__item is-featured">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native is-featured" value="12" checked="checked">
                  <span class="term__label">12 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="15">
                  <span class="term__label">15 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="18">
                  <span class="term__label">18 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="21">
                  <span class="term__label">21 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="24">
                  <span class="term__label">24 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="27">
                  <span class="term__label">27 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="30">
                  <span class="term__label">30 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="33">
                  <span class="term__label">33 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="36">
                  <span class="term__label">36 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="39">
                  <span class="term__label">39 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="42">
                  <span class="term__label">42 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="45">
                  <span class="term__label">45 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="48">
                  <span class="term__label">48 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="51">
                  <span class="term__label">51 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="54">
                  <span class="term__label">54 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="57">
                  <span class="term__label">57 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="60">
                  <span class="term__label">60 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="63">
                  <span class="term__label">63 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="66">
                  <span class="term__label">66 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="69">
                  <span class="term__label">69 mėn.</span>
                </label>
                <label class="term__item">
                  <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                    class="term__radio form__native" value="72">
                  <span class="term__label">72 mėn.</span>
                </label>
              </div>
              <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
              </div>
              <div class="ps__rail-y" style="top: 0px; height: 110px; right: 0px;">
                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 15px;"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="wizard__footer">
        <div class="wizard__notify">Pinigus pervesime per 30 min., jei paskolos sutartį
          sudarysite iki 20 val.</div>
        <div class="wizard__btn btn" data-wizard-result-trigger="">
          <span class="btn__text">Skaičiuoti įmoką</span>
          <i class="btn__icon btn__icon--right">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5.1997 8.9849">
              <title>icon--caret</title>
              <polygon
                points="0.707 8.985 0 8.278 3.786 4.493 0 0.707 0.707 0 5.2 4.493 0.707 8.985"
                fill="#fff"></polygon>
            </svg> </i>
        </div>
      </div>
    </div>
    <div class="form__content form__content--no-flip wizard__step wizard__step--2"
      data-wizard-step="2">
      <div class="wizard__content">
        <div class="wizard__summary summary">
          <div class="summary__back" data-wizard-update-trigger="">Keisti</div>
          <div class="summary__title" data-wizard-summary-type="">Paskola automobiliui</div>
          <div class="summary__table">
            <div class="summary__row">
              <div class="summary__col summary__col--label">Paskolos suma:</div>
              <div class="summary__col summary__col--val"><span
                  data-wizard-summary-amount=""></span>&nbsp;€</div>
            </div>
            <div class="summary__row">
              <div class="summary__col summary__col--label">Grąžinimo terminas:</div>
              <div class="summary__col summary__col--val"><span
                  data-wizard-summary-term=""></span>&nbsp;mėn.</div>
            </div>
            <div class="summary__row summary__row--bold">
              <div class="summary__col summary__col--label">Mėnesio įmoka:</div>
              <div class="summary__col summary__col--val"><span
                  data-wizard-summary-monthly=""></span>&nbsp;€</div>
            </div>
          </div>
        </div>
      </div>
      <div class="wizard__footer">
        <div class="wizard__footnote">
          <p>Pavyzdžiui, skolinantis <span data-wizard-example-amount=""></span>&nbsp;€, kai
            sutartis sudaroma <span data-wizard-example-term=""></span> mėn. terminui, metinė
            palūkanų norma – <span data-wizard-example-yearly=""></span>, mėnesio sutarties
            mokestis – <span data-wizard-example-fee=""></span>, BVKKMN – <span
              data-wizard-example-bvkkmn=""></span>, bendra mokėtina suma – <span
              data-wizard-example-total=""></span>, mėnesio įmoka – <span
              data-wizard-example-monthly=""></span>&nbsp;€.</p>

          <p>Atsižvelgiant į lizingo bendrovės atliktą Jūsų kreditingumo ir rizikos vertinimą ar
            pasirinkus kitokį sutarties terminą bei įmokų mokėjimo dieną, Jums gali būti
            pasiūlytos ir kitokios sąlygos (pavyzdžiui, metinės palūkanų normos arba mėnesio
            sutarties mokesčio dydis).</p>
        </div>
        <div class="wizard__notify">Pinigus pervesime per 30 min., jei paskolos sutartį
          sudarysite iki 20 val.</div>
        <button class="wizard__btn btn">
          <span class="btn__text">Gauti paskolą</span>
          <i class="btn__icon btn__icon--right">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5.1997 8.9849">
              <title>icon--caret</title>
              <polygon
                points="0.707 8.985 0 8.278 3.786 4.493 0 0.707 0.707 0 5.2 4.493 0.707 8.985"
                fill="#fff"></polygon>
            </svg> </i>
        </button>
      </div>
    </div>
    <div class="form__done form__done--error done done--error" data-form-done="error">
      <i class="done__close" data-done-close=""></i>
      <div class="done__top">
        <h2>Error</h2>
      </div>
      <div class="done__text">
        <p>Atsiprašome, šiuo metu turime nesklandumų. Bandykite vėliau dar kartą...</p>
      </div>
    </div>
  </form>
</div>