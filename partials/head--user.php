<?php
  $localhost = false;
  if(file_get_contents('http://localhost:5000/assets/js/app.js')){
    $localhost = true;
  }
?>

<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other">

<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
  <title>mokilizingas</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="msapplication-TileImage" content="../assets/img/favicons/ms-icon-144x144.png">
  <link rel="shortcut icon" href="../assets/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="icon" href="../assets/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicons/favicon-16x16.png">
  <link rel="mask-icon" href="../assets/img/favicons/safari-pinned-tab.svg" color="#02a0d3">
  <meta name="msapplication-TileColor" content="#02a0d3">
  <meta name="theme-color" content="#02a0d3">
  <meta name="msapplication-tap-highlight" content="no" />
  <meta name="robots" content="noindex, nofollow">
  <?php if(!$localhost): ?>
  <link rel="stylesheet" href="../assets/css/app.css">
  <?php endif; ?>
</head>

<body class="app app--user preload" data-lang="lt" data-root="http://localhost/mokilizingas-fe">
  <header class="app__header app__header--user header " data-header>
    <?php include 'header--user.php'; ?>
    <div class="header__cookiebar cookiebar" data-cookiebar="../endpoints/cookiebar.php">
      <?php include 'cookiebar.php'; ?>
    </div>
  </header>
  <aside class="app__offcanvas app__offcanvas--resume offcanvas" data-offcanvas="resume">
    <?php include 'offcanvas--resume.php'; ?>
  </aside>
  <aside class="app__offcanvas app__offcanvas--mob-nav offcanvas" data-offcanvas="mob-nav">
    <?php include 'offcanvas--mob-nav.php'; ?>
  </aside>
  <aside class="app__offcanvas app__offcanvas--wizard offcanvas" data-offcanvas="wizard">
    <?php include 'offcanvas--wizard.php'; ?>
  </aside>
