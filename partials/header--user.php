<div class="header__container container">
  <a class="header__logo header__logo--desktop" href="home">
    <?php include '../assets/img/logo.svg'; ?>
  </a>
  <a class="header__logo header__logo--mobile" href="home">
    <?php include '../assets/img/logo__ok.svg'; ?>
  </a>
  <a href="" class="header__close"></a>
</div>
