<div class="mob-nav__wrapper">
  <div class="mob-nav__actions">
    <button class="form__submit btn">
      <i class="btn__icon btn__icon--left">
        <?php include '../assets/img/icon--calc.svg'; ?>
      </i>
      <span class="btn__text">Paskolos skaičiuoklė</span>
    </button>
  </div>
  <div class="mob-nav__menu">
    <ul class="mob-nav__list">
      <li class="mob-nav__item">
        <a href="career" class="mob-nav__trigger">Karjera</a>
        <div class="mob-nav__details">4 pozicijos</div>
      </li>
      <li class="mob-nav__item"><a href="loan" class="mob-nav__trigger">Paskola</a></li>
      <li class="mob-nav__item"><a href="leasing" class="mob-nav__trigger">Lizingas</a></li>
      <li class="mob-nav__item mob-nav__item--s"><a href="loan" class="mob-nav__trigger mob-nav__trigger--s">Prekių
          draudimas</a></li>
      <li class="mob-nav__item"><a href="loan" class="mob-nav__trigger">Kortelė</a></li>
      <li class="mob-nav__item"><a href="about" class="mob-nav__trigger">Apie mus</a></li>
      <li class="mob-nav__item"><a href="faq" class="mob-nav__trigger">Pagalba</a></li>
      <li class="mob-nav__item mob-nav__item--s"><a href="loan" class="mob-nav__trigger mob-nav__trigger--s">Prašymų
          formos</a></li>
    </ul>

  </div>
  <div class="mob-nav__submenu">
    <ul class="mob-nav__list">
      <li class="mob-nav__item mob-nav__item--s"><a href="parnters" class="mob-nav__trigger mob-nav__trigger--s">Verslo
          partneriams</a></li>
      <li class="mob-nav__item mob-nav__item--s"><a href="about" class="mob-nav__trigger mob-nav__trigger--s">Apie
          mokilizingą</a></li>
      <li class="mob-nav__item mob-nav__item--s"><a href="privacy" class="mob-nav__trigger mob-nav__trigger--s">Privatumo
          politika</a></li>
      <li class="mob-nav__item mob-nav__item--s"><a href="privacy" class="mob-nav__trigger mob-nav__trigger--s">Taisyklės
          ir sąlygos</a></li>
      <li class="mob-nav__item mob-nav__item--s"><a href="privacy" class="mob-nav__trigger mob-nav__trigger--s">Dokumentai
          ir nuostatos</a></li>
    </ul>
  </div>
  <div class="mob-nav__footer">
    <div class="mob-nav__langs langs">
      <ul class="langs__list">
        <li class="langs__item is-active"><a href="" class="langs__trigger">LT</a></li>
        <li class="langs__item"><a href="" class="langs__trigger">LV</a></li>
      </ul>
    </div>
    <div class="mob-nav__socials socials socials--dark">
      <?php include 'socials.php'; ?>
    </div>
  </div>

</div>
