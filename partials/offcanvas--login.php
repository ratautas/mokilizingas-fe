<div class="offcanvas__wrapper">
  <div class="offcanvas__top">
    <h4>PRISIJUNGTI</h4>
  </div>
  <div class="offcanvas__tabs tabs" data-tabs>
    <div class="tabs__triggers tabs__triggers--offcanvas">
      <div class="tabs__trigger tabs__trigger--offcanvas is-active" tabindex="0"
        data-tabs-trigger="email">El.paštu
        <i class="tabs__underline"></i>
      </div>
      <div class="tabs__trigger tabs__trigger--offcanvas" tabindex="0"
        data-tabs-trigger="signature">
        M. parašu
        <span class="tabs__tag tag">Patariame</span>
      </div>
    </div>
    <div class="tabs__contents tabs__contents--offcanvas">
      <div class="tabs__content is-active" data-tabs-content="email">
        <form class="offcanvas__form form"
          action="http://localhost/mokilizingas-fe/endpoints/contact.php">
          <div class="offcanvas__content" data-form-content>
            <div class="form__content form__content--login">
              <div class="form__row form__row--high">
                <div class="form__item">
                  <span class="form__label">El.paštas / vartotojo vardas</span>
                  <input type="text" name="login-offcanvas" required class="form__input">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row form__row--high">
                <div class="form__item">
                  <span class="form__label">Slaptažodis</span>
                  <input type="password" name="login-password" required class="form__input">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row form__row--high">
                <div class="form__item">
                  <i class="form__trigger" data-offcanvas-trigger="remind">Pamiršai slaptažodį?</i>
                </div>
              </div>
            </div>
            <div class="form__footer form__footer--offcanvas">
              <div class="form__row form__row--high">
                <div class="form__item">
                  <button class="form__submit btn">
                    <span class="btn__text">Tęsti</span>
                  </button>
                </div>
              </div>
              <div class="form__already already">
                <div class="already__label">Esate naujokas?</div>
                <div class="already__trigger" data-offcanvas-trigger="register">Susikurkite paskyrą
                </div>
              </div>
            </div>
          </div>
          <div class="form__done form__done--success done done--success" data-form-done="success">
            <i class="done__close" data-done-close></i>
            <div class="done__top">
              <h2>Ačiū.</h2>
            </div>
            <div class="done__text">
              <p>Su jumis susisieks mūsų konsultantė</p>
            </div>
          </div>
          <div class="form__done form__done--error done done--error" data-form-done="error">
            <i class="done__close" data-done-close></i>
            <div class="done__top">
              <h2>Klaida!</h2>
            </div>
            <div class="done__text">
              <p>Su jumis susisieks mūsų konsultantė</p>
            </div>
          </div>
        </form>
      </div>
      <div class="tabs__content" data-tabs-content="signature">
        <form class="offcanvas__form form"
          action="http://localhost/mokilizingas-fe/endpoints/contact.php">
          <div class="offcanvas__content" data-form-content>
            <div class="form__content form__content--login">
              <div class="form__row form__row--high">
                <div class="form__item">
                  <span class="form__label">Asmens kodas</span>
                  <input type="text" name="login-code" pattern="^.[0-9]{10,10}$" required
                    class="form__input">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row form__row--high">
                <div class="form__item">
                  <span class="form__label">Telefono numeris</span>
                  <input type="tel" name="login-tel" required class="form__input">
                  <span class="form__error">Error</span>
                </div>
              </div>
            </div>
            <div class="form__footer form__footer--offcanvas">
              <div class="form__row form__row--high">
                <div class="form__item">
                  <button class="form__submit btn">
                    <span class="btn__text">Tęsti</span>
                  </button>
                </div>
              </div>
              <div class="form__already already">
                <div class="already__label">Esate naujokas?</div>
                <div class="already__trigger" data-offcanvas-trigger="register">Susikurkite paskyrą
                </div>
              </div>
            </div>
          </div>
          <div class="form__done form__done--success done done--success" data-form-done="success">
            <i class="done__close" data-done-close></i>
            <div class="done__top">
              <h2>Ačiū.</h2>
            </div>
            <div class="done__text">
              <p>Su jumis susisieks mūsų konsultantė</p>
            </div>
          </div>
          <div class="form__done form__done--error done done--error" data-form-done="error">
            <i class="done__close" data-done-close></i>
            <div class="done__top">
              <h2>Klaida!</h2>
            </div>
            <div class="done__text">
              <p>Su jumis susisieks mūsų konsultantė</p>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="offcanvas__close offcanvas__close--user" data-offcanvas-close></div>
</div>