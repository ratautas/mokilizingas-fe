<div class="offcanvas__wrapper">
  <div class="offcanvas__top">
    <h4>PARAŠYK MUMS</h4>
  </div>
  <form class="offcanvas__form form" action="http://localhost/mokilizingas-fe/endpoints/contact.php">
    <div class="offcanvas__content" data-form-content>
      <div class="form__content form__content--resume">
        <div class="form__row form__row--high">
          <div class="form__item">
            <span class="form__label">Jūsų vardas*</span>
            <input type="text" name="resume-name" required class="form__input">
            <span class="form__error">Error</span>
          </div>
          <div class="form__item">
            <span class="form__label">Jūsų pavardė*</span>
            <input type="text" name="resume-surname" required class="form__input">
            <span class="form__error">Error</span>
          </div>
        </div>
        <div class="form__row form__row--high">
          <div class="form__item">
            <span class="form__label">Jūsų el. paštas*</span>
            <input type="email" name="resume-email" required class="form__input">
            <span class="form__error">Error</span>
          </div>
        </div>
        <div class="form__row form__row--high">
          <div class="form__item">
            <div class="form__file file">
              <i class="file__icon">
                <?php include '../assets/img/icon--upload.svg'; ?>
              </i>
              <span class="file__label file__label--add">Pridėti CV</span>
              <div class="file__label file__label--name" data-file-name></div>
            </div>
            <input type="file" accept=".doc,.pdf" name="resume-file" required class="form__native form__native--file">
            <span class="form__error">Error</span>
          </div>
        </div>
        <div class="form__row form__row--high">
          <label class="form__item">
            <input type="checkbox" class="form__native" required>
            <i class="form__control form__control--checkbox"></i>
            <span class="form__inlabel">
              Patvirtinu, kad pateikti duomenys yra teisingi. Susipažinau ir sutinku su
              <a target="_blank" href="basic">privatumo politika</a>.</span>
            <span class="form__error">Error</span>
          </label>
        </div>
      </div>
      <div class="form__footer form__footer--offcanvas">
        <div class="form__row form__row--high">
          <div class="form__item">
            <button class="form__submit btn">
              <i class="btn__icon btn__icon--left">
                <?php include '../src/img/icon--mail.svg'; ?>
              </i>
              <span class="btn__text">Siųsti</span>
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="form__done form__done--success done done--success" data-form-done="success">
      <i class="done__close" data-done-close></i>
      <div class="done__top">
        <h2>Ačiū.</h2>
      </div>
      <div class="done__text">
        <p>Su jumis susisieks mūsų konsultantė</p>
      </div>
    </div>
    <div class="form__done form__done--error done done--error" data-form-done="error">
      <i class="done__close" data-done-close></i>
      <div class="done__top">
        <h2>Klaida!</h2>
      </div>
      <div class="done__text">
        <p>Su jumis susisieks mūsų konsultantė</p>
      </div>
    </div>
  </form>
  <div class="offcanvas__close" data-offcanvas-close></div>
</div>
