<div class="cookiebar__intro">
  <p>Mes naudojame slapukus siekdami individualizuoti Jūsų naršymo patirtį ir Jums rodomą turinį
    bei pateikti pasiūlymus, kurie būtų aktualūs būtent Jums. </p>
</div>
<div class="cookiebar__text">
  <p>Parduotuvė naudoja tiek būtinuosius (svetainės veikimą užtikrinančius ar naudojimąsi ja
    palengvinančius) slapukus, tokius kaip filtravimo nustatymų ar prisijungimo prisiminimo, tiek
    ir analitinius slapukus. Mes nenaudosime ir jokiai trečiajai šaliai neleisime naudoti
    statistinės analizės duomenų tam, kad atsektų ar rinktų bet kokią informaciją, leidžiančią
    nustatyti šios svetainės lankytojų tapatybę. Paspaudę „sutinku“ arba toliau tęsdami naršymą
    svetainėje Jūs sutinkate su slapukų įdiegimu ir naudojimu. Savo sutikimą galėsite atšaukti
    bet kuriuo metu, pakeisdami savo interneto naršyklės nustatymus ir ištrindami įrašytus
    slapukus. Daugiau informacijos – slapukų politikoje</p>
</div>
<div class="cookiebar__actions">
  <div class="cookiebar__action btn btn--narrow" data-cookiebar-accept>Sutinku</div>
</div>
