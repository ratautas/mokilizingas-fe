</main>
<?php if(!$localhost): ?>
<script async defer type="text/javascript" src="../assets/js/app.js"></script>
<?php else: ?>
<script type="text/javascript" src="http://localhost:5000/assets/js/app.js"></script>
<script id="__bs_script__">
  //<![CDATA[
  document.write(
    "<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.3'><\/script>"
    .replace(
      "HOST", location.hostname));
  //]]>

</script>

<?php endif; ?>
<?php include 'list.php'; ?>
</body>

</html>
