<div class="offcanvas__wrapper">
  <div class="offcanvas__top">
    <h4>PRIMINTI SLAPTAŽODĮ?</h4>
  </div>
  <form class="offcanvas__form form" action="http://localhost/mokilizingas-fe/endpoints/remind.php">
    <div class="offcanvas__content" data-form-content>
      <div class="form__content form__content--register">
        <div class="form__row form__row--high">
          <div class="form__item">
            <span class="form__label">Jūsų el. paštas*</span>
            <input type="email" name="remoind-email" required class="form__input">
            <span class="form__error">Error</span>
          </div>
        </div>
      </div>
      <div class="form__footer form__footer--offcanvas">
        <div class="form__row form__row--high">
          <div class="form__item">
            <button class="form__submit btn">
              <span class="btn__text">Priminti</span>
            </button>
          </div>
        </div>
        <div class="form__already already">
          <div class="already__label">Turite paskyrą?</div>
          <div class="already__trigger" data-offcanvas-trigger="login">Prisijunkite</div>
        </div>
      </div>
    </div>
    <div class="form__done form__done--success done done--success" data-form-done="success">
      <i class="done__close" data-done-close></i>
      <div class="done__top">
        <h2>Ačiū.</h2>
      </div>
      <div class="done__text">
        <p>Su jumis susisieks mūsų konsultantė</p>
      </div>
    </div>
    <div class="form__done form__done--error done done--error" data-form-done="error">
      <i class="done__close" data-done-close></i>
      <div class="done__top">
        <h2>Klaida!</h2>
      </div>
      <div class="done__text">
        <p>Su jumis susisieks mūsų konsultantė</p>
      </div>
    </div>
  </form>
  <div class="offcanvas__close offcanvas__close--user" data-offcanvas-trigger="login"></div>
</div>
