<div class="wizard__wrapper" data-wizard>
  <form class="wizard__form custom-submit" data-wizard-form
    action="http://localhost/mokilizingas-fe/endpoints/wizard.php" novalidate="true">
    <div class="form__content wizard__step wizard__step--1 is-active" data-wizard-step="1">
      <div class="wizard__content">
        <div data-wizard-type="1" data-wizard-id="2" data-wizard-type-label="Paskola automobiliui"
          data-wizard-min="500" data-wizard-max="15000"
          data-wizard-image="../media/proffer__icon--2.png"
          data-wizard-radio-terms="6,12,18,24"
          data-wizard-terms="3,6,12,15,18,21,24,27,30" data-wizard-default-term="12"
          data-wizard-default-amount="500"></div>
        <div data-wizard-type="2" data-wizard-id="1" data-wizard-type-label="Vartojimo paskola"
          data-wizard-min="300" data-wizard-max="15000"
          data-wizard-image="../media/proffer__icon--1.png"
          data-wizard-radio-terms="12,15,18,21"
          data-wizard-terms="6,12,15,18,21,24,27,30,60" data-wizard-default-term="12"
          data-wizard-default-amount="300"></div>
        <div data-wizard-type="3" data-wizard-id="3" data-wizard-type-label="Paskola būsto remontui"
          data-wizard-min="300" data-wizard-max="15000"
          data-wizard-radio-terms="30,45,60,75"
          data-wizard-image="../media/proffer__icon--3.png" data-wizard-terms="3,6,30,45,60,75"
          data-wizard-default-term="30" data-wizard-default-amount="300"></div>
        <div class="form__row">
          <div class="form__item">
            <select name="wizard-type" class="form__select" data-wizard-select data-dropdown>
              <option value="1">Paskola automobiliui</option>
              <option value="2">Vartojimo paskola</option>
              <option value="3">Paskola būsto remontui</option>
            </select>
            <span class="form__error">Error</span>
          </div>
        </div>
        <div class="form__row">
          <div class="form__item">
            <span class="form__label">Įveskite norimą paskolos sumą</span>
            <div class="form__help">
              <span data-wizard-range-min>500&nbsp;€</span>
              &nbsp;-&nbsp;
              <span data-wizard-range-max>15000&nbsp;€</span>
            </div>
            <input type="number" inputmode="decimal" pattern="[0-9]*" name="wizard-amount-input"
              required class="form__input">
            <span class="form__error"></span>
          </div>
        </div>
        <div class="form__row">
          <div class="form__item">
            <div class="form__range form__range--amount range">
              <div class="range__target range__target--amount" data-wizard-range></div>
            </div>
            <input type="hidden" name="wizard-amount" data-wizard-amount tabindex="-1" value="500">
          </div>
        </div>
        <div class="form__row">
          <div class="form__item">
            <span class="form__label">Populiariausi grąžinimo terminai</span>
            <div class="form__help form__help--top">
              Nuo
              <span data-wizard-term-min>6</span>
              &nbsp;-&nbsp;
              <span data-wizard-term-max>72</span>
              &nbsp;mėn
            </div>
            <div class="term__scroller">
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="3">
                <span class="term__label">3 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="6">
                <span class="term__label">6 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="9">
                <span class="term__label">9 mėn.</span>
              </label>
              <label class="term__item is-featured">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native is-featured" value="12" checked="checked">
                <span class="term__label">12 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="15">
                <span class="term__label">15 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="18">
                <span class="term__label">18 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="21">
                <span class="term__label">21 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="24">
                <span class="term__label">24 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="30">
                <span class="term__label">30 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="45">
                <span class="term__label">45 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="60">
                <span class="term__label">60 mėn.</span>
              </label>
              <label class="term__item">
                <input type="radio" name="wizard-term-radio" data-wizard-term-radio
                  class="term__radio form__native" value="75">
                <span class="term__label">75 mėn.</span>
              </label>
            </div>
          </div>
        </div>
        <div class="form__row">
          <div class="form__item">
            <span class="form__label">Įrašyti savo terminą</span>
            <div class="form__help">
              Nuo
              <span data-wizard-term-min>6</span>
              &nbsp;-&nbsp;
              <span data-wizard-term-max>72</span>
              &nbsp;mėn
            </div>
            <input type="number" inputmode="decimal" pattern="[0-9]*" name="wizard-term"
              data-wizard-term-input required class="form__input form__input--faded">
            <span class="form__error"></span>
          </div>
        </div>
      </div>
      <div class="wizard__footer">
        <div class="wizard__notify">Pinigus pervesime per 30 min., jei paskolos sutartį
          sudarysite iki 20 val.</div>
        <div class="wizard__btn btn" data-wizard-result-trigger>
          <span class="btn__text">Skaičiuoti įmoką</span>
          <i class="btn__icon btn__icon--right">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5.1997 8.9849">
              <polygon
                points="0.707 8.985 0 8.278 3.786 4.493 0 0.707 0.707 0 5.2 4.493 0.707 8.985"
                fill="#fff"></polygon>
            </svg> </i>
        </div>
      </div>
    </div>
    <div class="form__content form__content--no-flip wizard__step wizard__step--2"
      data-wizard-step="2">
      <div class="wizard__content">
        <div class="wizard__summary summary">
          <div class="summary__back" data-wizard-update-trigger>Keisti sumą</div>
          <div class="summary__title" data-wizard-summary-type>Paskola automobiliui</div>
          <div class="summary__range range">
            <div class="range__target range__target--update-term" data-wizard-update-term-range>
            </div>
          </div>
          <div class="summary__table">
            <div class="summary__row">
              <div class="summary__col summary__col--label">Paskolos suma:</div>
              <div class="summary__col summary__col--val"><span
                  data-wizard-summary-amount></span>&nbsp;€</div>
            </div>
            <div class="summary__row">
              <div class="summary__col summary__col--label">Grąžinimo terminas:</div>
              <div class="summary__col summary__col--val"><span
                  data-wizard-summary-term></span>&nbsp;mėn.</div>
            </div>
            <div class="summary__row summary__row--bold">
              <div class="summary__col summary__col--label">Mėnesio įmoka:</div>
              <div class="summary__col summary__col--val"><span
                  data-wizard-summary-monthly></span>&nbsp;€</div>
            </div>
          </div>
        </div>
      </div>
      <div class="wizard__footer">
        <div class="wizard__footnote">
          <p>Pavyzdžiui, skolinantis <span data-wizard-example-amount></span>&nbsp;€, kai
            sutartis sudaroma <span data-wizard-example-term></span> mėn. terminui, metinė
            palūkanų norma – <span data-wizard-example-yearly></span>, mėnesio sutarties
            mokestis – <span data-wizard-example-fee></span>, BVKKMN – <span
              data-wizard-example-bvkkmn></span>, bendra mokėtina suma – <span
              data-wizard-example-total></span>, mėnesio įmoka – <span
              data-wizard-example-monthly></span>&nbsp;€.</p>

          <p>Atsižvelgiant į lizingo bendrovės atliktą Jūsų kreditingumo ir rizikos vertinimą ar
            pasirinkus kitokį sutarties terminą bei įmokų mokėjimo dieną, Jums gali būti
            pasiūlytos ir kitokios sąlygos (pavyzdžiui, metinės palūkanų normos arba mėnesio
            sutarties mokesčio dydis).</p>
        </div>
        <div class="wizard__notify">Pinigus pervesime per 30 min., jei paskolos sutartį
          sudarysite iki 20 val.</div>
        <input type="hidden" name="type" tabindex="-1" />
        <input type="hidden" name="term" tabindex="-1" />
        <input type="hidden" name="amount" tabindex="-1" />
        <button class="wizard__btn btn">
          <span class="btn__text">Gauti paskolą</span>
          <i class="btn__icon btn__icon--right">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5.1997 8.9849">
              <polygon
                points="0.707 8.985 0 8.278 3.786 4.493 0 0.707 0.707 0 5.2 4.493 0.707 8.985"
                fill="#fff"></polygon>
            </svg> </i>
        </button>
      </div>
    </div>
    <div class="form__done form__done--error done done--error" data-form-done="error">
      <i class="done__close" data-done-close></i>
      <div class="done__top">
        <h2>Error</h2>
      </div>
      <div class="done__text">
        <p>Atsiprašome, šiuo metu turime nesklandumų. Bandykite vėliau dar kartą...</p>
      </div>
    </div>
  </form>
</div>