<div class="header__container container">
  <div class="header__navicon navicon" data-offcanvas-trigger="mob-nav">
    <i class="navicon__bar navicon__bar--1"></i>
    <i class="navicon__bar navicon__bar--2"></i>
    <i class="navicon__bar navicon__bar--3"></i>
  </div>
  <a class="header__logo header__logo--desktop" href="home">
    <?php include '../assets/img/logo.svg'; ?>
  </a>
  <a class="header__logo header__logo--mobile" href="home">
    <?php include '../assets/img/logo__ok.svg'; ?>
  </a>
  <nav class="header__nav nav">
    <div class="nav__container">
      <ul class="nav__list">
        <li class="nav__item"><a href="loan" class="nav__trigger">Paskola</a>
          <div class="nav__submenu">
            <ul class="nav__sublist">
              <li class="nav__subitem">
                <a href="loan" class="nav__subtrigger">Paskola automoboliui</a>
              </li>
              <li class="nav__subitem">
                <a href="loan" class="nav__subtrigger">Vartojimo paskola</a>
              </li>
              <li class="nav__subitem">
                <a href="loan" class="nav__subtrigger">Būsto paskola</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav__item"><a href="leasing" class="nav__trigger">Lizingas</a></li>
        <li class="nav__item"><a href="" class="nav__trigger">Kortelė</a></li>
        <li class="nav__item"><a href="faq" class="nav__trigger">Pagalba</a></li>
        <li class="nav__item is-active"><a href="about" class="nav__trigger">Apie mus</a></li>
        <li class="nav__item"><a href="career" class="nav__trigger">Karjera</a></li>
      </ul>
    </div>
  </nav>
  <div class="header__user-trigger user-trigger">
    <div class="user-trigger__container">
      <i class="user-trigger__icon">
        <?php include '../assets/img/icon--user.svg'; ?>
      </i>
      <span class="user-trigger__label">Savitarna</span>
      <a href="login" class="user-trigger__link">Savitarna</a>
    </div>
  </div>
</div>