<div class="offcanvas__wrapper">
  <div class="offcanvas__actions">
    <button class="offcanvas__action btn" data-offcanvas-trigger="wizard">
      <i class="btn__icon btn__icon--left">
        <?php include '../assets/img/icon--calc.svg'; ?>
      </i>
      <span class="btn__text">Paskolos skaičiuoklė</span>
    </button>
  </div>
  <div class="offcanvas__menu">
    <ul class="offcanvas__list">
      <li class="offcanvas__item">
        <a href="career" class="offcanvas__trigger">Karjera</a>
        <div class="offcanvas__details">4 pozicijos</div>
      </li>
      <li class="offcanvas__item"><a href="loan" class="offcanvas__trigger">Paskola</a></li>
      <li class="offcanvas__item"><a href="leasing" class="offcanvas__trigger">Lizingas</a></li>
      <li class="offcanvas__item offcanvas__item--s"><a href="loan" class="offcanvas__trigger offcanvas__trigger--s">Prekių
          draudimas</a></li>
      <li class="offcanvas__item"><a href="loan" class="offcanvas__trigger">Kortelė</a></li>
      <li class="offcanvas__item"><a href="about" class="offcanvas__trigger">Apie mus</a></li>
      <li class="offcanvas__item"><a href="faq" class="offcanvas__trigger">Pagalba</a></li>
      <li class="offcanvas__item offcanvas__item--s"><a href="loan" class="offcanvas__trigger offcanvas__trigger--s">Prašymų
          formos</a></li>
    </ul>

  </div>
  <div class="offcanvas__submenu">
    <ul class="offcanvas__list">
      <li class="offcanvas__item offcanvas__item--s"><a href="parnters" class="offcanvas__trigger offcanvas__trigger--s">Verslo
          partneriams</a></li>
      <li class="offcanvas__item offcanvas__item--s"><a href="about" class="offcanvas__trigger offcanvas__trigger--s">Apie
          mokilizingą</a></li>
      <li class="offcanvas__item offcanvas__item--s"><a href="privacy" class="offcanvas__trigger offcanvas__trigger--s">Privatumo
          politika</a></li>
      <li class="offcanvas__item offcanvas__item--s"><a href="privacy" class="offcanvas__trigger offcanvas__trigger--s">Taisyklės
          ir sąlygos</a></li>
      <li class="offcanvas__item offcanvas__item--s"><a href="privacy" class="offcanvas__trigger offcanvas__trigger--s">Dokumentai
          ir nuostatos</a></li>
    </ul>
  </div>
  <div class="offcanvas__footer">
    <div class="offcanvas__langs langs">
      <ul class="langs__list">
        <li class="langs__item is-active"><a href="" class="langs__trigger">LT</a></li>
        <li class="langs__item"><a href="" class="langs__trigger">LV</a></li>
      </ul>
    </div>
    <div class="offcanvas__socials socials socials--dark">
      <?php include 'socials.php'; ?>
    </div>
  </div>

</div>
