<div class="footer__links">
  <div class="footer__container container">
    <div class="footer__logo">
      <?php include '../assets/img/logo.svg'; ?>
    </div>
    <div class="footer__nav">
      <ul class="footer__nav-list">
        <li class="footer__nav-item"><a href="" class="footer__nav-trigger">Verslo partneriams</a>
        </li>
        <li class="footer__nav-item"><a href="" class="footer__nav-trigger">Prašymų formos</a></li>
        <li class="footer__nav-item"><a href="" class="footer__nav-trigger">Prekių draudimas</a>
        </li>
        <li class="footer__nav-item"><a href="" class="footer__nav-trigger">Partnerių sąrašas</a>
        </li>
        <li class="footer__nav-item"><a href="" class="footer__nav-trigger">Apie Mokilizingą</a>
        </li>
      </ul>
    </div>
    <div class="footer__langs langs">
      <ul class="langs__list">
        <li class="langs__item is-active"><a href="" class="langs__trigger">LT</a></li>
        <li class="langs__item"><a href="" class="langs__trigger">LV</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="footer__connect">
  <div class="footer__container container">
    <div class="footer__reachings">
      <div class="footer__reaching reaching">
        <i class="reaching__icon reaching__icon--footer">
          <?php include '../assets/img/icon--message.svg'; ?>
        </i>
        <div class="reaching__label reaching__label--footer">KLAUSKITE</div>
        <div class="reaching__content reaching__content--footer">
          <div class="reaching__row">
            <a href="tel:+370 700 55 888">+370 700 55 888</a>
          </div>
          <div class="reaching__row">
            <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
          </div>
        </div>
      </div>
      <div class="footer__reaching reaching">
        <i class="reaching__icon reaching__icon--footer">
          <?php include '../assets/img/icon--location.svg'; ?></i>
        <div class="reaching__label reaching__label--footer">MUS RASITE</div>
        <div class="reaching__content reaching__content--footer">
          <div class="reaching__row">Kareivių g. 11 B, 09109, Vilnius</div>
        </div>
      </div>
      <div class="footer__reaching reaching">
        <i class="reaching__icon reaching__icon--footer">
          <?php include '../assets/img/icon--phone.svg'; ?></i>
        <div class="reaching__label reaching__label--footer">SKAMBINKITE</div>
        <div class="reaching__content reaching__content--footer">
          <div class="reaching__row">
            <div class="reaching__col reaching__col--s">I-V</div>
            <div class="reaching__col reaching__col--l">08:00</div>
          </div>
          <div class="reaching__row">
            <div class="reaching__col reaching__col--s">VI-VII</div>
            <div class="reaching__col reaching__col--l">nedarbo dienos</div>
          </div>
        </div>
      </div>
      <div class="footer__reaching reaching">
        <i class="reaching__icon reaching__icon--footer">
          <?php include '../assets/img/icon--list.svg'; ?></i>
        <div class="reaching__label reaching__label--footer">REKVIZITAI</div>
        <div class="reaching__content reaching__content--footer">
          <div class="reaching__row">Įmonės kodas: 124926897</div>
          <div class="reaching__row">PVM kodas: LT24926897</div>
          <div class="reaching__row">Banko sąskaita: LT45 7400 0423 45892 3819</div>
        </div>
      </div>
    </div>
    <div class="footer__subscribe">
      <h4>GAUKITE GERIAUSIUS PASIŪLYMUS</h4>
      <form class="form form--subscribe"
        action="http://localhost/mokilizingas-fe/endpoints/contact.php" data-form="subscribe">
        <div class="form__content" data-form-content>
          <div class="form__row">
            <div class="form__item">
              <input type="email" name="name" placeholder="vardenis@pavardenis.lt" required
                class="form__input form__input--white">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <label class="form__item">
              <input type="checkbox" class="form__native" required>
              <i class="form__control form__control--white form__control--checkbox"></i>
              <span class="form__inlabel">Susipažinau ir sutinku su <a href="basic" target="_blank"
                  class="form__trigger">
                  privatumo politika</a></span>
              <span class="form__error">Error</span>
            </label>
          </div>
          <div class="form__row">
            <div class="form__item">
              <button class="form__submit btn">
                <i class="btn__icon btn__icon--left">
                  <?php include '../assets/img/icon--mail.svg'; ?>
                </i>
                <span class="btn__text">Prenumeruoti</span>
              </button>
            </div>
          </div>
        </div>
        <div class="form__done form__done--success done done--success" data-form-done="success">
          <i class="done__close" data-done-close></i>
          <div class="done__top">
            <h2>Ačiū.</h2>
          </div>
          <div class="done__text">
            <p>Su jumis susisieks mūsų konsultantė</p>
          </div>
        </div>
        <div class="form__done form__done--error done done--error" data-form-done="error">
          <i class="done__close" data-done-close></i>
          <div class="done__top">
            <h2>Klaida!</h2>
          </div>
          <div class="done__text">
            <p>Su jumis susisieks mūsų konsultantė</p>
          </div>
        </div>
      </form>
    </div>
    <div class="footer__sitemap sitemap" data-sitemap>
      <div class="sitemap__top">
        <h4>PASALAUGOS</h4>
      </div>
      <div class="sitemap__blocks">
        <div class="sitemap__block">
          <ul class="sitemap__list">
            <li class="sitemap__item sitemap__item--featured"><a href=""
                class="sitemap__trigger">Greitas kreditas</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Ilgalaike paskola</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Kreditai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Automobilio pirkimas
                issimoketinai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Kreditas
                automobiliui</a>
            </li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Greitas
                kreditas</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Ilgalaike paskola</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Kreditai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Automobilio pirkimas
                issimoketinai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Kreditas
                automobiliui</a>
            </li>
          </ul>
        </div>
        <div class="sitemap__block">
          <ul class="sitemap__list">
            <li class="sitemap__item sitemap__item--featured"><a href=""
                class="sitemap__trigger">Automobiliu lizingas be banko</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Ilgalaike paskola</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Kreditai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Automobilio pirkimas
                issimoketinai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Kreditas
                automobiliui</a>
            </li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Greitas
                kreditas</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Ilgalaike paskola</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Kreditai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Automobilio pirkimas
                issimoketinai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Kreditas
                automobiliui</a>
            </li>
          </ul>
        </div>
        <div class="sitemap__block">
          <ul class="sitemap__list">
            <li class="sitemap__item sitemap__item--featured"><a href=""
                class="sitemap__trigger">Traktoriaus lizingas</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Ilgalaike paskola</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Kreditai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Automobilio pirkimas
                issimoketinai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Kreditas
                automobiliui</a>
            </li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Greitas
                kreditas</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Ilgalaike paskola</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Kreditai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href=""
                class="sitemap__trigger">Automobilio pirkimas
                issimoketinai</a></li>
            <li class="sitemap__item" data-sitemap-item><a href="" class="sitemap__trigger">Kreditas
                automobiliui</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="sitemap__expander sitemap__expander--more is-active" data-sitemap-expand>Daugiau</div>
      <div class="sitemap__expander sitemap__expander--less" data-sitemap-contract>Mažiau</div>
    </div>
  </div>
</div>

<div class="footer__bottom">
  <div class="footer__container container">
    <div class="footer__copyright">&copy;2018 Mokilizingas</div>
    <div class="footer__quick-links">
      <a href="basic" class="footer__quick-link">Privatumo politika</a>
      <a href="basic" class="footer__quick-link">Taisyklės ir sąlygos</a>
    </div>
    <a href="" target="_blank" class="footer__credits">
      <?php include '../assets/img/choco.svg'; ?></a>
  </div>
</div>