<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--yellow">
    <div class="default__container container">
      <div class="default__socials socials">
        <?php include '../partials/socials.php'; ?>
      </div>
      <div class="default__header default__header--high">
        <h1>Karjera</h1>
        <div class="default__media default__media--high media">
          <i class="media__image" style="background-image:url('../media/careers__media.png')"></i>
        </div>
        <div class="default__intro default__intro--narrow">
          <p>Esame profesionalių ir entuziazmu degančių žmonių komanda. Prisijungęs prie mūsų
            turėsi dideles profesinio
            ir asmeninio augimo galimybes, iniciatyvumą ir savarankiškumą skatinančią darbo aplinką
            bei daug erdvės
            savo kompetencijoms atskleisti.</p>
          <p>Išbandome įvairius formatus: nuo bendrų pietų iki aktyvaus sporto. Bėgame, miname,
            irkluojame, slidinėjame
            – visi drauge nuolat kuriame savo mažas tradicijas.</p>
          <p>Siųsk savo CV adresu <a href="mailto:hr@mokilizingas.lt">hr@mokilizingas.lt</a> arba
            susisiek tiesiogiai
            per Linkedin.</p>
        </div>
        <div class="default__actions">
          <a href="" class="default__action btn">
            <i class="btn__icon btn__icon--left">
              <?php include '../assets/img/icon--linkedin.svg'; ?></i>
            <span class="btn__text">Apply via Linkedin</span>
          </a>
          <div data-offcanvas-trigger="resume" class="default__action btn btn--narrow">
            <span class="btn__text">Siųsk CV</span>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--positions">
    <div class="default__container container">
      <h3>IEŠKOME</h3>
      <div class="default__cards">
        <div class="default__card card card--third">
          <div class="card__media card__media--third media">
            <i class="media__image media__image--cover" style="background-image:url('../media/careers__media--card.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Sistemų adiminstratorius </div>
            </div>
            <div class="card__bottom">
              <a href="position" class="card__trigger">Prisijunkite prie mūsų</a>
            </div>
          </div>
          <a href="position" class="card__link"></a>
        </div>
        <div class="default__card card card--third">
          <div class="card__media card__media--third media"><i class="media__image media__image--cover"
              style="background-image:url('../media/careers__media--card.png')"></i></div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Sistemų adiminstratorius </div>
            </div>
            <div class="card__bottom">
              <a href="position" class="card__trigger">Prisijunkite prie mūsų</a>
            </div>
          </div>
          <a href="position" class="card__link"></a>
        </div>
        <div class="default__card card card--third">
          <div class="card__media card__media--third media"><i class="media__image media__image--cover"
              style="background-image:url('../media/careers__media--card.png')"></i></div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Sistemų adiminstratorius </div>
            </div>
            <div class="card__bottom">
              <a href="position" class="card__trigger">Prisijunkite prie mūsų</a>
            </div>
          </div>
          <a href="position" class="card__link"></a>
        </div>
        <div class="default__card card card--third">
          <div class="card__media card__media--third media"><i class="media__image media__image--cover"
              style="background-image:url('../media/careers__media--card.png')"></i></div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Sistemų adiminstratorius </div>
            </div>
            <div class="card__bottom">
              <a href="position" class="card__trigger">Prisijunkite prie mūsų</a>
            </div>
          </div>
          <a href="position" class="card__link"></a>
        </div>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php'; ?>
