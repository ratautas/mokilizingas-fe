<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--grey">
    <div class="default__container container">
      <div class="default__socials socials socials--dark">
        <?php include '../partials/socials.php'; ?>
      </div>
      <div class="default__header default__header--narrow">
        <h1>System administrator</h1>
        <div class="default__media default__media--position media">
          <i class="media__image media__image--bottom"
            style="background-image:url('../media/position__media.png')"></i>
        </div>
        <label class="default__agreement form__item">
          <input type="checkbox" class="form__native" name="agree" data-linkedin-agree required>
          <i class="form__control form__control--checkbox"></i>
          <span class="form__inlabel">
            Patvirtinu, kad pateikti duomenys yra teisingi. Susipažinau ir sutinku su
            <a target="_blank" href="basic">privatumo politika</a>.</span>
        </label>
        <div class="default__actions">
          <a href="" class="default__action btn" data-linkedin-btn>
            <i class="btn__icon btn__icon--left">
              <?php include '../assets/img/icon--linkedin.svg'; ?></i>
            <span class="btn__text">Apply via Linkedin</span>
          </a>
          <div data-offcanvas-trigger="resume" class="default__action btn btn--narrow">
            <span class="btn__text">Siųsk CV</span>
          </div>
        </div>
        <div class="default__intro">
          <p>Join our challenge - loving MOKILIZINGAS team. We've been working in the financial
            sector and specialising
            in credit and leasing services for over 18 years. During this period more than 900 000
            clients were served
            and 2 million contracts were signed. Maybe you
            will be the one to help us move forward even faster?</p>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--columns default__section--grey">
    <div class="default__container container">
      <div class="default__columns">
        <div class="default__column">
          <div class="default__block">
            <h3>JOB DESCRIPTION</h3>
            <div class="default__text">
              <p>Windows and Linux server’s administration along with Oracle database
                administration. Daily assurance
                of backup copies. Systems monitoring and elimination of possible faults.
                Negotiations and collaboration
                with various communication, internet and other IT service providers. Active
                participation in company’s
                projects and other related system administrator tasks. </p>
            </div>
          </div>
        </div>
        <div class="default__column">
          <div class="default__block">
            <h3>WHAT YOU’LL GET IN RETURN:</h3>
            <div class="default__listing listing">
              <div class="listing__list">
                <div class="listing__item">End-to-end ownership of the processes that you are
                  managing and freedom to
                  initiate new developments</div>
                <div class="listing__item">Extra entertainments and fun within the company</div>
                <div class="listing__item">Ambitious, challenging and like-minded team</div>
                <div class="listing__item">Additional health insurance</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="default__columns">
        <div class="default__column">
          <div class="default__block">
            <h3>WHAT YOU’LL NEED TO SUCCEED:</h3>
            <div class="default__listing listing">
              <div class="listing__list">
                <div class="listing__item">At least 2 years’ experience in system administration;
                </div>
                <div class="listing__item">Good knowledge of Microsoft Windows Server;</div>
                <div class="listing__item">Good knowledge of Linux OS (Ubuntu, Debian, Red Hat);
                </div>
                <div class="listing__item">Experience in Oracle and MS SQL database administration;
                </div>
                <div class="listing__item">Practice in Active Directory, DNS, DHCP;</div>
                <div class="listing__item">Work with Web services: Apache, Tomcat, Nginx;</div>
                <div class="listing__item">Experience in configuring „Load Balance“, HA;</div>
                <div class="listing__item">Work experience with monitoring systems like Zabbix,
                  PRTG;</div>
                <div class="listing__item">Knowledge of server virtualization platforms;</div>
                <div class="listing__item">Knowledge in computer networks (Vlan, IPSec, Port</div>
                <div class="listing__item">Forward) and work experience with Firewalls;</div>
                <div class="listing__item">Good technical knowledge and English reading skills;
                </div>
                <div class="listing__item">Demonstrated ability to work as part of a team. </div>
              </div>
            </div>
            <label class="default__agreement form__item">
              <input type="checkbox" class="form__native" name="agree" data-linkedin-agree required>
              <i class="form__control form__control--checkbox"></i>
              <span class="form__inlabel">
                Patvirtinu, kad pateikti duomenys yra teisingi. Susipažinau ir sutinku su
                <a target="_blank" href="basic">privatumo politika</a>.</span>
            </label>
            <div class="default__actions">
              <a href="" class="default__action btn" data-linkedin-btn>
                <i class="btn__icon btn__icon--left">
                  <?php include '../assets/img/icon--linkedin.svg'; ?></i>
                <span class="btn__text">Apply via Linkedin</span>
              </a>
              <div data-offcanvas-trigger="resume" class="default__action btn btn--narrow">
                <span class="btn__text">Siųsk CV</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php';
