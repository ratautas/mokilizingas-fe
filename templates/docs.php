<?php include '../partials/head.php'; ?>
<main class="app__page app__page--basic basic" data-page>
  <div class="basic__container container">
    <h1>Dokumentai ir nuostatos</h1>
    <div class="basic__downloads">
      <div class="basic__table table">
        <div class="table__top">
          <h3>DOKUMENTAI</h3>
        </div>
        <div class="table__wrapper" data-toggler>
          <div class="table__content">
            <div class="table__row">
              <div class="table__cols">
                <div class="table__col table__col--80">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row">
              <div class="table__cols">
                <div class="table__col table__col--90">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash01">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash02">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Lorem ipsum dolor si amet</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash03">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash04">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
          </div>
        </div>
      </div>
      <div class="basic__table table">
        <div class="table__top">
          <h3>KITI DOKUMENTAI</h3>
        </div>
        <div class="table__wrapper" data-toggler>
          <div class="table__content">
            <div class="table__row table__row--expandable" data-toggler-item="hash05">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash06">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Lorem ipsum dolor si amet</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash07">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash08">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
          </div>
        </div>
      </div>
      <div class="basic__table table">
        <div class="table__top">
          <h3>DAR KITI DOKUMENTAI</h3>
        </div>
        <div class="table__wrapper" data-toggler>
          <div class="table__content">
            <div class="table__row table__row--expandable" data-toggler-item="hash09">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash10">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Lorem ipsum dolor si amet</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash11">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash12">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash13">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash14">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash15">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash16">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash17">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash18">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash19">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
            <div class="table__row table__row--expandable" data-toggler-item="hash20">
              <div class="table__cols" data-toggler-trigger>
                <div class="table__col table__col--50">Asmens duomenų tvarkymo principai</div>
                <div class="table__col table__col--11">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">2018-08-08</div>
                </div>
                <div class="table__col table__col--15">
                  <div class="table__label">Galioja nuo:</div>
                  <div class="table__value">Galiojantis</div>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">PDF</a>
                </div>
                <div class="table__col table__col--10">
                  <a href="" target="_blank" download class="table__download">DOC</a>
                </div>
                <i class="table__expander" data-toggler-trigger></i>
              </div>
              <div class="table__extra" data-toggler-content>I was hidden!</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include '../partials/foot.php';
