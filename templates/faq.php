<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default app__page--faq default" data-page>
  <section class="default__section default__section--header">
    <div class="default__container container">
      <div class="default__header">
        <h1>Dažniausiai užduodami klausimai</h1>
        <div class="default__media media">
          <i class="media__image" style="background-image:url('../media/faq__media.png')"></i>
        </div>
        <div class="default__intro">
          <p> Pateikiame atsakymus į klausimus, kurių dažniausiai sulaukiame
            iš jūsų. Neradote, ko ieškojote? Skambinkite <a href="tel:++370 700 55 888">+370 700 55
              888</a> arba rašykite <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
          </p>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--faq">
    <div class="default__container container">
      <div class="default__faq faq" data-tabs data-faq>
        <aside class="faq__aside" data-sticky>
          <h4>KLAUSIMŲ KATEGORIJOS</h4>
          <div class="faq__categories">
            <div class="faq__category is-active" data-tabs-trigger="paskola">Paskola</div>
            <div class="faq__category" data-tabs-trigger="lizingas">Lizingas</div>
            <div class="faq__category" data-tabs-trigger="kortele">Kortelė</div>
          </div>
        </aside>
        <div class="faq__content">
          <div class="faq__filter filter">
            <input data-faq-input type="text" class="filter__input" placeholder="Įveskite savo klausimą">
            <i class="filter__icon">
              <?php include '../assets/img/icon--search.svg'; ?></i>
            <button class="filter__submit">Ieškoti</button>
          </div>
          <div class="faq__tab is-active" data-tabs-content="paskola" data-toggler>
            <div class="faq__list">
              <div class="faq__item" data-toggler-item="hash01">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra saugaus atsiskaitymo kortele paslauga (3d
                    secure)?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item is-open" data-toggler-item="hash02">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kaip išpirkti elektroninius pinigus iš mokipay piniginės?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash03">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kada gaunate mano įmoką?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash04">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ar už kiekvieną sutartį gausiu atskirą e. sąskaitą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash05">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ką daryti, jei užblokavau mokilizingo kortelę arba
                    pamiršau pin kodą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash06">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ką daryti, jei savitarnoje, mokilizingo kortelės išraše,
                    rodomi neteisingi
                    duomenys?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash07">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kaip keisti mokilizingo kortelės kredito limitą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash08">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra „antigravity payment systems“?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash09">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra lhv?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="faq__tab" data-tabs-content="lizingas" data-toggler>
            <div class="faq__list">
              <div class="faq__item" data-toggler-item="hash11">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra saugaus atsiskaitymo kortele paslauga (3d
                    secure)?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item is-open" data-toggler-item="hash12">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kaip išpirkti elektroninius pinigus iš mokipay piniginės?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash13">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kada gaunate mano įmoką?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash14">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ar už kiekvieną sutartį gausiu atskirą e. sąskaitą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash15">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ką daryti, jei užblokavau mokilizingo kortelę arba
                    pamiršau pin kodą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash16">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ką daryti, jei savitarnoje, mokilizingo kortelės išraše,
                    rodomi neteisingi
                    duomenys?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash17">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kaip keisti mokilizingo kortelės kredito limitą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash18">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra „antigravity payment systems“?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash19">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra lhv?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="faq__tab" data-tabs-content="kortele" data-toggler>
            <div class="faq__list">
              <div class="faq__item" data-toggler-item="hash21">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra saugaus atsiskaitymo kortele paslauga (3d
                    secure)?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item is-open" data-toggler-item="hash22">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kaip išpirkti elektroninius pinigus iš mokipay piniginės?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash23">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kada gaunate mano įmoką?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash24">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ar už kiekvieną sutartį gausiu atskirą e. sąskaitą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash25">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ką daryti, jei užblokavau mokilizingo kortelę arba
                    pamiršau pin kodą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash26">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Ką daryti, jei savitarnoje, mokilizingo kortelės išraše,
                    rodomi neteisingi
                    duomenys?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash27">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kaip keisti mokilizingo kortelės kredito limitą?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash28">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra „antigravity payment systems“?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
              <div class="faq__item" data-toggler-item="hash29">
                <div class="faq__question" data-toggler-trigger>
                  <div class="faq__label">Kas yra lhv?</div>
                  <i class="faq__icon"></i>
                </div>
                <div class="faq__answer" data-toggler-content>
                  <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                    kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="faq__no-results" data-faq-no-results>
            <h3>Nieko nerasta</h3>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--reachout">
    <div class="default__container container">
      <div class="default__support">
        <h2>Neradote ko ieškojote?</h2>
        <div class="default__reachout reachout">
          <div class="reachout__blocks">
            <form class="reachout__form form" action="http://localhost/mokilizingas-fe/endpoints/contact.php">
              <div class="form__content" data-form-content>
                <div class="form__intro">
                  <h4>PARAŠYK MUMS</h4>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">Jūsų vardas*</span>
                    <input type="text" name="name" required class="form__input form__input--white">
                    <span class="form__error">Error</span>
                  </div>
                  <div class="form__item">
                    <span class="form__label">Jūsų vardas*</span>
                    <input type="text" name="name2" required class="form__input form__input--white">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">JūsųEl. paštas*</span>
                    <input type="email" name="email" required class="form__input form__input--white">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">Žinutė</span>
                    <textarea type="text" name="message" rows="7" required class="form__input form__input--white"></textarea>
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__row">
                  <label class="form__item">
                    <input type="checkbox" class="form__native" required>
                    <i class="form__control form__control--white form__control--checkbox"></i>
                    <span class="form__inlabel">
                      Patvirtinu, kad pateikti duomenys yra teisingi. Susipažinau ir sutinku su
                      <a target="_blank" href="basic">privatumo politika</a>.</span>
                    <span class="form__error">Error</span>
                  </label>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <button class="form__submit btn">
                      <i class="btn__icon btn__icon--left">
                        <?php include '../assets/img/icon--mail.svg'; ?>
                      </i>
                      <span class="btn__text">Siųsti klausimą</span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="form__done form__done--success done done--success" data-form-done="success">
                <i class="done__close" data-done-close></i>
                <div class="done__top">
                  <h2>Ačiū.</h2>
                </div>
                <div class="done__text">
                  <p>Su jumis susisieks mūsų konsultantė</p>
                </div>
              </div>
              <div class="form__done form__done--error done done--error" data-form-done="error">
                <i class="done__close" data-done-close></i>
                <div class="done__top">
                  <h2>Klaida!</h2>
                </div>
                <div class="done__text">
                  <p>Su jumis susisieks mūsų konsultantė</p>
                </div>
              </div>
            </form>
            <div class="reachout__consultant consultant">
              <i class="consultant__media" style="background-image:url('../media/consultant__media.png')"></i>
              <div class="consultant__content">
                <h3>ARBA SUSISIEK SU MŪSŲ KONSULTANTE TIESIOGIAI</h3>
                <div class="consultant__contacts">
                  <div class="consultant__contact">
                    <div class="consultant__label">I-V</div>
                    <div class="consultant__value">10:00-17:00</div>
                  </div>
                  <div class="consultant__contact">
                    <div class="consultant__label">VI-VII</div>
                    <div class="consultant__value">nedarbo dienos</div>
                  </div>
                </div>
              </div>
              <div class="consultant__cta btn  btn--white-blue">
                <i class="btn__icon btn__icon--left">
                  <?php include '../assets/img/icon--chat.svg'; ?>
                </i>
                <span class="btn__text">Parašyk mums</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include '../partials/foot.php'; ?>
