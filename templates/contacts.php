<?php include '../partials/head.php'; ?>

<main class="app__page app__page--basic app__page--grey basic" data-page>
  <div class="basic__container container">
    <h1>Kontaktai</h1>
    <div class="basic__contacts contacts">
      <div class="contacts__reachings">
        <div class="contacts__reaching reaching">
          <i class="reaching__icon">
            <?php include '../assets/img/icon--message.svg'; ?></i>
          <div class="reaching__label">KLAUSKITE</div>
          <div class="reaching__content">
            <div class="reaching__row">
              <a href="tel:+370 700 55 888">+370 700 55 888</a>
            </div>
            <div class="reaching__row">
              <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
            </div>
          </div>
        </div>
        <div class="contacts__reaching reaching">
          <i class="reaching__icon">
            <?php include '../assets/img/icon--location.svg'; ?></i>
          <div class="reaching__label">MUS RASITE</div>
          <div class="reaching__content">
            <div class="reaching__row">Kareivių g. 11 B, 09109, Vilnius</div>
          </div>
        </div>
        <div class="contacts__reaching reaching">
          <i class="reaching__icon">
            <?php include '../assets/img/icon--list.svg'; ?></i>
          <div class="reaching__label">REKVIZITAI</div>
          <div class="reaching__content">
            <div class="reaching__row">Įmonės kodas: 124926897</div>
            <div class="reaching__row">PVM kodas: LT24926897</div>
            <div class="reaching__row">Banko sąskaita: LT45 7400 0423 45892 3819</div>
          </div>
        </div>
      </div>
      <div class="contacts__map" data-map data-map-zoom="16" data-map-lng="25.2996064" data-map-lat="54.7177671"></div>
    </div>
    <div class="basic__cards">
      <div class="basic__card card card--third">
        <div class="card__media card__media--third card__media--high media">
          <i class="media__image media__image--cover" style="background-image:url('../media/contacts__media.png')"></i>
        </div>
        <div class="card__container card__container--third">
          <div class="card__content">
            <div class="card__title">Kauno regiono atstovybė</div>
            <div class="card__text">
              <p>Pramonės pr. 4E, 51329, Kaunas</p>
              <p><a href="mailto:i.bankuviene@mokilizingas.lt">i.bankuviene@mokilizingas.lt</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="basic__card card card--third">
        <div class="card__media card__media--third card__media--high media">
          <i class="media__image media__image--cover" style="background-image:url('../media/contacts__media.png')"></i>
        </div>
        <div class="card__container card__container--third">
          <div class="card__content">
            <div class="card__title">Klaipėdos regiono atstovybė</div>
            <div class="card__text">
              <p>Naujojo Sodo g. 1 A (“K centras”), 92118, Klaipėda, 18 aukštas.</p>
              <p><a href="mailto:v.dabasinskas@mokilizingas.lt">v.dabasinskas@mokilizingas.lt</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="basic__card card card--third">
        <div class="card__media card__media--third card__media--high media">
          <i class="media__image media__image--cover" style="background-image:url('../media/contacts__media.png')"></i>
        </div>
        <div class="card__container card__container--third">
          <div class="card__content">
            <div class="card__title">Panėvežio ir Šiaulių regiono atstovybė</div>
            <div class="card__text">
              <p><a href="mailto:d.mickus@mokilizingas.lt">d.mickus@mokilizingas.lt</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="basic__reachout reachout">
    <div class="reachout__container container">
      <div class="reachout__blocks">
        <form class="reachout__form form" action="http://localhost/mokilizingas-fe/endpoints/contact.php">
          <div class="form__content" data-form-content>
            <div class="form__intro">
              <h4>PARAŠYK MUMS</h4>
            </div>
            <div class="form__row">
              <div class="form__item">
                <span class="form__label">Jūsų vardas*</span>
                <input type="text" name="name" required class="form__input form__input--white">
                <span class="form__error">Error</span>
              </div>
              <div class="form__item">
                <span class="form__label">Jūsų vardas*</span>
                <input type="text" name="name2" required class="form__input form__input--white">
                <span class="form__error">Error</span>
              </div>
            </div>
            <div class="form__row">
              <div class="form__item">
                <span class="form__label">JūsųEl. paštas*</span>
                <input type="email" name="email" required class="form__input form__input--white">
                <span class="form__error">Error</span>
              </div>
            </div>
            <div class="form__row">
              <div class="form__item">
                <span class="form__label">Žinutė</span>
                <textarea type="text" name="message" rows="7" required class="form__input form__input--white"></textarea>
                <span class="form__error">Error</span>
              </div>
            </div>
            <div class="form__row">
              <label class="form__item">
                <input type="checkbox" class="form__native" required>
                <i class="form__control form__control--white form__control--checkbox"></i>
                <span class="form__inlabel">
                  Patvirtinu, kad pateikti duomenys yra teisingi. Susipažinau ir sutinku su
                  <a target="_blank" href="basic">privatumo politika</a>.</span>
                <span class="form__error">Error</span>
              </label>
            </div>
            <div class="form__row">
              <div class="form__item">
                <button class="form__submit btn">
                  <i class="btn__icon btn__icon--left">
                    <?php include '../assets/img/icon--mail.svg'; ?>
                  </i>
                  <span class="btn__text">Siųsti klausimą</span>
                </button>
              </div>
            </div>
          </div>
          <div class="form__success success" data-form-success>
            <i class="success__icon success__icon--white"></i>
            <div class="success__text">
              <h2>Forma išsiųsta sėkmingai.</h2>
            </div>
          </div>
          <div class="form__done form__done--success done done--success" data-form-done="success">
            <i class="done__close" data-done-close></i>
            <div class="done__top">
              <h2>Ačiū.</h2>
            </div>
            <div class="done__text">
              <p>Su jumis susisieks mūsų konsultantė</p>
            </div>
          </div>
          <div class="form__done form__done--error done done--error" data-form-done="error">
            <i class="done__close" data-done-close></i>
            <div class="done__top">
              <h2>Klaida!</h2>
            </div>
            <div class="done__text">
              <p>Su jumis susisieks mūsų konsultantė</p>
            </div>
          </div>
        </form>
        <div class="reachout__consultant consultant">
          <i class="consultant__media" style="background-image:url('../media/consultant__media.png')"></i>
          <div class="consultant__content">
            <h3>ARBA SUSISIEK SU MŪSŲ KONSULTANTE TIESIOGIAI</h3>
            <div class="consultant__contacts">
              <div class="consultant__contact">
                <div class="consultant__label">I-V</div>
                <div class="consultant__value">10:00-17:00</div>
              </div>
              <div class="consultant__contact">
                <div class="consultant__label">VI-VII</div>
                <div class="consultant__value">nedarbo dienos</div>
              </div>
            </div>
          </div>
          <div class="consultant__cta btn btn--white-blue">
            <i class="btn__icon btn__icon--left">
              <?php include '../assets/img/icon--chat.svg'; ?>
            </i>
            <span class="btn__text">Parašyk mums</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include '../partials/foot.php'; ?>
