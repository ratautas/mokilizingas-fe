<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--cyan">
    <div class="default__container container">
      <div class="default__socials socials">
        <?php include '../partials/socials.php'; ?>
      </div>
      <div class="default__header default__header--high">
        <h1>Apie mus</h1>
        <div class="default__media default__media--high media">
          <i class="media__image" style="background-image:url('../media/about__media.png')"></i>
        </div>
        <div class="default__intro default__intro--narrow">
          <p>Mėgstu ieškoti naujų iššūkių – geriausiai jaučiuosi ten, kur galiu save išmėginti ir
            tobulėti. Žmonėms, norintiems pasiekti tikslų (ypač naujokams, kurie dar tik įsibėgėja),
            visada patariu nebijoti klausti. Klysti ar kažko nežinoti nėra jokia gėda – ne veltui
            sakoma: “If you’re not prepared to be wrong, you’ll never come up with anyhing
            original.” Jei reikėtų įvardinti, kas esu MOKILIZINGE, vadinčiau save Žonglieriumi –
            rūpinuosi daugybe dalykų vienu metu. Man svarbu, kad komandai MOKILIZINGAS būtų daugiau
            nei darbo vieta. Ir tai pavyksta – nes ten, kur nesijauti įspraustas į rėmus, gali save
            atrasti vis iš naujo.</p>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--about">
    <div class="default__container container">
      <div class="default__about about">
        <div class="about__section">
          <div class="about__media about__media--left media">
            <i class="media__image media__image--cover"
              style="background-image:url('http://mokilizingas-be.devprojects.lt/storage/app/media/image-10.png')"></i>
          </div>
          <div class="about__content about__content--right">
            <h2>Benas Pavlauskas apie Mokilizingą</h2>
            <div class="about__details">
              <div class="about__meta">Pareigos: Mokilizingo vadovas</div>
            </div>
            <div class="about__text">
              <blockquote>"Vienas geriausių patarimų, kuriuos esu gavęs - nepervertink
                patogumo."</blockquote>
              <p>Mėgstu ieškoti naujų iššūkių – geriausiai jaučiuosi ten, kur galiu save išmėginti
                ir tobulėti. Žmonėms, norintiems pasiekti tikslų (ypač naujokams, kurie dar tik
                įsibėgėja), visada patariu nebijoti klausti. Klysti ar kažko nežinoti nėra jokia
                gėda – ne veltui sakoma: “If you’re not prepared to be wrong, you’ll never come up
                with anyhing original.” Jei reikėtų įvardinti, kas esu MOKILIZINGE, vadinčiau save
                Žonglieriumi – rūpinuosi daugybe dalykų vienu metu. Man svarbu, kad komandai
                MOKILIZINGAS būtų daugiau nei darbo vieta. Ir tai pavyksta – nes ten, kur nesijauti
                įspraustas į rėmus, gali save atrasti vis iš naujo.</p>
            </div>
            <div class="about__actions">
              <a href="" target="_blank" rel="noopener noreferrer"
                class="about__action btn btn--ghost">
                <i class="btn__icon btn__icon--left">
                  <?php include '../assets/img/icon--linkedin.svg'; ?></i>
                <span class="btn__text">Linkedin</span>
              </a>
            </div>
          </div>
        </div>
        <div class="about__section">
          <div class="about__media about__media--right media">
            <i class="media__image media__image--cover"
              style="background-image:url('http://mokilizingas-be.devprojects.lt/storage/app/media/image-12.png')"></i>
          </div>
          <div class="about__content about__content--left">
            <h2>Virginija Pipirė apie Mokilizingą</h2>
            <div class="about__details">
              <div class="about__meta">Pareigos: HR vadovė</div>
            </div>
            <div class="about__text">
              <blockquote>"There is nothing good or bad - it's simply how you use it."</blockquote>
              <p>Būtent tokiu požiūriu vadovaujuosi padėdama žmonėms siekti to, kas užsibrėžta. Man
                artimas posakis, kad didesni tikslai įgyvendinami taip pat kaip ir maži – darymo
                būdu. Reikia tiesiog imtis veiksmų – kaip tik to ir stengiuosi įkvėpti visus
                prisijungiančius prie komandos. Laikau save Vibe Manager: man smagiausia matyti,
                kaip komanda progresuoja, susibendrauja ir ima dirbti efektyviai. Rasti tuos, kurie
                kultūriškai tinka komandai, yra mano misija ir didžiausias iššūkis. MOKILIZINGE
                susirinkę entuziastingi žmonės, tarp kurių gera dirbti: bandyti, klysti, mokytis iš
                klaidų, atsiskleisti. Neįsivaizduoju geresnės terpės ieškantiems galimybių augti!
              </p>
            </div>
            <div class="about__actions">
              <a href="" target="_blank" rel="noopener noreferrer"
                class="about__action btn btn--ghost">
                <i class="btn__icon btn__icon--left">
                  <?php include '../assets/img/icon--linkedin.svg'; ?></i>
                <span class="btn__text">Linkedin</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--team">
    <div class="default__container container">
      <h3>MOKILIZINGO KOMANDA</h3>
      <div class="default__cards">
        <div class="default__card card card--third card--team">
          <div class="card__media card__media--third card__media--team media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/member.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Vardas Pavardė</div>
              <div class="card__subtitle">Pareigos pavadinimas</div>
            </div>
            <div class="card__bottom">
              <a href="https://linkedin.com" target="_blank" class="card__trigger">Linkedin</a>
            </div>
          </div>
          <a href="https://linkedin.com" target="_blank" class="card__link"></a>
        </div>
        <div class="default__card card card--third card--team">
          <div class="card__media card__media--third card__media--team media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/member.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Vardas Pavardė</div>
              <div class="card__subtitle">Pareigos pavadinimas</div>
            </div>
            <div class="card__bottom">
              <a href="https://linkedin.com" target="_blank" class="card__trigger">Linkedin</a>
            </div>
          </div>
          <a href="https://linkedin.com" target="_blank" class="card__link"></a>
        </div>
        <div class="default__card card card--third card--team">
          <div class="card__media card__media--third card__media--team media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/member.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Vardas Pavardė</div>
              <div class="card__subtitle">Pareigos pavadinimas</div>
            </div>
            <div class="card__bottom">
              <a href="https://linkedin.com" target="_blank" class="card__trigger">Linkedin</a>
            </div>
          </div>
          <a href="https://linkedin.com" target="_blank" class="card__link"></a>
        </div>
        <div class="default__card card card--third card--team">
          <div class="card__media card__media--third card__media--team media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/member.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Vardas Pavardė</div>
              <div class="card__subtitle">Pareigos pavadinimas</div>
            </div>
            <div class="card__bottom">
              <a href="https://linkedin.com" target="_blank" class="card__trigger">Linkedin</a>
            </div>
          </div>
          <a href="https://linkedin.com" target="_blank" class="card__link"></a>
        </div>
        <div class="default__card card card--third card--team">
          <div class="card__media card__media--third card__media--team media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/member.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Vardas Pavardė</div>
              <div class="card__subtitle">Pareigos pavadinimas</div>
            </div>
            <div class="card__bottom">
              <a href="https://linkedin.com" target="_blank" class="card__trigger">Linkedin</a>
            </div>
          </div>
          <a href="https://linkedin.com" target="_blank" class="card__link"></a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--tabs">
    <div class="default__container container">
      <div class="default__tabs tabs" data-tabs>
        <div class="tabs__top">
          <h2>Mūsų vertybės ir istorija</h2>
          <div class="tabs__media media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/tip__media.png')"></i>
          </div>
          <div class="tabs__triggers">
            <div class="tabs__trigger is-active" tabindex="0" data-tabs-trigger="vertybes">Vertybės
              <i class="tabs__underline"></i>
            </div>
            <div class="tabs__trigger" tabindex="0" data-tabs-trigger="istorija">Istorija</div>
          </div>
        </div>
        <div class="tabs__contents">
          <div class="tabs__content is-active" data-tabs-content="vertybes">
            <div class="tabs__column">
              <div class="tabs__block tabs__block--has-icon">
                <div class="tabs__label">AKTYVUMAS</div>
                <div class="tabs__text">Nuolat judėti, veržtis į priekį, ieškoti naujų galimybių –
                  tai mūsų veiklos pagrindas. Esame orientuoti į rezultatą, vertiname atkaklumą ir
                  kiekvieną klaidą priimame kaip pamoką.</div>
                <i class="tabs__icon" style="background-image:url('../media/value--1.png')"></i>
              </div>
              <div class="tabs__block tabs__block--has-icon">
                <div class="tabs__label">ATVIRUMAS</div>
                <div class="tabs__text">Esame profesionalai, kuriems svarbus kiekvienas komandos
                  narys. Skatiname nuomonių įvairovę, argumentuotas diskusijas ir bendradarbiavimą –
                  tai leidžia siekti aukščiausios kokybės rezultatų. </div>
                <i class="tabs__icon" style="background-image:url('../media/value--2.png')"></i>
              </div>
            </div>
            <div class="tabs__column">
              <div class="tabs__block tabs__block--has-icon">
                <div class="tabs__label">IŠMANUMAS</div>
                <div class="tabs__text">Viena didžiausių mūsų vertybių – aštrus, smalsus protas,
                  nebijantis mesti iššūkio sau ir supančiai aplinkai. Iš savęs reikalaujame
                  profesionalaus požiūrio, dėmesio detalėms bei praktinių sprendimų.</div>
                <i class="tabs__icon" style="background-image:url('../media/value--3.png')"></i>
              </div>
            </div>
          </div>
          <div class="tabs__content" data-tabs-content="istorija">
            <div class="tabs__column">
              <div class="tabs__block">
                <div class="tabs__label">Bendradarbiavimas.</div>
                <div class="tabs__text">Su kolegomis, klientais, verslo partneriais siekiame dirbti
                  lygiavertės
                  partnerystės pagrindu.</div>
              </div>
              <div class="tabs__block">
                <div class="tabs__label">Atsakomybė.</div>
                <div class="tabs__text">Atsakingai įsipareigojame. Jaučiame atsakomybę ne tik už
                  savo veiklą, bet ir
                  už visą aplinką.</div>
              </div>
            </div>
            <div class="tabs__column">
              <div class="tabs__block">
                <div class="tabs__label">Lankstumas.</div>
                <div class="tabs__text">Vertiname tiek įprastą, tiek ir netradicinį požiūrį į
                  sprendimus.</div>
              </div>
              <div class="tabs__block">
                <div class="tabs__label">Greitis.</div>
                <div class="tabs__text">Reaguojame greitai. Ieškome naujų idėjų ir pirmi jas
                  įgyvendiname.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--feedback">
    <div class="default__container container">
      <div class="default__feedback feedback" data-load-more>
        <h2>Atsiliepimai</h2>
        <div class="feedback__intro">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et tristique ipsum,
            vitae malesuada diam. Pellentesque habitant morbi tristique senectus et netus et
            malesuada fames ac turpis egestas. Sed laoreet sit amet nunc id pharetra. Sed nunc
            nulla, aliquet ut vehicula id, sagittis id enim. Morbi sit amet mattis enim. Vivamus ac
            erat sed neque pulvinar imperdiet vitae quisest. Suspendisse potenti.</p>
        </div>
        <div class="feedback__reviews" data-reviews-masonry data-load-more-append>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image"
                  style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
        </div>
        <div class="feedback__more" data-reviews-reveal>
          <div class="feedback__more-label">
            Rodyti daugiau
          </div>
          <div class="feedback__more-dots">
            <i class="feedback__dot feedback__dot--1"></i>
            <i class="feedback__dot feedback__dot--2"></i>
            <i class="feedback__dot feedback__dot--3"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--contacts default__section--grey">
    <div class="default__container container">
      <h2>Kontaktai</h2>
      <div class="default__contacts contacts">
        <div class="contacts__reachings">
          <div class="contacts__reaching reaching">
            <i class="reaching__icon">
              <?php include '../assets/img/icon--message.svg'; ?></i>
            <div class="reaching__label">KLAUSKITE</div>
            <div class="reaching__content">
              <div class="reaching__row">
                <a href="tel:+370 700 55 888">+370 700 55 888</a>
              </div>
              <div class="reaching__row">
                <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </div>
            </div>
          </div>
          <div class="contacts__reaching reaching">
            <i class="reaching__icon">
              <?php include '../assets/img/icon--location.svg'; ?></i>
            <div class="reaching__label">MUS RASITE</div>
            <div class="reaching__content">
              <div class="reaching__row">Kareivių g. 11 B, 09109, Vilnius</div>
            </div>
          </div>
          <div class="contacts__reaching reaching">
            <i class="reaching__icon">
              <?php include '../assets/img/icon--list.svg'; ?></i>
            <div class="reaching__label">REKVIZITAI</div>
            <div class="reaching__content">
              <div class="reaching__row">Įmonės kodas: 124926897</div>
              <div class="reaching__row">PVM kodas: LT24926897</div>
              <div class="reaching__row">Banko sąskaita: LT45 7400 0423 45892 3819</div>
            </div>
          </div>
        </div>
        <div class="contacts__map" data-map data-map-zoom="16" data-map-lng="25.2996064"
          data-map-lat="54.7177671"></div>
      </div>
      <div class="default__cards">
        <div class="default__card card card--third">
          <div class="card__media card__media--third media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/contacts__media.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Kauno regiono atstovybė</div>
              <div class="card__text">
                <p>Pramonės pr. 4E, 51329, Kaunas</p>
                <p><a href="mailto:i.bankuviene@mokilizingas.lt">i.bankuviene@mokilizingas.lt</a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="default__card card card--third">
          <div class="card__media card__media--third media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/contacts__media.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Klaipėdos regiono atstovybė</div>
              <div class="card__text">
                <p>Naujojo Sodo g. 1 A (“K centras”), 92118, Klaipėda, 18 aukštas.</p>
                <p><a href="mailto:v.dabasinskas@mokilizingas.lt">v.dabasinskas@mokilizingas.lt</a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="default__card card card--third">
          <div class="card__media card__media--third media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/contacts__media.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Panėvežio ir Šiaulių regiono atstovybė</div>
              <div class="card__text">
                <p><a href="mailto:d.mickus@mokilizingas.lt">d.mickus@mokilizingas.lt</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php';
