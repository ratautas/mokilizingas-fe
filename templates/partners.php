<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--blue">
    <div class="default__container container">
      <div class="default__socials socials">
        <?php include '../partials/socials.php'; ?>
      </div>
      <div class="default__header">
        <h1>Tapkime partneriais ir siekime tikslų kartu</h1>
        <div class="default__media media">
          <img src="../media/partners__media.png" alt="" class="media__image">
        </div>
        <div class="default__intro">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque elementum bibendum
            velit vel efficitur.
            Suspendisse diam felis, vulputate quis odio et, vehicula dictum turpis. Sed commodo,
            neque ut suscipit
            egestas, tortor sapien maximus eLorem ipsum dolor sit amet, consectetur adipiscing
            elit. Quisque elementum
            bibendum velit vel efficitur. Suspendisse diam felis, vulputate quis odio et, vehicula
            dictum.</p>
        </div>
      </div>
  </section>
  <section class="default__section default__section--columns default__section--blue">
    <div class="default__container container">
      <div class="default__columns">
        <div class="default__column">
          <div class="default__block">
            <div class="default__listing listing">
              <div class="listing__title">Bendradarbiauti paprasta, nes:</div>
              <div class="listing__item">Nereikia nieko diegti – pakanka kompiuterio ar planšetės
                su interneto ryšiu bei spausdintuvo.</div>
              <div class="listing__item">Sistemą kūrėme tokią paprastą, kad ją kiekvienas perprastų
                per porą minučių.</div>
              <div class="listing__item">Reikia pagalbos? Kad jums būtų patogiau, konsultacijas
                teikiame ir savaitgalį.</div>
              <div class="listing__item">Prisijungę prie savitarnos visada žinosite, kada kokias
                sutartis sudarėte ir
                koks jų statusas.</div>
              <div class="listing__item">Esame lankstūs – kiekvienam klientui prideriname optimalų
                variantą.</div>
              <div class="listing__item">Nereikės apsišarvuoti kantrybe – pinigus pervesime labai
                operatyviai.
              </div>
            </div>
          </div>
          <div class="default__block">
            <div class="default__reaching default__reaching--single reaching">
              <i class="reaching__icon">
                <?php include '../assets/img/icon--message.svg'; ?></i>
              <div class="reaching__label">KLAUSKITE</div>
              <div class="reaching__content">
                <div class="reaching__row">
                  <a href="tel:+370 700 55 888">+370 700 55 888</a>
                </div>
                <div class="reaching__row">
                  <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="default__column">
          <form class="default__form form"
            action="http://localhost/mokilizingas-fe/endpoints/contact.php">
            <div class="form__content" data-form-content>
              <div class="form__intro">
                <h3>DIRBKIME KARTU</h3>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Įmonės pavadinimas*</span>
                  <input type="text" name="company" required class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Kontaktinis telefono numeris*</span>
                  <input type="tel" name="tel" required class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Miestas*</span>
                  <input type="text" name="city" required class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">El. paštas*</span>
                  <input type="email" name="email" required class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <button class="form__submit form__submit--partners btn">
                    <i class="btn__icon btn__icon--left">
                      <?php include '../assets/img/icon--phone.svg'; ?>
                    </i>
                    <span class="btn__text">Gauti skambutį</span>
                  </button>
                </div>
              </div>
            </div>
            <div class="form__done form__done--success done done--success" data-form-done="success">
              <i class="done__close" data-done-close></i>
              <div class="done__top">
                <h2>Ačiū.</h2>
              </div>
              <div class="done__text">
                <p>Su jumis susisieks mūsų konsultantė</p>
              </div>
            </div>
            <div class="form__done form__done--error done done--error" data-form-done="error">
              <i class="done__close" data-done-close></i>
              <div class="done__top">
                <h2>Klaida!</h2>
              </div>
              <div class="done__text">
                <p>Su jumis susisieks mūsų konsultantė</p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php';
