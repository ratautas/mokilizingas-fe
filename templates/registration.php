<?php include '../partials/head--user.php'; ?>
<main class="app__page app__page--user user" data-page>
  <div class="user__container container">
    <div class="user__top">
      <h3>REGISTRUOTIS</h3>
      <div class="user__already already">
        <div class="already__label">Turite paskyrą?</div>
        <a href="login" class="already__trigger">Prisijunkite</a>
      </div>
    </div>
    <div class="user__content">
      <form class="user__form form" action="http://localhost/mokilizingas-fe/endpoints/contact.php">
        <div class="form__content form__content--register" data-form-content>
          <div class="form__row">
            <div class="form__item">
              <span class="form__label">Jūsų vardas*</span>
              <input type="text" name="register-name" required class="form__input">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <div class="form__item">
              <span class="form__label">Jūsų pavardė*</span>
              <input type="text" name="register-surname" required class="form__input">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <div class="form__item">
              <span class="form__label">Asmens kodas</span>
              <input type="text" name="register-code" pattern="^.[0-9]{10,10}$" required
                class="form__input">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <div class="form__item">
              <span class="form__label">Jūsų el. paštas*</span>
              <input type="email" name="register-email" required class="form__input">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <div class="form__item">
              <span class="form__label">Slaptažodis</span>
              <input type="password" name="register-password" minlength="8" required
                class="form__input">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <div class="form__item">
              <span class="form__label">Pakartokite slaptažodį</span>
              <input type="password" name="register-password2" data-match="register-password"
                required class="form__input">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <label class="form__item">
              <input type="checkbox" class="form__native" required>
              <i class="form__control form__control--checkbox"></i>
              <span class="form__inlabel">
                Patvirtinu, kad pateikti duomenys yra teisingi. Susipažinau ir sutinku su
                <a target="_blank" href="basic">privatumo politika</a>.</span>
              <span class="form__error">Error</span>
            </label>
          </div>
          <div class="form__row">
            <div class="form__item">
              <button class="form__submit btn btn--full">
                <span class="btn__text">Tęsti</span>
              </button>
            </div>
          </div>
        </div>
        <div class="form__done form__done--success done done--success" data-form-done="success">
          <i class="done__close" data-done-close></i>
          <div class="done__top">
            <h2>Ačiū.</h2>
          </div>
          <div class="done__text">
            <p>Su jumis susisieks mūsų konsultantė</p>
          </div>
        </div>
        <div class="form__done form__done--error done done--error" data-form-done="error">
          <i class="done__close" data-done-close></i>
          <div class="done__top">
            <h2>Klaida!</h2>
          </div>
          <div class="done__text">
            <p>Su jumis susisieks mūsų konsultantė</p>
          </div>
        </div>
      </form>
    </div>
  </div>
  <?php include '../partials/foot--user.php'; ?>
