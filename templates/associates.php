<?php include '../partials/head.php'; ?>
<main class="app__page app__page--basic basic" data-page>
  <div class="basic__container container">
    <h1>Partnerių sąrašas</h1>
    <div class="basic__assoc assoc" data-assoc>
      <div class="assoc__top">
        <form class="assoc__filter filter custom-submit">
          <input data-assoc-input type="text" class="filter__input filter__input--blue"
            placeholder="Partnerio pavadinimas, įmonės kodas..." name="q">
          <i class="filter__icon">
            <?php include '../assets/img/icon--search.svg'; ?></i>
          <button class="filter__submit" type="submit">Ieškoti</button>
        </form>
        <div class="assoc__section assoc__section--head assoc__section--desktop">
          <div class="assoc__col assoc__col--18">
            <div class="assoc__label">Įmonės kodas</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__label">Partnerio pavadinimas</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__label">Adresas</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__label">Telefonas</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__label">El.paštas</div>
          </div>
          <div class="assoc__col assoc__col--10">
            <div class="assoc__label">Interneto svetainė</div>
          </div>
        </div>
      </div>
      <div class="assoc__sections">
        <div class="assoc__section assoc__section--desktop" data-assoc-item>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>303003620</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value">Dainavos g. 1-8, Raudondvario k., Kauno r. sav</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
          </div>
          <div class="assoc__col assoc__col--10">
            <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
              <?php include '../assets/img/icon--web.svg'; ?>
            </a>
          </div>
        </div>
        <div class="assoc__section assoc__section--mobile" data-assoc-item>
          <div class="assoc__row">
            <div class="assoc__col assoc__col--44">
              <div class="assoc__label">Įmonės kodas</div>
              <div class="assoc__value" data-assoc-value>303003620</div>
            </div>
            <div class="assoc__col assoc__col--56">
              <div class="assoc__label">Telefonas</div>
              <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Partnerio pavadinimas</div>
              <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">El.paštas</div>
              <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Interneto svetainė</div>
              <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
                <?php include '../assets/img/icon--web.svg'; ?>
              </a>
            </div>
          </div>
        </div>
        <div class="assoc__section assoc__section--desktop" data-assoc-item>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>303003620</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value">Dainavos g. 1-8, Raudondvario k., Kauno r. sav</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
          </div>
          <div class="assoc__col assoc__col--10">
            <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
              <?php include '../assets/img/icon--web.svg'; ?>
            </a>
          </div>
        </div>
        <div class="assoc__section assoc__section--mobile" data-assoc-item>
          <div class="assoc__row">
            <div class="assoc__col assoc__col--44">
              <div class="assoc__label">Įmonės kodas</div>
              <div class="assoc__value" data-assoc-value>303003620</div>
            </div>
            <div class="assoc__col assoc__col--56">
              <div class="assoc__label">Telefonas</div>
              <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Partnerio pavadinimas</div>
              <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">El.paštas</div>
              <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Interneto svetainė</div>
              <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
                <?php include '../assets/img/icon--web.svg'; ?>
              </a>
            </div>
          </div>
        </div>
        <div class="assoc__section assoc__section--desktop" data-assoc-item>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>303003620</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value">Dainavos g. 1-8, Raudondvario k., Kauno r. sav</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
          </div>
          <div class="assoc__col assoc__col--10">
            <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
              <?php include '../assets/img/icon--web.svg'; ?>
            </a>
          </div>
        </div>
        <div class="assoc__section assoc__section--mobile" data-assoc-item>
          <div class="assoc__row">
            <div class="assoc__col assoc__col--44">
              <div class="assoc__label">Įmonės kodas</div>
              <div class="assoc__value" data-assoc-value>303003620</div>
            </div>
            <div class="assoc__col assoc__col--56">
              <div class="assoc__label">Telefonas</div>
              <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Partnerio pavadinimas</div>
              <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">El.paštas</div>
              <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Interneto svetainė</div>
              <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
                <?php include '../assets/img/icon--web.svg'; ?>
              </a>
            </div>
          </div>
        </div>
        <div class="assoc__section assoc__section--desktop" data-assoc-item>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>303003620</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value">Dainavos g. 1-8, Raudondvario k., Kauno r. sav</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
          </div>
          <div class="assoc__col assoc__col--10">
            <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
              <?php include '../assets/img/icon--web.svg'; ?>
            </a>
          </div>
        </div>
        <div class="assoc__section assoc__section--mobile" data-assoc-item>
          <div class="assoc__row">
            <div class="assoc__col assoc__col--44">
              <div class="assoc__label">Įmonės kodas</div>
              <div class="assoc__value" data-assoc-value>303003620</div>
            </div>
            <div class="assoc__col assoc__col--56">
              <div class="assoc__label">Telefonas</div>
              <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Partnerio pavadinimas</div>
              <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">El.paštas</div>
              <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Interneto svetainė</div>
              <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
                <?php include '../assets/img/icon--web.svg'; ?>
              </a>
            </div>
          </div>
        </div>
        <div class="assoc__section assoc__section--desktop" data-assoc-item>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>303003620</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value">Dainavos g. 1-8, Raudondvario k., Kauno r. sav</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
          </div>
          <div class="assoc__col assoc__col--10">
            <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
              <?php include '../assets/img/icon--web.svg'; ?>
            </a>
          </div>
        </div>
        <div class="assoc__section assoc__section--mobile" data-assoc-item>
          <div class="assoc__row">
            <div class="assoc__col assoc__col--44">
              <div class="assoc__label">Įmonės kodas</div>
              <div class="assoc__value" data-assoc-value>303003620</div>
            </div>
            <div class="assoc__col assoc__col--56">
              <div class="assoc__label">Telefonas</div>
              <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Partnerio pavadinimas</div>
              <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">El.paštas</div>
              <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Interneto svetainė</div>
              <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
                <?php include '../assets/img/icon--web.svg'; ?>
              </a>
            </div>
          </div>
        </div>
        <div class="assoc__section assoc__section--desktop" data-assoc-item>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>303003620</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value">Dainavos g. 1-8, Raudondvario k., Kauno r. sav</div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
          </div>
          <div class="assoc__col assoc__col--18">
            <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
          </div>
          <div class="assoc__col assoc__col--10">
            <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
              <?php include '../assets/img/icon--web.svg'; ?>
            </a>
          </div>
        </div>
        <div class="assoc__section assoc__section--mobile" data-assoc-item>
          <div class="assoc__row">
            <div class="assoc__col assoc__col--44">
              <div class="assoc__label">Įmonės kodas</div>
              <div class="assoc__value" data-assoc-value>303003620</div>
            </div>
            <div class="assoc__col assoc__col--56">
              <div class="assoc__label">Telefonas</div>
              <div class="assoc__value"><a href="tel:(41) 455545">(41) 455545</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Partnerio pavadinimas</div>
              <div class="assoc__value" data-assoc-value>2DG Projects, MB</div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">El.paštas</div>
              <div class="assoc__value"><a href="mailto:info@info.lt">info@info.lt</a></div>
            </div>
          </div>
          <div class="assoc__row">
            <div class="assoc__col">
              <div class="assoc__label">Interneto svetainė</div>
              <a class="assoc__icon" href="http://www.irsiluma.lt/" target="_blank">
                <?php include '../assets/img/icon--web.svg'; ?>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="assoc__pagination pagination" data-assoc-pagination>
        <div class="pagination__container">
          <div class="pagination__nav pagination__nav--prev"></div>
          <div class="pagination__items">
            <a href="" class="pagination__item">1</a>
            <a href="" class="pagination__item">2</a>
            <a href="" class="pagination__item">3</a>
            <a href="" class="pagination__item">4</a>
            <a href="" class="pagination__item pagination__item--current">5</a>
            <a href="" class="pagination__item">6</a>
            <span href="" class="pagination__item">...</span>
            <a href="" class="pagination__item">23</a>
          </div>
          <div class="pagination__nav pagination__nav--next"></div>
        </div>
      </div>
      <div class="assoc__no-results" data-assoc-no-results>No results:(</div>
    </div>
  </div>
  <?php include '../partials/foot.php';
