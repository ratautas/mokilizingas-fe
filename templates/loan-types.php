<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--pink">
    <div class="default__container default__container--with-wizard container">
      <div class="default__header default__header--high">
        <h2>Paskola</h2>
        <div class="default__tag">Palūkanos tik nuo 2 %</div>
        <div class="default__media default__media--list-right media">
          <div class="media__image media__image--sequence sequence" data-sequence="../media/sequences/kiaule_sprite.png"
            data-sequence-fps="24" data-sequence-frames="34">
            <img src="../media/sequences/kiaule.png" alt="" class="sequence__placeholder"
              data-sequence-placeholder>
            <canvas class="sequence__canvas" data-sequence-canvas></canvas>
          </div>
        </div>
        <div class="default__intro default__intro--list-left">
          <p>Kad ir kaip gerai planuotumėte savo finansus, kartais kiekvienam pasitaiko nenumatytų
            išlaidų. Tegul netikėtumai neišmuša iš vėžių – MOKILIZINGO paskola sukurta tam, kad
            Jums padėtų.
            Išsirinkite labiausiai tinkantį paskolos tipą.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque elementum bibendum
            velit vel efficitur. Suspendisse diam felis, vulputate quis odio et, vehicula dictum
            turpis. Sed commodo,
            neque ut suscipit egestas, tortor sapien maximus </p>
        </div>
      </div>
    </div>
  </section>
  <div class="default__wizard default__wizard--loans wizard">
    <?php include '../partials/wizard--v1.php'; ?>
    <div class="wizard__socials socials">
      <?php include '../partials/socials.php'; ?>
    </div>
  </div>
  <section class="default__section default__section--header default__section--orange">
    <div class="default__container container">
      <div class="default__header default__header--high default__header--list-right">
        <h2>Paskola automobiliui</h2>
        <div class="default__tag">Palūkanos tik nuo 4,9 %</div>
        <div class="default__media default__media--list-left media">
          <div class="media__image media__image--sequence sequence" data-sequence="../media/sequences/car_sprite.png"
            data-sequence-fps="24" data-sequence-frames="10">
            <img src="../media/sequences/car.png" alt="" class="sequence__placeholder"
              data-sequence-placeholder>
            <canvas class="sequence__canvas" data-sequence-canvas></canvas>
          </div>
        </div>
        <div class="default__intro default__intro--list-right">
          <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti seną
            automobilį?
            Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras sąlygas.
            Nepamirškite įvertinti
            visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir sezoninį padangų
            keitimą.</p>
        </div>
        <div class="default__listing default__listing--head listing">
          <div class="listing__list listing__list--head">
            <div class="listing__item">Nėra sutarties sudarymo mokesčio</div>
            <div class="listing__item">Palūkanos tik nuo 4,9 %</div>
            <div class="listing__item">Suma nuo 500 iki 15 000 Eur</div>
            <div class="listing__item">12 – 72 mėn. laikotarpiui</div>
            <div class="listing__item">Nereikia pradinio įnašo ir Kasko draudimo</div>
            <div class="listing__item">Atsakymas dėl paskolos per kelias minutes, o pinigai Jus
              pasieks per 30 min.
            </div>
          </div>
        </div>
        <div class="default__actions default__actions--types">
          <a href="loan" class="default__action btn btn--white btn--narrow">
            <span class="btn__text">Plačiau</span>
          </a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--header default__section--yellow">
    <div class="default__container container">
      <div class="default__header default__header--high">
        <h2>Vartojimo paskola</h2>
        <div class="default__tag">Palūkanos tik nuo 2 %</div>
        <div class="default__media default__media--list-right media">
          <div class="media__image media__image--sequence sequence" data-sequence="../media/sequences/kuprine_sprite.png"
            data-sequence-fps="24" data-sequence-frames="41">
            <img src="../media/sequences/kuprine.png" alt="" class="sequence__placeholder"
              data-sequence-placeholder>
            <canvas class="sequence__canvas" data-sequence-canvas></canvas>
          </div>
        </div>
        <div class="default__intro default__intro--list-left">
          <p>Norite išeiti pelnytų atostogų, bet atsidarę piniginę persigalvojate? Planuojate
            atsakingiau rūpintis
            sveikata, bet vis atidedate tai rytojui? Negaiškite laiko – MOKILIZINGO vartojimo
            paskola leis tikslus
            pasiekti jau dabar. Skoliname atsakingai, įvertindami Jūsų finansines galimybes, tačiau
            kam naudosite
            paskolą, sprendžiate vien tik Jūs.</p>
        </div>
        <div class="default__listing default__listing--head listing">
          <div class="listing__list listing__list--head">
            <div class="listing__item">Palūkanos tik nuo 2 %</div>
            <div class="listing__item">Suma nuo 100 iki 15 000 Eur</div>
            <div class="listing__item">3 – 72 mėn. laikotarpiui</div>
            <div class="listing__item">Galimybė grąžinti kreditą anksčiau</div>
            <div class="listing__item">Atsakymas dėl paskolos per kelias minutes</div>
            <div class="listing__item">Pinigai Jus pasieks per 30 min., jei sutartį sudarysite iki
              20 val.
            </div>
          </div>
        </div>
        <div class="default__actions default__actions--types">
          <a href="loan" class="default__action btn btn--white btn--narrow">
            <span class="btn__text">Plačiau</span>
          </a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--header default__section--blue">
    <div class="default__container container">
      <div class="default__header default__header--list-right default__header--high">
        <h2>Paskola būsto remontui</h2>
        <div class="default__tag">Palūkanos tik nuo 2 %</div>
        <div class="default__media default__media--list-left media">
          <div class="media__image media__image--sequence sequence" data-sequence="../media/sequences/graztas_sprite.png"
            data-sequence-fps="24" data-sequence-frames="20">
            <img src="../media/sequences/graztas.png" alt="" class="sequence__placeholder"
              data-sequence-placeholder>
            <canvas class="sequence__canvas" data-sequence-canvas></canvas>
          </div>
        </div>
        <div class="default__intro default__intro--list-right">
          <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti seną
            automobilį?
            Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras sąlygas.
            Nepamirškite įvertinti
            visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir sezoninį padangų
            keitimą.</p>
        </div>
        <div class="default__listing default__listing--head listing">
          <div class="listing__list listing__list--head">
            <div class="listing__item">Palūkanos tik nuo 2 %</div>
            <div class="listing__item">Suma nuo 2 000 iki 15 000 Eur</div>
            <div class="listing__item">24 – 72 mėn. laikotarpiui</div>
            <div class="listing__item">Galimybė grąžinti kreditą anksčiau</div>
            <div class="listing__item">Atsakymas dėl paskolos per kelias minutes</div>
            <div class="listing__item">Pinigai Jus pasieks per 30 min., jei sutartį sudarysite iki
              20 val.</div>
          </div>
        </div>
        <div class="default__actions default__actions--types">
          <a href="loan" class="default__action btn btn--white btn--narrow">
            <span class="btn__text">Plačiau</span>
          </a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--plain default__section--grey">
    <div class="default__container container">
      <h4>SĄLYGOS</h4>
      <div class="default__text">
        <p>Pavyzdžiui, skolinantis 300 €, kai sutartis sudaroma 36 mėn. terminui, metinė palūkanų
          norma – 6,50 %,
          mėnesio sutarties mokestis – 0,50 %, BVKKMN – 18,40 %, bendra mokėtina suma – 385,03 €,
          mėnesio įmoka – 10,69
          €. </p>
        <p>Atsižvelgiant į lizingo bendrovės atliktą Jūsų kreditingumo ir rizikos vertinimą ar
          pasirinkus kitokį
          sutarties terminą bei įmokų mokėjimo dieną, Jums gali būti pasiūlytos ir kitokios sąlygos
          (pavyzdžiui,
          metinės palūkanų normos arba mėnesio sutarties mokesčio dydis).</p>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php'; ?>
