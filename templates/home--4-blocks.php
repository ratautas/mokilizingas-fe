<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--hero">
    <!-- <div class="default__hero hero" data-swiper="hero" data-swiper-autoplay="50000" data-swiper-effect="fade" -->
    <div class="default__hero hero" data-swiper="hero" data-swiper-effect="fade">
      <div class="hero__slider swiper-container" data-swiper-target>
        <div class="hero__wrapper swiper-wrapper">
          <div class="hero__slide hero__slide--yellow swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Kiekvienam pasitaiko nenumatytų išlaidų.</h1>
                <div class="default__intro default__intro--narrow">
                  <p>Negaiškite laiko – MOKILIZINGO vartojimo paskola leis tikslus pasiekti jau
                    dabar.</p>
                </div>
                <div class="default__actions">
                  <a href="" class="default__action btn btn--narrow btn--white-ghost">
                    <span class="btn__text">Plačiau</span>
                  </a>
                </div>
                <div class="default__media default__media--high media">
                  <div class="media__image media__image--sequence sequence"
                    data-sequence="../media/sequences/lagaminas/sprite.png"
                    data-sequence-frames="40">
                    <img src="../media/sequences/lagaminas/Kuprine_Spin%20C%20raw_00001.png" alt=""
                      class="sequence__placeholder" data-sequence-placeholder>
                    <canvas class="sequence__canvas" data-sequence-canvas></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hero__slide hero__slide--orange swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Kiekvienam pasitaiko nenumatytų išlaidų.</h1>
                <div class="default__intro default__intro--narrow">
                  <p>Negaiškite laiko – MOKILIZINGO vartojimo paskola leis tikslus pasiekti jau
                    dabar.</p>
                </div>
                <div class="default__actions">
                  <a href="" class="default__action btn btn--narrow btn--white-ghost">
                    <span class="btn__text">Plačiau</span>
                  </a>
                </div>
                <div class="default__media default__media--high media">
                  <img src="../media/loan__media--2.png" alt=""
                    class="media__image media__image--cover">
                </div>
              </div>
            </div>
          </div>
          <div class="hero__slide hero__slide--cyan swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Paskola automobiliui</h1>
                <div class="default__tag">Palūkanos tik nuo 4,9 %</div>
                <div class="default__intro default__intro--narrow">
                  <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                    seną automobilį?
                    Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras
                    sąlygas. Nepamirškite
                    įvertinti
                    visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir sezoninį
                    padangų keitimą.</p>
                </div>
                <div class="default__listing default__listing--head listing">
                  <div class="listing__list listing__list--head">
                    <div class="listing__item">Nėra sutarties sudarymo mokesčio</div>
                    <div class="listing__item">Palūkanos tik nuo 4,9 %</div>
                    <div class="listing__item">Suma nuo 500 iki 15 000 Eur</div>
                    <div class="listing__item">12 – 72 mėn. laikotarpiui</div>
                    <div class="listing__item">Nereikia pradinio įnašo ir Kasko draudimo</div>
                    <div class="listing__item">Atsakymas dėl paskolos per kelias minutes, o pinigai
                      Jus pasieks per 30
                      min.
                    </div>
                  </div>
                </div>
                <div class="default__media default__media--high media">
                  <img src="../media/loan__media--2.png" alt=""
                    class="media__image media__image--cover">
                </div>
              </div>
            </div>

          </div>
          <div class="hero__slide hero__slide--yellow swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Paskola automobiliui</h1>
                <div class="default__tag">Palūkanos tik nuo 4,9 %</div>
                <div class="default__intro default__intro--narrow">
                  <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                    seną automobilį?
                    Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras
                    sąlygas. Nepamirškite
                    įvertinti visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir
                    sezoninį padangų keitimą.</p>
                </div>
                <div class="default__listing default__listing--head listing">
                  <div class="listing__list listing__list--head">
                    <div class="listing__item">Nėra sutarties sudarymo mokesčio</div>
                    <div class="listing__item">Palūkanos tik nuo 4,9 %</div>
                    <div class="listing__item">Suma nuo 500 iki 15 000 Eur</div>
                    <div class="listing__item">12 – 72 mėn. laikotarpiui</div>
                    <div class="listing__item">Nereikia pradinio įnašo ir Kasko draudimo</div>
                    <div class="listing__item">Atsakymas dėl paskolos per kelias minutes, o pinigai
                      Jus pasieks per 30
                      min.
                    </div>
                  </div>
                </div>
                <div class="default__media default__media--high media">
                  <img src="../media/loan__media--2.png" alt=""
                    class="media__image media__image--cover">
                </div>
              </div>
            </div>

          </div>
          <div class="hero__slide hero__slide--orange swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Paskola automobiliui</h1>
                <div class="default__tag">Palūkanos tik nuo 4,9 %</div>
                <div class="default__intro default__intro--narrow">
                  <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                    seną automobilį?
                    Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras
                    sąlygas. Nepamirškite
                    įvertinti
                    visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir sezoninį
                    padangų keitimą.</p>
                </div>
                <div class="default__listing default__listing--head listing">
                  <div class="listing__list listing__list--head">
                    <div class="listing__item">Nėra sutarties sudarymo mokesčio</div>
                    <div class="listing__item">Palūkanos tik nuo 4,9 %</div>
                    <div class="listing__item">Suma nuo 500 iki 15 000 Eur</div>
                    <div class="listing__item">12 – 72 mėn. laikotarpiui</div>
                    <div class="listing__item">Nereikia pradinio įnašo ir Kasko draudimo</div>
                    <div class="listing__item">Atsakymas dėl paskolos per kelias minutes, o pinigai
                      Jus pasieks per 30
                      min.
                    </div>
                  </div>
                </div>
                <div class="default__media default__media--high media">
                  <img src="../media/loan__media--2.png" alt=""
                    class="media__image media__image--cover">
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="hero__bullets swiper-pagination" data-swiper-bullets>
          <i class="hero__bullet is-active" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
        </div>
      </div>
    </div>
  </section>
  <div class="default__wizard wizard">
    <?php // include '../partials/wizard--v1.php';?>
    <div class="wizard__wrapper" data-wizard="">
      <form target="_blank" class="wizard__form form custom-submit" data-wizard-form=""
        action="http://mokilizingas-be.devprojects.lt/endpoint/loan" novalidate="true">
        <div class="form__content wizard__step wizard__step--1 is-active" data-wizard-step="1">
          <div class="wizard__content">
            <div data-wizard-type="1" data-wizard-id="2"
              data-wizard-type-label="Paskola automobiliui" data-wizard-min="500"
              data-wizard-max="15000"
              data-wizard-terms="12,15,9,6,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,72"
              data-wizard-default-term="12" data-wizard-default-amount="500"></div>
            <div data-wizard-type="2" data-wizard-id="1" data-wizard-type-label="Vartojimo paskola"
              data-wizard-min="300" data-wizard-max="15000"
              data-wizard-terms="24,27,30,33,6,36,39,42,45,48,51,54,57,60,63,66,69,60,63,66,69,36,39,42,45,12,72,48,51,54,57,24,27,30,33,12,3,6,9,72,9,15,18,21,15,18,21,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72"
              data-wizard-default-term="12" data-wizard-default-amount="300"></div>
            <div data-wizard-type="3" data-wizard-id="3"
              data-wizard-type-label="Paskola būsto remontui" data-wizard-min="300"
              data-wizard-max="15000"
              data-wizard-terms="24,18,21,15,12,9,6,3,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72"
              data-wizard-default-term="12" data-wizard-default-amount="300"></div>
            <div class="form__row">
              <div class="form__item has-value">
                <div class="choices is-blue" data-type="select-one" role="listbox" tabindex="0"
                  aria-haspopup="true" aria-expanded="false" dir="ltr">
                  <div class="choices__inner"><select name="wizard-type"
                      class="form__select choices__input is-hidden has-value" data-wizard-select=""
                      data-dropdown="" tabindex="-1" aria-hidden="true" data-choice="active">
                      <option value="1" selected="">Paskola automobiliui</option>
                    </select>
                    <div class="choices__list choices__list--single">
                      <div class="choices__item choices__item--selectable" data-item="" data-id="1"
                        data-value="1" aria-selected="true">
                        Paskola automobiliui
                      </div>
                    </div>
                  </div>
                  <div class="choices__list choices__list--dropdown" aria-expanded="false">
                    <div class="choices__list" dir="ltr" role="listbox">
                      <div
                        class="choices__item choices__item--choice choices__item--selectable is-highlighted"
                        data-select-text="" data-choice="" data-id="1" data-value="1"
                        data-choice-selectable="" id="choices--wizard-type-9v-item-choice-1"
                        role="option" aria-selected="true">
                        Paskola automobiliui
                      </div>
                      <div class="choices__item choices__item--choice choices__item--selectable"
                        data-select-text="" data-choice="" data-id="2" data-value="2"
                        data-choice-selectable="" id="choices--wizard-type-9v-item-choice-2"
                        role="option">
                        Vartojimo paskola
                      </div>
                      <div class="choices__item choices__item--choice choices__item--selectable"
                        data-select-text="" data-choice="" data-id="3" data-value="3"
                        data-choice-selectable="" id="choices--wizard-type-9v-item-choice-3"
                        role="option">
                        Paskola būsto remontui
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form__row">
              <div class="form__item">
                <span class="form__label">Įveskite norimą paskolos sumą</span>
                <div class="form__help">
                  <span data-wizard-range-min="">500&nbsp;€</span>
                  &nbsp;-&nbsp;
                  <span data-wizard-range-max="">15000&nbsp;€</span>
                </div>
                <input type="number" inputmode="decimal" pattern="[0-9]*" name="wizard-amount-input" required="" class="form__input">
                <span class="form__error"></span>
              </div>
            </div>
            <div class="form__row">
              <div class="form__item">
                <div class="form__range form__range--wizard range">
                  <div class="range__target noUi-target noUi-ltr noUi-horizontal"
                    data-wizard-range="">
                    <div class="noUi-base">
                      <div class="noUi-connects"></div>
                      <div class="noUi-origin"
                        style="transform: translate(-100%, 0px); z-index: 4;">
                        <div class="noUi-handle noUi-handle-lower noUi-handle--wizard"
                          data-handle="0" tabindex="0" role="slider" aria-orientation="horizontal"
                          aria-valuemin="500.0" aria-valuemax="15000.0" aria-valuenow="500.0"
                          aria-valuetext="500&nbsp;€">
                          <div class="noUi-touch-area"></div>
                          <div class="noUi-tooltip noUi-tooltip--wizard">500&nbsp;€</div><i
                            class="noUi-handle-icon noUi-handle-icon--wizard"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="wizard-amount" data-wizard-amount="" tabindex="-1"
                  value="500">
              </div>
            </div>
            <div class="form__row">
              <div class="form__term term">
                <span class="form__label">Pasirinkite grąžinimo terminą</span>
                <label class="term__custom form__item">
                  <span class="term__enter">Įrašyti savo terminą</span>
                  <span class="term__placeholder">mėn.</span>
                  <input type="number" inputmode="decimal" pattern="[0-9]*" name="wizard-term" data-wizard-term-input=""
                    class="term__input form__input" min="1">
                </label>
                <div class="term__wrapper ps ps--active-y" data-wizard-scroll="">
                  <div class="term__scroller">
                    <label class="term__item is-disabled">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native is-disabled" value="3">
                      <span class="term__label">3 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="6">
                      <span class="term__label">6 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="9">
                      <span class="term__label">9 mėn.</span>
                    </label>
                    <label class="term__item is-featured has-value">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native is-featured has-value" value="12"
                        checked="checked">
                      <span class="term__label">12 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="15">
                      <span class="term__label">15 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="18">
                      <span class="term__label">18 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="21">
                      <span class="term__label">21 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="24">
                      <span class="term__label">24 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="27">
                      <span class="term__label">27 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="30">
                      <span class="term__label">30 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="33">
                      <span class="term__label">33 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="36">
                      <span class="term__label">36 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="39">
                      <span class="term__label">39 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="42">
                      <span class="term__label">42 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="45">
                      <span class="term__label">45 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="48">
                      <span class="term__label">48 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="51">
                      <span class="term__label">51 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="54">
                      <span class="term__label">54 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="57">
                      <span class="term__label">57 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="60">
                      <span class="term__label">60 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="63">
                      <span class="term__label">63 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="66">
                      <span class="term__label">66 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="69">
                      <span class="term__label">69 mėn.</span>
                    </label>
                    <label class="term__item">
                      <input type="radio" name="wizard-term-radio" data-wizard-term-radio=""
                        class="term__radio form__native" value="72">
                      <span class="term__label">72 mėn.</span>
                    </label>
                  </div>
                  <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                  </div>
                  <div class="ps__rail-y" style="top: 0px; height: 110px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 15px;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="wizard__footer">
            <div class="wizard__notify">Pinigus pervesime per 30 min., jei paskolos sutartį
              sudarysite iki 20 val.</div>
            <div class="wizard__btn btn" data-wizard-result-trigger="">
              <span class="btn__text">Skaičiuoti įmoką</span>
              <i class="btn__icon btn__icon--right">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5.1997 8.9849">
                  <title>icon--caret</title>
                  <polygon
                    points="0.707 8.985 0 8.278 3.786 4.493 0 0.707 0.707 0 5.2 4.493 0.707 8.985"
                    fill="#fff"></polygon>
                </svg> </i>
            </div>
          </div>
        </div>
        <div class="form__content form__content--no-flip wizard__step wizard__step--2"
          data-wizard-step="2">
          <div class="wizard__content">
            <div class="wizard__summary summary">
              <div class="summary__back" data-wizard-update-trigger="">Keisti</div>
              <div class="summary__title" data-wizard-summary-type="">Paskola automobiliui</div>
              <div class="summary__table">
                <div class="summary__row">
                  <div class="summary__col summary__col--label">Paskolos suma:</div>
                  <div class="summary__col summary__col--val"><span
                      data-wizard-summary-amount=""></span>&nbsp;€</div>
                </div>
                <div class="summary__row">
                  <div class="summary__col summary__col--label">Grąžinimo terminas:</div>
                  <div class="summary__col summary__col--val"><span
                      data-wizard-summary-term=""></span>&nbsp;mėn.</div>
                </div>
                <div class="summary__row summary__row--bold">
                  <div class="summary__col summary__col--label">Mėnesio įmoka:</div>
                  <div class="summary__col summary__col--val"><span
                      data-wizard-summary-monthly=""></span>&nbsp;€</div>
                </div>
              </div>
            </div>
          </div>
          <div class="wizard__footer">
            <div class="wizard__footnote">
              <p>Pavyzdžiui, skolinantis <span data-wizard-example-amount=""></span>&nbsp;€, kai
                sutartis sudaroma <span data-wizard-example-term=""></span> mėn. terminui, metinė
                palūkanų norma – <span data-wizard-example-yearly=""></span>, mėnesio sutarties
                mokestis – <span data-wizard-example-fee=""></span>, BVKKMN – <span
                  data-wizard-example-bvkkmn=""></span>, bendra mokėtina suma – <span
                  data-wizard-example-total=""></span>, mėnesio įmoka – <span
                  data-wizard-example-monthly=""></span>&nbsp;€.</p>

              <p>Atsižvelgiant į lizingo bendrovės atliktą Jūsų kreditingumo ir rizikos vertinimą ar
                pasirinkus kitokį sutarties terminą bei įmokų mokėjimo dieną, Jums gali būti
                pasiūlytos ir kitokios sąlygos (pavyzdžiui, metinės palūkanų normos arba mėnesio
                sutarties mokesčio dydis).</p>
            </div>
            <div class="wizard__notify">Pinigus pervesime per 30 min., jei paskolos sutartį
              sudarysite iki 20 val.</div>
            <button class="wizard__btn btn">
              <span class="btn__text">Gauti paskolą</span>
              <i class="btn__icon btn__icon--right">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5.1997 8.9849">
                  <title>icon--caret</title>
                  <polygon
                    points="0.707 8.985 0 8.278 3.786 4.493 0 0.707 0.707 0 5.2 4.493 0.707 8.985"
                    fill="#fff"></polygon>
                </svg> </i>
            </button>
          </div>
        </div>
        <div class="form__done form__done--error done done--error" data-form-done="error">
          <i class="done__close" data-done-close=""></i>
          <div class="done__top">
            <h2>Error</h2>
          </div>
          <div class="done__text">
            <p>Atsiprašome, šiuo metu turime nesklandumų. Bandykite vėliau dar kartą...</p>
          </div>
        </div>
      </form>
    </div>

    <div class="wizard__socials socials">
      <?php include '../partials/socials.php'; ?>
    </div>
    <div class="wizard__toast toast" data-toast="2000">
      <div class="toast__content">
        <div class="toast__media media">
          <i class="media__image media__image--cover"
            style="background-image:url('../media/toast__media.png')"></i>
        </div>
        <div class="toast__text">Išbandykite paskolą būsto remontui</div>
        <a href="" class="toast__link"></a>
        <i class="toast__close" data-toast-close></i>
      </div>
    </div>
  </div>
  <section class="default__section default__section--services">
    <div class="default__container container">
      <h4>PASKOLOS</h4>
      <a href="" class="default__more">Daugiau informacijos</a>
      <div class="default__cards">
        <div class="default__card card card--half">
          <div class="card__media card__media--half media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/loan__media--5.png')"></i>
          </div>
          <div class="card__container card__container--half">
            <div class="card__content">
              <div class="card__title">Paskola automobiliui</div>
              <div class="card__text">
                <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                  seną automobilį?</p>
              </div>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
        <div class="default__card card card--half">
          <div class="card__media card__media--half media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/loan__media--7.png')"></i>
          </div>
          <div class="card__container card__container--half">
            <div class="card__content">
              <div class="card__featured">Rekomenduojame</div>
              <div class="card__title">Vartojimo paskola</div>
              <div class="card__text">
                <p>Norite išeiti pelnytų atostogų, bet atsidarę piniginę persigalvojate? </p>
              </div>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
        <div class="default__card card card--half">
          <div class="card__media card__media--half media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/loan__media--6.png')"></i>
          </div>
          <div class="card__container card__container--half">
            <div class="card__content">
              <div class="card__title">Paskola būsto remontui</div>
              <div class="card__text">
                <p>Norite atnaujinti namus, tačiau finansinės galimybės leidžia pasirūpinti tik
                  vienu kambariu?</p>
              </div>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
        <div class="default__card card card--half">
          <div class="card__media card__media--half media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/loan__media--5.png')"></i>
          </div>
          <div class="card__container card__container--half">
            <div class="card__content">
              <div class="card__title">Paskola automobiliui</div>
              <div class="card__text">
                <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                  seną automobilį?</p>
              </div>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
      </div>
    </div>
    </div>
  </section>
  <section class="default__section default__section--proffer">
    <div class="default__container container">
      <div class="default__proffer proffer">
        <div class="proffer__info">
          <h2>Kodėl verta naudotis Mokilizingo paslaugomis?</h2>
          <div class="proffer__reviews" data-swiper="proffer" data-swiper-space-between="104">
            <div class="proffer__swiper swiper-container" data-swiper-target>
              <div class="proffer__wrapper swiper-wrapper">
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--1.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei
                    (brokas),
                    kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--2.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei
                    (brokas),
                    kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--2.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei
                    (brokas),
                    kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik kad
                    nenusimano ir nežino
                    elementarių
                    grąžinimo sąlygų vykdomų kartu su partneriais, bet ir palieka klientą likimo
                    valiai). Tuo tarpu
                    Mokilizingas pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu
                    ir supažindino
                    pardavėją su
                    jo </div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--3.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei
                    (brokas),
                    kuri buvo pirkta</div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--2.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
                </div>
              </div>
            </div>
            <div class="proffer__nav proffer__nav--prev" data-swiper-prev></div>
            <div class="proffer__nav proffer__nav--next" data-swiper-next></div>
          </div>
          <div class="proffer__text">
            <p>Kad ir kaip gerai planuotumėte savo finansus, kartais kiekvienam pasitaiko
              nenumatytų išlaidų. Tegul netikėtumai neišmuša iš vėžių – MOKILIZINGO paskola sukurta
              tam, kad Jums padėtų. Išsirinkite labiausiai tinkantį paskolos tipą.</p>
          </div>
          <div class="proffer__actions">
            <div class="proffer__action" data-proffer-type="consumer" data-proffer-min="50"
              data-proffer-max="15000" data-proffer-default="100"
              style="background-image:url('../media/proffer__icon--1.png')">
              <i class="proffer__icon"></i>
              <div class="proffer__label">Vartojimo paskola</div>
              <div class="proffer__amount">( Nuo 50 € Iki 15 000 € ) </div>
            </div>
            <div class="proffer__action" data-proffer-type="auto" data-proffer-min="1500"
              data-proffer-max="15000" data-proffer-default="2000"
              style="background-image:url('../media/proffer__icon--2.png')">
              <i class="proffer__icon"></i>
              <div class="proffer__label">Paskola automobiliui</div>
              <div class="proffer__amount">( Nuo 1 500 € Iki 15 000 € ) </div>
            </div>
            <div class="proffer__action" data-proffer-type="repair" data-proffer-min="2000"
              data-proffer-max="15000" data-proffer-default="3000"
              style="background-image:url('../media/proffer__icon--3.png')">
              <i class="proffer__icon"></i>
              <div class="proffer__label">Paskola būsto remontui</div>
              <div class="proffer__amount">( Nuo 2 000 € Iki 15 000 € ) </div>
            </div>
          </div>
        </div>
        <div class="proffer__offer">
          <form class="proffer__form form"
            action="http://localhost/mokilizingas-fe/endpoints/proffer.php">
            <div class="form__content form__content--proffer" data-form-content>
              <div class="form__intro">
                <h4>GAUKITE ASMENINĮ PASIŪLYMĄ</h4>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Jūsų vardas*</span>
                  <input type="text" minlength="3" name="proffer-name" required
                    class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
                <div class="form__item">
                  <span class="form__label">Jūsų vardas*</span>
                  <input type="text" name="proffer-name2" required
                    class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Jūsų el. paštas*</span>
                  <input type="email" name="proffer-email" required
                    class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Paskolos paskirtis</span>
                  <select name="proffer-type" class="form__select" data-proffer-select
                    data-dropdown>
                    <option value="repair">Paskola būsto remontui</option>
                    <option value="consumer">Vartojimo paskola</option>
                    <option value="auto">Paskola automobiliui</option>
                  </select>
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Kiek norėtumete pasiskolinti*</span>
                  <div class="form__range range">
                    <div class="range__target" data-proffer-range></div>
                    <div class="range__value range__value--min" data-proffer-range-min></div>
                    <div class="range__value range__value--max" data-proffer-range-max></div>
                  </div>
                  <input type="hidden" name="proffer-amount" data-proffer-amount tabindex="-1">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <label class="form__item">
                  <input type="checkbox" name="proffer-agree" class="form__native" required>
                  <i class="form__control form__control--white form__control--checkbox"></i>
                  <span class="form__inlabel">Patvirtinu, kad pateikti duomenys yra teisingi.
                    Susipažinau ir sutinku su
                    <a target="_blank" href="basic">privatumo politika</a>.</span>
                  <span class="form__error">Error</span>
                </label>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <button class="form__submit form__submit--footer btn">
                    <i class="btn__icon btn__icon--left">
                      <?php include '../assets/img/icon--phone.svg'; ?>
                    </i>
                    <span class="btn__text">Gauti skambutį</span>
                  </button>
                </div>
              </div>
            </div>
            <div class="form__done form__done--success done done--success" data-form-done="success">
              <i class="done__close" data-done-close></i>
              <div class="done__top">
                <h2>Ačiū.</h2>
              </div>
              <div class="done__text">
                <p>Su jumis susisieks mūsų konsultantė</p>
              </div>
            </div>
            <div class="form__done form__done--error done done--error" data-form-done="error">
              <i class="done__close" data-done-close></i>
              <div class="done__top">
                <h2>Klaida!</h2>
              </div>
              <div class="done__text">
                <p>Su jumis susisieks mūsų konsultantė</p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--recommend">
    <div class="default__container container">
      <h3>REKOMENDUOJAME</h3>
      <div class="default__cards">
        <div class="default__card card card--horizontal card--half">
          <div class="card__media card__media--half card__media--horizontal media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/recommend__media--1.png')"></i>
          </div>
          <div class="card__container card__container--half card__container--horizontal">
            <div class="card__content">
              <div class="card__title">Pirkite prekę dviese su artimu žmogumi. </div>
            </div>
            <div class="card__bottom">
              <a href="recommendation" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="recommendation" class="card__link"></a>
        </div>
        <div class="default__card card card--horizontal card--half">
          <div class="card__media card__media--half card__media--horizontal media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/recommend__media--2.png')"></i>
          </div>
          <div class="card__container card__container--half card__container--horizontal">
            <div class="card__content">
              <div class="card__title">MOKI3 - turėkite dabar atsiskaitykite per 3 mėn.</div>
            </div>
            <div class="card__bottom">
              <a href="recommendation" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="recommendation" class="card__link"></a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--faq">
    <div class="default__container container">
      <div class="default__faq default__faq--loan faq" data-toggler>
        <h3>PAGALBA</h3>
        <div class="faq__list">
          <div class="faq__item" data-toggler-item="hash01">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip sumokėti lizingu?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash05">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip pirkti lizingu?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
          <div class="faq__item is-open" data-toggler-item="hash02">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip gauti paskolą?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer faq__answer--cta" data-toggler-content>
              <p>Patogiausias ir greičiausias būdas sudaryti paskolos sutartį – prisijungti prie
                Savitarnos.</p>
              <p>Sutartį taip pat sudarysite paskambinę telefonu <b><a
                    href="tel:+370 700 55 888">+370
                    700 55 888</a></b>, užsukę į <a
                  href="https://www.post.lt/lt/pagalba/pasto-skyriai-dezutes" target="_blank"
                  rel="noopener noreferrer">„PayPost” skyrių</a> arba MOKILIZINGAS
                atstovybėse.</p>
              <a href="loan-types" class="faq__cta btn btn--black">
                <span class="btn__text">Paskolos</span>
              </a>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash03">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kodėl negaliu gauti paskolos?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash04">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Prašymų formos</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--plain default__section--grey">
    <div class="default__container container">
      <h3>SĄLYGOS</h3>
      <div class="default__text">
        <p>Pavyzdžiui, skolinantis 300 €, kai sutartis sudaroma 36 mėn. terminui, metinė palūkanų
          norma – 6,50 %,
          mėnesio sutarties mokestis – 0,50 %, BVKKMN – 18,40 %, bendra mokėtina suma – 385,03 €,
          mėnesio įmoka – 10,69
          €. </p>
        <p>Atsižvelgiant į lizingo bendrovės atliktą Jūsų kreditingumo ir rizikos vertinimą ar
          pasirinkus kitokį
          sutarties terminą bei įmokų mokėjimo dieną, Jums gali būti pasiūlytos ir kitokios sąlygos
          (pavyzdžiui,
          metinės palūkanų normos arba mėnesio sutarties mokesčio dydis).</p>
      </div>
    </div>
  </section>
  <section class="default__section default__section--header default__section--blue">
    <div class="default__container container">
      <div class="default__header default__header--business">
        <h1>Verslo partneriams</h1>
        <div class="default__intro default__intro--business">
          <p>Su savo partneriais siekiame bendrų tikslų ir džiaugiamės puikiais rezultatais.
            Dirbame tam, kad kartu su jais suteiktume klientams patirtį, kurią jie norėtų kartoti.
          </p>
        </div>
        <div class="default__media default__media--business media">
          <i class="media__image" style="background-image:url('../media/business__media.png')"></i>
        </div>
        <div class="default__actions">
          <a href="" class="default__action btn btn--white-blue">Tapkite partneriu</a>
        </div>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php';
