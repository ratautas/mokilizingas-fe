<?php include '../partials/head.php'; ?>
<main class="app__page app__page--basic basic" data-page>
  <div class="basic__container container">
    <h1>Naudojimo taisyklės</h1>
    <article class="basic__article">
      <p>UAB „MOKILIZINGAS“ SVETAINĖS NAUDOJIMO TAISYKLĖS</p>
      <p>&nbsp;</p>
      <p>Šios naudojimosi svetaine taisyklės yra taikomos visiems asmenims, kurie naudojasi žemiau
        nurodyta internetine svetaine. Pradėdami naudotis žemiau nurodyta svetaine, Jūs
        įsipareigojate laikytis šių taisyklių reikalavimų.</p>
      <p>1. Bendrosios nuostatos</p>
      <p>1.1. Interneto svetainė (toliau – Svetainė), viešai prieinama adresu www.mokilizingas.lt,
        nuosavybės teise
        priklauso ir yra kontroliuojama UAB „Mokilizingas“ (toliau – Bendrovė), juridinio asmens
        kodas 124926897,
        buveinės adresas: Kareivių g. 11B, Vilnius.</p>
      <p>1.2. Visi šios Svetainės lankytojai (toliau – Vartotojai) turi laikytis šių Svetainės
        naudojimo taisyklių
        (toliau – Taisyklės). Įeidami į šią Svetainę, Vartotojai patvirtina, kad besąlygiškai
        sutinka su šiomis
        Taisyklėmis ir įsipareigoja jų laikytis, apsilankę ir (ar) besinaudojantys Svetainės
        visomis ir (ar) bet
        kuria joje teikiamų paslaugų, įskaitant ir Vartotojus, neatlikusius registracijos
        procedūros.</p>
      <p>1.3. Šiose Taisyklėse vartojama sąvoka „Naudojimasis Svetaine“ yra suprantama ir aiškinama
        plečiamai: kaip
        bet kokių veiksmų atlikimas Vartotojui prisijungus prie Svetainės elektroninių ryšių
        tinklais.</p>
      <p>1.4. Šiose Taisyklėse vartojama sąvoka „Paslaugos“ yra suprantama kaip paslaugos, kurios
        tampa prieinamos
        Vartotojui užsiregistravus ir prisijungus prie savitarnos sistemos. Savitarnos sistema –
        tai Bendrovės
        valdoma sistema, prie kurios prisiregistravęs ir prisijungęs Vartotojas gali pasiimti
        vartojamąją paskolą,
        pasirašyti sutikimą dėl asmens duomenų tvarkymo, peržiūrėti sudarytas lizingo bei paskolos
        sutartis ir jų
        mokėjimo grafikus, sumokėti sutarties įmokas bei atlikti kitus veiksmus (toliau –
        Savitarna).</p>
      <p>1.5. Vartotojui ir Bendrovei tarpusavyje pasirašius susitarimą dėl visų ar dalies Pasaugų
        gavimo, šios
        Taisyklės yra taikomos tiek, kiek jos neprieštarauja minėtam susitarimui arba susitarimas
        nereglamentuoja
        konkrečių santykių.</p>
      <p>&nbsp;</p>
      <p>2. Vartotojų registracija ir asmens duomenų apsauga</p>
      <p>2.1. Naudotis arba turėti galimybę visapusiškai naudotis Svetaine, Savitarna ir Bendrovės
        teikiamomis
        Paslaugomis galima tik Svetainėje atlikus registracijos procedūrą. Registracijos procedūra
        atliekama
        paspaudus Svetainės dešiniajame kampe, Savitarnos skiltyje, esančią aktyviąją nuorodą
        „Registracija“,
        užpildžius visus privalomus registracijos formos laukelius: vardas, pavardė, asmens kodas,
        elektroninio pašto
        adresas ir prisijungimo slaptažodis bei susipažinus su Privatumo politika ir šiomis
        Taisyklėmis. Apie
        sėkmingą registraciją Vartotojas informuojamas trumpuoju tekstiniu pranešimu, siunčiamu
        Vartotojo nurodytu
        elektroniniu pašto adresu. Vartotojas, aktyvuodamas registraciją, patvirtina, kad:</p>
      <p>2.1.1. Vartotojas įdėmiai ir atidžiai perskaitė šias Taisykles ir Privatumo politiką, jas
        suprato ir su
        jomis laisva valia sutinka bei įsipareigoja naudotis Svetaine ir (ar) Savitarna Taisyklėse
        ir Privatumo
        politikoje nustatyta tvarka ir sąlygomis;</p>
      <p>2.1.2. Vartotojo pateikti duomenys yra teisingi, tikslūs ir išsamūs. Vartotojas, keisdamas
        ar pildydamas
        duomenis apie save, privalo pateikti tik teisingus duomenis. Bet kokie nuostoliai, atsiradę
        dėl klaidingų
        duomenų pateikimo, tenka Vartotojui;</p>
      <p>2.1.3. pasikeitus Vartotojo registracijos metu nurodytiems duomenims ar kitai susijusiai
        informacijai,
        Vartotojas įsipareigoja nedelsiant, bet ne vėliau kaip per 2 (dvi) darbo dienas, informuoti
        apie tai
        Bendrovę, pakeisdamas (papildydamas) savo profilio duomenis Savitarnoje arba kreipęsis į
        Bendrovę tiesiogiai;</p>
      <p>2.1.4. Vartotojas yra veiksnus fizinis asmuo ir registracijos metu nėra apsvaigęs nuo
        alkoholio, narkotinių
        ar psichotropinių medžiagų;</p>
      <p>2.1.5. Vartotojas sutinka, kad Bendrovė tvarkytų bet kokius Vartotojo pateiktus jo asmens
        duomenis ir (ar)
        kitokią informaciją ir naudotų šių Taisyklių vykdymo ir Privatumo politikoje nustatytais
        tikslais, Paslaugoms
        teikti, tiesioginės rinkodaros, statistikos bei kitais tikslais. Pagrindiniai asmens
        duomenų rinkimo,
        tvarkymo ir saugojimo principai bei tvarka nurodyta Bendrovės <a href="https://www.mokilizingas.lt/privatumo-politika/">Privatumo
          politikoje</a>.</p>
      <p>2.2. Vartotojui kategoriškai draudžiama savintis kitų asmenų tapatybę, nurodant svetimą
        vardą, pavardę ir
        (ar) kitus duomenis. Nepaisančiam minėtų reikalavimų Vartotojui Bendrovė turi teisę,
        nedelsiant ir iš anksto
        nepranešus, uždrausti naudotis Svetaine ir (ar) Savitarna ir (ar) Bendrovės teikiamomis
        visomis ar paskiromis
        Paslaugomis. Esant pagrįstiems įtarimams, kad tokiais savo veiksmais Vartotojas padarė ar
        galėjo padaryti
        žalą Bendrovei ir (ar) tretiesiems asmenims ir (ar) viešajam interesui, Bendrovė turi teisę
        perduoti visus
        turimus duomenis apie tokį Vartotoją kompetetingoms valstybinėms institucijoms.</p>
      <p>2.3. Vartotojas besąlygiškai įsipareigoja užtikrinti pateiktų duomenų slaptumą ir privalo
        dėti maksimalias
        pastangas, kad apsaugotų prisijungimo prie Savitarnos duomenis (tame tarpe slaptažodį) nuo
        trečiųjų asmenų
        susižinojimo ir jokiais tiesioginiais (netiesioginiais) būdais neatskleisti jo tretiesiems
        asmenims bei
        užtikrinti, kad jokie tretieji asmenys negalėtų pasinaudoti jo duomenimis naudojantis
        Svetaine ir (ar)
        Savitarna ir (ar) Bendrovės teikiamomis Paslaugomis ir (ar) kitais tikslais.</p>
      <p>2.4. Vartotojas atsako už bet kokius trečiųjų asmenų veiksmus, jei jie buvo padaryti
        pasinaudojant Vartotojo
        duomenimis, ir visos pareigos bei atsakomybė, atsiradę ar susiję su tokiais trečiųjų asmenų
        veiksmais, visa
        apimtimi tenka Vartotojui.</p>
      <p>2.5. Vartotojas turi teisę išsiregistruoti iš Bendrovės Savitarnos ir kitų jos valdomų
        duomenų bazių tik
        įvykdęs visus savo kreditorinius įsipareigojimus Bendrovei ir pateikęs prašymą raštu <a
          href="https://www.mokilizingas.lt/privatumo-politika/">Privatumo
          politikoje</a> nustatyta tvarka.</p>
      <p>&nbsp;</p>
      <p>3. Bendrovės ir Vartotojo teisės bei pareigos</p>
      <p>3.1. Bendrovė pasilieka teisę bet kada, nepranešusi Vartotojui, keisti Svetainėje
        nurodytas Bendrovės
        teikiamas</p>
      <table>
        <thead>
          <tr>
            <td>Slapuko pavadinimas</td>
            <td>Slapuko paskirtis</td>
            <td>Sukūrimo momentas</td>
            <td>Galiojimo laikas</td>
            <td>Naudojami duomenys</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>tvs_session_id</td>
            <td>Naudojamas kaip turinio valdymo sistemos sesijos identifikatorius</td>
            <td>Įeinant į interneto Svetainę</td>
            <td>Iki interneto Svetainės lango uždarymo pabaigos</td>
            <td>Unikalus ID</td>
          </tr>
          <tr>
            <td>_ga</td>
            <td>Naudojamas statistinei informacijai apie interneto Svetainės lankomumą rinkti</td>
            <td>Įeinant į interneto Svetainę</td>
            <td>2 metai</td>
            <td>Unikalus ID</td>
          </tr>
          <tr>
            <td>_gat</td>
            <td>Naudojamas riboti kreipimusi skaičiui į doubleckick.net</td>
            <td>Įeinant į interneto Svetainę</td>
            <td>1 minutė</td>
            <td>Sveikas skaičius</td>
          </tr>
          <tr>
            <td>_gid</td>
            <td>Naudojamas statistinei informacijai rinkti</td>
            <td>Įeinant į interneto Svetainę</td>
            <td>24 valandos</td>
            <td>Unikalus ID</td>
          </tr>
        </tbody>
      </table>
    </article>
  </div>
  <?php include '../partials/foot.php'; ?>
