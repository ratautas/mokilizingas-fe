<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--cyan">
    <div class="default__container default__container--with-wizard container">
      <div class="default__header default__header--with-wizard default__header--high">
        <h1>Lizingas</h1>
        <div class="default__media default__media--high media">
          <i class="media__image" style="background-image:url('../media/leasing__media.png')"></i>
        </div>
        <div class="default__intro">
          <p>Svajojate apie naują kompiuterį, dviratį ar baldus? Kiaulės-taupyklės neprireiks, jei
            pirksite lizingu.
            Savo pirkinį gausite iškart, o mokėsite už jį mažomis dalimis pagal pasirinktą įmokų
            grafiką. O kad ramiai
            miegotumėte naktimis, nusipirktą mobilųjį telefoną, kompiuterį, planšetę ar kitą prekę
            mielai APDRAUSIME.</p>
          <p>Sutarties terminas gali būti nuo 6 iki 60 mėnesių. Finansuojame nuo 50 iki 10000 Eur
            sumą.</p>
        </div>
      </div>
    </div>
  </section>
  <div class="default__wizard wizard">
    <?php include '../partials/wizard--v1.php'; ?>
    <div class="wizard__socials socials">
      <?php include '../partials/socials.php'; ?>
    </div>
    <div class="wizard__toast toast" data-toast="2000">
      <div class="toast__content toast__content--no-media">
        <div class="toast__text">Išbandykite paskolą būsto remontui</div>
        <a href="" class="toast__link"></a>
        <i class="toast__close" data-toast-close></i>
      </div>
    </div>
  </div>
  <section class="default__section default__section--offers">
    <div class="default__container container">
      <h4>PARTNERIŲ PASIŪLYMAI</h4>
      <div class="default__offers offers" data-swiper="offers" data-swiper-slides-per-view="2"
        data-swiper-space-between="30">
        <div class="offers__slider swiper-container" data-swiper-target>
          <div class="offers__wrapper swiper-wrapper">
            <a href="" target="_blank" class="offers__slide swiper-slide"
              style="background-image:url('../media/default__offer--1.png')">
              <span class="offers__banner">
                Ypatingos sąlygos perkant lizingu
              </span>
            </a>
            <a href="" target="_blank" class="offers__slide swiper-slide"
              style="background-image:url('../media/default__offer--2.png')">
              <span class="offers__banner">
                Išskirtiniai atostogų pasiūlymai
              </span>
            </a>
            <a href="" target="_blank" class="offers__slide swiper-slide"
              style="background-image:url('../media/default__offer--2.png')">
              <span class="offers__banner">
                Išskirtiniai atostogų pasiūlymai 2
              </span>
            </a>
            <a href="" target="_blank" class="offers__slide swiper-slide"
              style="background-image:url('../media/default__offer--2.png')">
              <span class="offers__banner">
                Išskirtiniai atostogų pasiūlymai 3
              </span>
            </a>
          </div>
        </div>
        <div class="offers__actions">
          <div class="offers__nav offers__nav--prev" data-swiper-prev></div>
          <i class="offers__bullet is-active" data-swiper-bullet></i>
          <i class="offers__bullet" data-swiper-bullet></i>
          <i class="offers__bullet" data-swiper-bullet></i>
          <i class="offers__bullet" data-swiper-bullet></i>
          <i class="offers__bullet" data-swiper-bullet></i>
          <div class="offers__nav offers__nav--next" data-swiper-next></div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--tips">
    <div class="default__container container">
      <h4>KAIP NEPERMOKĖTI PERKANT LIZINGU?</h4>
      <div class="default__tips">
        <div class="default__tip default__tip--left tip">
          <div class="tip__content tip__content--left">
            <h2>Atkreipkite dėmesį į buitinės technikos energijos</h2>
            <div class="tip__media tip__media--right media">
              <i style="background-image:url('../media/saldytuvas.png')" class="media__image"></i>
            </div>
            <div class="tip__text">
              <p>Nauja buitinė technika gali padėti sutaupyti.<br>
                Pvz., naujausias A+++ klasės šaldytuvas suvartoja 50–60 % mažiau elektros energijos
                nei A+ klasės
                šaldytuvas. Be to, tausojate aplinką.</p>
            </div>
          </div>
        </div>
        <div class="default__tip default__tip--right tip">
          <div class="tip__content tip__content--right">
            <h2>Lyginkite skirtingų lizingo bendrovių</h2>
            <div class="tip__media tip__media--left media">
              <i style="background-image:url('../media/Supynes.png')" class="media__image"></i>
            </div>
            <div class="tip__text tip__text--right">
              <p>Pirkdami prekę, palyginkite skirtingų bendrovių mėnesio įmokas, kitas sąlygas ir
                mokesčių dydžius tam
                pačiam terminui. Lyginant lizingo pasiūlymus ypač svarbu atkreipti dėmesį į bendrą
                vartojimo kredito
                kainos metinę normą (BVKKMN), kuri parodo, kiek vidutiniškai lizingas pabrangsta
                per metus. Pvz.,
                skolinatės 1000 Eur 24 mėn. Bendrovės A pasiūlyme BVKKMN yra 5 %, o bendrovės B –
                19 %. Pasirinkę
                bendrovę A, sutaupysite – todėl ją rinktis apsimoka labiau.</p>
            </div>
          </div>
        </div>
        <div class="default__tip default__tip--left tip">
          <div class="tip__content tip__content--left">
            <h2>Naudokitės</h2>
            <div class="tip__media tip__media--right media">
              <i style="background-image:url('../media/telefonas.png')" class="media__image"></i>
            </div>
            <div class="tip__text">
              <p>M. parašas suteikia galimybę pirkti lizingu internete, pasiimti paskolą mūsų
                svetainėje, užsakyti
                kortelę. Šis parašas yra lygiavertis Jūsų įprastiniam parašui su kuriuo galėsite
                prisijungti prie el.
                paslaugų teikiančių internetinių portalų, el. bankininkystės sistemų ir patvirtinti
                savo tapatybę. Su
                M. parašu svarbius dokumentus galite pasirašyti dar greičiau.</p>
            </div>
          </div>
        </div>
        <div class="default__tip default__tip--right tip">
          <div class="tip__content tip__content--right">
            <h2>Planuokite savo</h2>
            <div class="tip__media tip__media--left media">
              <i style="background-image:url('../media/kalendorius.png')" class="media__image"></i>
            </div>
            <div class="tip__text tip__text--right">
              <p>Pirkdami prekę, palyginkite skirtingų bendrovių mėnesio įmokas, kitas sąlygas ir
                mokesčių dydžius tam
                pačiam terminui. Lyginant lizingo pasiūlymus ypač svarbu atkreipti dėmesį į bendrą
                vartojimo kredito
                kainos metinę normą (BVKKMN), kuri parodo, kiek vidutiniškai lizingas pabrangsta
                per metus. Pvz.,
                skolinatės 1000 Eur 24 mėn. Bendrovės A pasiūlyme BVKKMN yra 5 %, o bendrovės B –
                19 %. Pasirinkę
                bendrovę A, sutaupysite – todėl ją rinktis apsimoka labiau.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--reminder default__section--grey">
    <div class="default__container container">
      <h4>NEPAMIRŠKITE</h4>
      <div class="default__listing listing">
        <div class="listing__list listing__list--reminder">
          <div class="listing__item listing__item--dark listing__item--high">Per 14 dienų turite
            teisę atsisakyti lizingo sutarties;</div>
          <div class="listing__item listing__item--dark listing__item--high">Visą sumą galite
            sumokėti anksčiau, taip sumažindami vartojimo kredito kainą;</div>
          <div class="listing__item listing__item--dark listing__item--high">Su lizingu prekę
            gaunate iškart, neišleisdami visų santaupų.</div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <section class="default__section default__section--header default__section--pink">
    <div class="default__container container">
      <div class="default__header default__header--promo">
        <h2>O gal paskola?</h2>
        <div class="default__media default__media--promo media">
          <i class="media__image" style="background-image:url('../media/loan__media--1.png')"></i>
        </div>
        <div class="default__intro default__intro--promo">
          <p>Planuojate atnaujinti būstą, vykti ilgai lauktų atostogų, atidžiau rūpintis sveikata
            ar pagaliau
            susiremontuoti automobilį? Galime padėti įgyvendinti Jūsų tikslus ir paskolinti iki 15
            000 Eur 3-72 mėn.</p>
        </div>
        <div class="default__actions">
          <a href="" class="default__action btn btn--white-blue">Gauti paskolą</a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--recommend">
    <div class="default__container container">
      <h4>REKOMENDUOJAME</h4>
      <div class="default__cards">
        <div class="default__card card card--horizontal card--half">
          <div class="card__media card__media--half card__media--horizontal media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/recommend__media--1.png')"></i>
          </div>
          <div class="card__container card__container--half card__container--horizontal">
            <div class="card__content">
              <div class="card__title">Pirkite prekę dviese su artimu žmogumi. </div>
            </div>
            <div class="card__bottom">
              <a href="recommendation" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="recommendation" class="card__link"></a>
        </div>
        <div class="default__card card card--horizontal card--half">
          <div class="card__media card__media--half card__media--horizontal media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/recommend__media--2.png')"></i>
          </div>
          <div class="card__container card__container--half card__container--horizontal">
            <div class="card__content">
              <div class="card__title">MOKI3 - turėkite dabar atsiskaitykite per 3 mėn.</div>
            </div>
            <div class="card__bottom">
              <a href="recommendation" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="recommendation" class="card__link"></a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--partners">
    <div class="default__container container">
      <h4>MŪSŲ PARTNERIAI</h4>
      <div class="default__partners partners">
        <script>
          var regions = [{
              label: 'Vilnius',
              value: 'vilnius',
              customProperties: {
                zoom: 10,
                lng: 25.36,
                lat: 54.76,
              },
            },
            {
              label: 'Kaunas',
              value: 'kaunas',
              customProperties: {
                zoom: 10,
                lng: 23.85,
                lat: 54.9,
              }
            }, {
              label: 'Klaipėda',
              value: 'klaipeda',
              customProperties: {
                zoom: 10,
                lng: 21,
                lat: 55.7,
              }
            }, {
              label: 'Šiauliai',
              value: 'siauliai',
              customProperties: {
                zoom: 10,
                lng: 25.26,
                lat: 54.7,
              }
            }, {
              label: 'Panevėžys',
              value: 'panevezys',
              customProperties: {
                zoom: 10,
                lng: 25.26,
                lat: 54.7,
              }
            }
          ];
          var partners = {
            type: 'FeatureCollection',
            features: [{
                geometry: {
                  type: 'Point',
                  coordinates: [25.32, 54.72],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              }, {
                geometry: {
                  type: 'Point',
                  coordinates: [25.34, 54.74],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              }, {
                geometry: {
                  type: 'Point',
                  coordinates: [25.36, 54.76],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              }, {
                geometry: {
                  type: 'Point',
                  coordinates: [25.38, 54.78],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              },
              {
                geometry: {
                  type: 'Point',
                  coordinates: [21.1, 55.7],
                },
                type: 'Feature',
                properties: {
                  region: 'klaipeda',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Klaipeda',
                },
              },
              {
                geometry: {
                  type: 'Point',
                  coordinates: [23.3, 55.9],
                },
                type: 'Feature',
                properties: {
                  region: 'siauliai',
                  description: 'Come and try our award-winning cakes and pastries.',
                  name: 'Siauliai',
                },
              },
            ],
          };
        </script>
        <div class="partners__container">
          <div class="partners__content">
            <div class="partners__map" data-partners-map></div>
            <div class="partners__actions">
              <select class="partners__select" data-partners-select></select>
              <div class="partners__finder" data-partners-locate>Arčiausiai manęs</div>
            </div>
          </div>
          <div class="partners__logos">
            <div class="partners__logo">
              <img src="../media/logo--senukai.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--berry.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--egvile.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--senukai.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--berry.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--egvile.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php';
