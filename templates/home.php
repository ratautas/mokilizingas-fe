<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--hero">
    <div class="default__hero hero" data-swiper="hero">
      <div class="hero__slider swiper-container" data-swiper-target>
        <div class="hero__wrapper swiper-wrapper">
          <div class="hero__slide hero__slide--yellow hero__slide--background swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Paskola automobiliui</h1>
                <div class="default__tag">Palūkanos tik nuo 4,9 %</div>
                <div class="default__intro default__intro--hero">
                  <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                    seną automobilį?
                    Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras
                    sąlygas. Nepamirškite
                    įvertinti visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir
                    sezoninį padangų keitimą.</p>
                </div>
                <div class="default__listing default__listing--head listing">
                  <div class="listing__list listing__list--head">
                    <div class="listing__item">Nėra sutarties sudarymo mokesčio</div>
                    <div class="listing__item">Palūkanos tik nuo 4,9 %</div>
                    <div class="listing__item">Suma nuo 500 iki 15 000 Eur</div>
                    <div class="listing__item">12 – 72 mėn. laikotarpiui</div>
                    <div class="listing__item">Nereikia pradinio įnašo ir Kasko draudimo</div>
                    <div class="listing__item">Atsakymas dėl paskolos per kelias minutes, o pinigai
                      Jus pasieks per 30
                      min.
                    </div>
                  </div>
                </div>
                <div class="default__media default__media--background media">
                  <i class="media__image" style="background-image:url('../media/hero__bg.png')"></i>
                  <img src="../media/hero__bg.png" alt="" class="media__image media__image--hero" />
                </div>
              </div>
            </div>
          </div>
          <div class="hero__slide hero__slide--yellow swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Paskola automobiliui</h1>
                <div class="default__tag">Palūkanos tik nuo 4,9 %</div>
                <div class="default__intro default__intro--hero">
                  <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                    seną automobilį?
                    Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras
                    sąlygas. Nepamirškite
                    įvertinti visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir
                    sezoninį padangų keitimą.</p>
                </div>
                <div class="default__listing default__listing--head listing">
                  <div class="listing__list listing__list--head">
                    <div class="listing__item">Nėra sutarties sudarymo mokesčio</div>
                    <div class="listing__item">Palūkanos tik nuo 4,9 %</div>
                    <div class="listing__item">Suma nuo 500 iki 15 000 Eur</div>
                    <div class="listing__item">12 – 72 mėn. laikotarpiui</div>
                    <div class="listing__item">Nereikia pradinio įnašo ir Kasko draudimo</div>
                    <div class="listing__item">Atsakymas dėl paskolos per kelias minutes, o pinigai
                      Jus pasieks per 30
                      min.
                    </div>
                  </div>
                </div>
                <div class="default__media default__media--hero media">
                  <div class="media__image media__image--sequence sequence"
                    data-sequence="../media/sequences/car_sprite.png" data-sequence-fps="24"
                    data-sequence-frames="10" data-sequence-repeat="5">
                    <img src="../media/sequences/car.png" alt="" class="sequence__placeholder"
                      data-sequence-placeholder>
                    <canvas class="sequence__canvas" data-sequence-canvas></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hero__slide hero__slide--pink swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Kiekvienam pasitaiko nenumatytų išlaidų.</h1>
                <div class="default__intro default__intro--hero">
                  <p>Negaiškite laiko – MOKILIZINGO vartojimo paskola leis tikslus pasiekti jau
                    dabar.</p>
                </div>
                <div class="default__actions">
                  <a href="" class="default__action btn btn--narrow btn--white-ghost">
                    <span class="btn__text">Plačiau</span>
                  </a>
                </div>
                <div class="default__media default__media--hero media">
                  <div class="media__image media__image--sequence sequence"
                    data-sequence="../media/sequences/kiaule_sprite.png" data-sequence-fps="24"
                    data-sequence-frames="34">
                    <img src="../media/sequences/kiaule.png" alt="" class="sequence__placeholder"
                      data-sequence-placeholder>
                    <canvas class="sequence__canvas" data-sequence-canvas></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hero__slide hero__slide--orange swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Kiekvienam pasitaiko nenumatytų išlaidų.</h1>
                <div class="default__intro default__intro--hero">
                  <p>Negaiškite laiko – MOKILIZINGO vartojimo paskola leis tikslus pasiekti jau
                    dabar.</p>
                </div>
                <div class="default__actions">
                  <a href="" class="default__action btn btn--narrow btn--white-ghost">
                    <span class="btn__text">Plačiau</span>
                  </a>
                </div>
                <div class="default__media default__media--hero media">
                  <div class="media__image media__image--sequence sequence"
                    data-sequence="../media/sequences/kuprine_sprite.png" data-sequence-fps="24"
                    data-sequence-frames="41">
                    <img src="../media/sequences/kuprine.png" alt="" class="sequence__placeholder"
                      data-sequence-placeholder>
                    <canvas class="sequence__canvas" data-sequence-canvas></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hero__slide hero__slide--blue swiper-slide">
            <div class="hero__container container">
              <div class="default__header default__header--high">
                <h1>Kiekvienam pasitaiko nenumatytų išlaidų.</h1>
                <div class="default__intro default__intro--hero">
                  <p>Negaiškite laiko – MOKILIZINGO vartojimo paskola leis tikslus pasiekti jau
                    dabar.</p>
                </div>
                <div class="default__actions">
                  <a href="" class="default__action btn btn--narrow btn--white-ghost">
                    <span class="btn__text">Plačiau</span>
                  </a>
                </div>
                <div class="default__media default__media--hero media">
                  <div class="media__image media__image--sequence sequence"
                    data-sequence="../media/sequences/graztas_sprite.png" data-sequence-fps="24"
                    data-sequence-frames="20">
                    <img src="../media/sequences/graztas.png" alt="" class="sequence__placeholder"
                      data-sequence-placeholder>
                    <canvas class="sequence__canvas" data-sequence-canvas></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hero__actions swiper-pagination">
          <div class="hero__nav hero__nav--prev" data-swiper-prev></div>
          <i class="hero__bullet is-active" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
          <i class="hero__bullet" data-swiper-bullet></i>
          <div class="hero__nav hero__nav--next" data-swiper-next></div>
        </div>
      </div>
    </div>
  </section>
  <div class="default__wizard wizard">
    <?php // include '../partials/wizard--v1.php';?>
    <?php include '../partials/wizard--v3.php'; ?>
    <div class="wizard__socials socials">
      <?php include '../partials/socials.php'; ?>
    </div>
    <div class="wizard__toast toast" data-toast="2000">
      <div class="toast__content">
        <div class="toast__media media">
          <i class="media__image media__image--cover"
            style="background-image:url('../media/toast__media.png')"></i>
        </div>
        <div class="toast__text">Išbandykite paskolą būsto remontui</div>
        <a href="" class="toast__link"></a>
        <i class="toast__close" data-toast-close></i>
      </div>
    </div>
  </div>
  <section class="default__section default__section--services">
    <div class="default__container container">
      <h4>PASKOLOS</h4>
      <a href="" class="default__more">Daugiau informacijos</a>
      <div class="default__cards">
        <div class="default__card card card--third">
          <div class="card__media card__media--third media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/loan__media--5.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Paskola automobiliui</div>
              <div class="card__text">
                <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti
                  seną automobilį?</p>
              </div>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
        <div class="default__card card card--third">
          <div class="card__media card__media--third media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/loan__media--7.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__featured">Rekomenduojame</div>
              <div class="card__title">Vartojimo paskola</div>
              <div class="card__text">
                <p>Norite išeiti pelnytų atostogų, bet atsidarę piniginę persigalvojate? </p>
              </div>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
        <div class="default__card card card--third">
          <div class="card__media card__media--third media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/loan__media--6.png')"></i>
          </div>
          <div class="card__container card__container--third">
            <div class="card__content">
              <div class="card__title">Paskola būsto remontui</div>
              <div class="card__text">
                <p>Norite atnaujinti namus, tačiau finansinės galimybės leidžia pasirūpinti tik
                  vienu kambariu?</p>
              </div>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
      </div>
    </div>
    </div>
  </section>
  <section class="default__section default__section--proffer">
    <div class="default__container container">
      <div class="default__proffer proffer">
        <div class="proffer__info">
          <h2>Kodėl verta naudotis Mokilizingo paslaugomis?</h2>
          <div class="proffer__reviews" data-swiper="proffer" data-swiper-space-between="104">
            <div class="proffer__swiper swiper-container" data-swiper-target>
              <div class="proffer__wrapper swiper-wrapper">
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--1.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei
                    (brokas),
                    kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--2.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei (brokas),kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                    (kuris ne tik</div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--2.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei
                    (brokas),
                    kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik kad
                    nenusimano ir nežino
                    elementarių
                    grąžinimo sąlygų vykdomų kartu su partneriais, bet ir palieka klientą likimo
                    valiai). Tuo tarpu
                    Mokilizingas pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu
                    ir supažindino
                    pardavėją su
                    jo </div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--3.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                    Sugedus prekei
                    (brokas),
                    kuri buvo pirkta</div>
                </div>
                <div class="proffer__review review swiper-slide">
                  <div class="review__top">
                    <div class="review__image"
                      style="background-image:url('../media/review__image--2.png')"></div>
                    <div class="review__name">Paulius Povilas</div>
                    <div class="review__stars">
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star review__star--filled">
                        <?php include '../assets/img/icon--star--filled.svg'; ?>
                      </i>
                      <i class="review__star">
                        <?php include '../assets/img/icon--star.svg'; ?>
                      </i>
                    </div>

                  </div>
                  <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
                </div>
              </div>
            </div>
            <div class="proffer__nav proffer__nav--prev" data-swiper-prev></div>
            <div class="proffer__nav proffer__nav--next" data-swiper-next></div>
          </div>
          <div class="proffer__text">
            <p>Kad ir kaip gerai planuotumėte savo finansus, kartais kiekvienam pasitaiko
              nenumatytų išlaidų. Tegul netikėtumai neišmuša iš vėžių – MOKILIZINGO paskola sukurta
              tam, kad Jums padėtų. Išsirinkite labiausiai tinkantį paskolos tipą.</p>
          </div>
          <div class="proffer__actions">
            <div class="proffer__action" data-proffer-type="consumer" data-proffer-min="50"
              data-proffer-max="15000" data-proffer-default="100"
              style="background-image:url('../media/proffer__icon--1.png')">
              <i class="proffer__icon"></i>
              <div class="proffer__label">Vartojimo paskola</div>
              <div class="proffer__amount">( Nuo 50 € Iki 15 000 € ) </div>
            </div>
            <div class="proffer__action" data-proffer-type="auto" data-proffer-min="1500"
              data-proffer-max="15000" data-proffer-default="2000"
              style="background-image:url('../media/proffer__icon--2.png')">
              <i class="proffer__icon"></i>
              <div class="proffer__label">Paskola automobiliui</div>
              <div class="proffer__amount">( Nuo 1 500 € Iki 15 000 € ) </div>
            </div>
            <div class="proffer__action" data-proffer-type="repair" data-proffer-min="2000"
              data-proffer-max="15000" data-proffer-default="3000"
              style="background-image:url('../media/proffer__icon--3.png')">
              <i class="proffer__icon"></i>
              <div class="proffer__label">Paskola būsto remontui</div>
              <div class="proffer__amount">( Nuo 2 000 € Iki 15 000 € ) </div>
            </div>
          </div>
        </div>
        <div class="proffer__offer">
          <form class="proffer__form form"
            action="http://localhost/mokilizingas-fe/endpoints/proffer.php">
            <div class="form__content form__content--proffer" data-form-content>
              <div class="form__intro">
                <h4>GAUKITE ASMENINĮ PASIŪLYMĄ</h4>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Jūsų vardas*</span>
                  <input type="text" minlength="3" name="proffer-name" required
                    class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
                <div class="form__item">
                  <span class="form__label">Jūsų vardas*</span>
                  <input type="text" name="proffer-name2" required
                    class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Jūsų tel. numeris</span>
                  <input type="number" name="proffer-tel" inputmode="decimal" pattern="[0-9]*"
                    class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Jūsų el. paštas*</span>
                  <input type="email" name="proffer-email" required
                    class="form__input form__input--white">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Paskolos paskirtis</span>
                  <select name="proffer-type" class="form__select" data-proffer-select
                    data-dropdown>
                    <option value="repair">Paskola būsto remontui</option>
                    <option value="consumer">Vartojimo paskola</option>
                    <option value="auto">Paskola automobiliui</option>
                  </select>
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <span class="form__label">Kiek norėtumete pasiskolinti*</span>
                  <div class="form__range range">
                    <div class="range__target" data-proffer-range></div>
                    <div class="range__value range__value--min" data-proffer-range-min></div>
                    <div class="range__value range__value--max" data-proffer-range-max></div>
                  </div>
                  <input type="hidden" name="proffer-amount" data-proffer-amount tabindex="-1">
                  <span class="form__error">Error</span>
                </div>
              </div>
              <div class="form__row">
                <label class="form__item">
                  <input type="checkbox" name="proffer-agree" class="form__native" required>
                  <i class="form__control form__control--white form__control--checkbox"></i>
                  <span class="form__inlabel">Patvirtinu, kad pateikti duomenys yra teisingi.
                    Susipažinau ir sutinku su
                    <a target="_blank" href="basic">privatumo politika</a>.</span>
                  <span class="form__error">Error</span>
                </label>
              </div>
              <div class="form__row">
                <label class="form__item">
                  <input type="checkbox" name="proffer-newsletter" class="form__native">
                  <i class="form__control form__control--white form__control--checkbox"></i>
                  <span class="form__inlabel">Sutinku el. paštu gauti su AB „MOKILIZINGAS”
                    susijusias naujienas ir specialius pasiūlymus.</span>
                </label>
              </div>
              <div class="form__row">
                <div class="form__item">
                  <button class="form__submit btn">
                    <i class="btn__icon btn__icon--left">
                      <?php include '../assets/img/icon--mail.svg'; ?>
                    </i>
                    <span class="btn__text">Gauti pasiūlymą</span>
                  </button>
                </div>
              </div>
            </div>
            <div class="form__done form__done--success done done--success" data-form-done="success">
              <i class="done__close" data-done-close></i>
              <div class="done__top">
                <h2>Ačiū.</h2>
              </div>
              <div class="done__text">
                <p>Su jumis susisieks mūsų konsultantė</p>
              </div>
            </div>
            <div class="form__done form__done--error done done--error" data-form-done="error">
              <i class="done__close" data-done-close></i>
              <div class="done__top">
                <h2>Klaida!</h2>
              </div>
              <div class="done__text">
                <p>Su jumis susisieks mūsų konsultantė</p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--recommend">
    <div class="default__container container">
      <h3>REKOMENDUOJAME</h3>
      <div class="default__cards">
        <div class="default__card card card--horizontal card--half">
          <div class="card__media card__media--half card__media--horizontal media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/recommend__media--1.png')"></i>
          </div>
          <div class="card__container card__container--half card__container--horizontal">
            <div class="card__content">
              <div class="card__title">Pirkite prekę dviese su artimu žmogumi. </div>
            </div>
            <div class="card__bottom">
              <a href="recommendation" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="recommendation" class="card__link"></a>
        </div>
        <div class="default__card card card--horizontal card--half">
          <div class="card__media card__media--half card__media--horizontal media">
            <i class="media__image media__image--cover"
              style="background-image:url('../media/recommend__media--2.png')"></i>
          </div>
          <div class="card__container card__container--half card__container--horizontal">
            <div class="card__content">
              <div class="card__title">MOKI3 - turėkite dabar atsiskaitykite per 3 mėn.</div>
            </div>
            <div class="card__bottom">
              <a href="recommendation" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="recommendation" class="card__link"></a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--faq">
    <div class="default__container container">
      <div class="default__faq default__faq--loan faq" data-toggler>
        <h3>PAGALBA</h3>
        <div class="faq__list">
          <div class="faq__item" data-toggler-item="hash01">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip sumokėti lizingu?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash05">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip pirkti lizingu?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
          <div class="faq__item is-open" data-toggler-item="hash02">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip gauti paskolą?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer faq__answer--cta" data-toggler-content>
              <p>Patogiausias ir greičiausias būdas sudaryti paskolos sutartį – prisijungti prie
                Savitarnos.</p>
              <p>Sutartį taip pat sudarysite paskambinę telefonu <b><a
                    href="tel:+370 700 55 888">+370
                    700 55 888</a></b>, užsukę į <a
                  href="https://www.post.lt/lt/pagalba/pasto-skyriai-dezutes" target="_blank"
                  rel="noopener noreferrer">„PayPost” skyrių</a> arba MOKILIZINGAS
                atstovybėse.</p>
              <a href="loan-types" class="faq__cta btn btn--black">
                <span class="btn__text">Paskolos</span>
              </a>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash03">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kodėl negaliu gauti paskolos?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash04">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Prašymų formos</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a>
              </p>
            </div>
          </div>
        </div>
        <div class="default__actions">
          <a href="faq" class="default__action btn btn--blue">
            <span class="btn__text">Pagalba</span>
          </a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--plain default__section--grey">
    <div class="default__container container">
      <h3>SĄLYGOS</h3>
      <div class="default__text">
        <p>Pavyzdžiui, skolinantis 300 €, kai sutartis sudaroma 36 mėn. terminui, metinė palūkanų
          norma – 6,50 %,
          mėnesio sutarties mokestis – 0,50 %, BVKKMN – 18,40 %, bendra mokėtina suma – 385,03 €,
          mėnesio įmoka – 10,69
          €. </p>
        <p>Atsižvelgiant į lizingo bendrovės atliktą Jūsų kreditingumo ir rizikos vertinimą ar
          pasirinkus kitokį
          sutarties terminą bei įmokų mokėjimo dieną, Jums gali būti pasiūlytos ir kitokios sąlygos
          (pavyzdžiui,
          metinės palūkanų normos arba mėnesio sutarties mokesčio dydis).</p>
      </div>
    </div>
  </section>
  <section class="default__section default__section--header default__section--blue">
    <div class="default__container container">
      <div class="default__header default__header--business">
        <h1>Verslo partneriams</h1>
        <div class="default__intro default__intro--business">
          <p>Su savo partneriais siekiame bendrų tikslų ir džiaugiamės puikiais rezultatais.
            Dirbame tam, kad kartu su jais suteiktume klientams patirtį, kurią jie norėtų kartoti.
          </p>
        </div>
        <div class="default__media default__media--business media">
          <i class="media__image" style="background-image:url('../media/business__media.png')"></i>
        </div>
        <div class="default__actions">
          <a href="" class="default__action btn btn--white-blue">Tapkite partneriu</a>
        </div>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php';
