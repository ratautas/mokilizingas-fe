<?php include '../partials/head--user.php'; ?>
<main class="app__page app__page--user user" data-page>
  <div class="user__container container">
    <div class="user__top">
      <h3>PRISIJUNGTI</h3>
      <div class="user__already already">
        <div class="already__label">Esate naujokas?</div>
        <a href="registration" class="already__trigger">Susikurkite paskyrą</a>
      </div>
    </div>
    <div class="user__content">
      <div class="user__tabs tabs" data-tabs>
        <div class="tabs__triggers tabs__triggers--user">
          <div class="tabs__trigger tabs__trigger--user is-active" tabindex="0"
            data-tabs-trigger="email">El. paštu
            <i class="tabs__underline"></i>
          </div>
          <div class="tabs__trigger tabs__trigger--user" tabindex="0" data-tabs-trigger="signature">
            M. parašu
            <!-- <span class="tabs__tag tag">Patariame</span> -->
          </div>
          <div class="tabs__trigger tabs__trigger--user" tabindex="0" data-tabs-trigger="smartid">
            Smart ID</div>
        </div>
        <div class="tabs__contents tabs__contents--user">
          <div class="tabs__content is-active" data-tabs-content="email">
            <form class="tabs__form form custom-submit" data-user-form
              action="../endpoints/user.php">
              <div class="form__content form__content--login" data-form-content>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">El.paštas / vartotojo vardas</span>
                    <input type="text" name="login-email" required class="form__input">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">Slaptažodis</span>
                    <input type="password" name="login-password" required class="form__input">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <a href="remind" class="form__trigger">Pamiršai slaptažodį?</a>
                  </div>
                </div>
                <div class="form__footer form__footer--user">
                  <div class="form__row">
                    <div class="form__item">
                      <button class="form__submit btn btn--full">
                        <span class="btn__text">Prisijungti</span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="tabs__content" data-tabs-content="signature">
            <form class="tabs__form form custom-submit" data-user-form
              action="../endpoints/user.php">
              <div class="form__content form__content--login" data-form-content>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">Asmens kodas</span>
                    <input type="text" name="login-code" pattern="^.[0-9]{10,10}$" required
                      class="form__input">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">Telefono numeris</span>
                    <input type="tel" name="login-tel" required class="form__input">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__footer form__footer--user">
                  <div class="form__row">
                    <div class="form__item">
                      <button class="form__submit btn">
                        <span class="btn__text">Prisijungti</span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="tabs__content" data-tabs-content="smartid">
            <form class="tabs__form form custom-submit" data-user-form
              action="../endpoints/user.php">
              <div class="form__content form__content--login" data-form-content>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">Smart ID kodas</span>
                    <input type="number" inputmode="decimal" pattern="[0-9]*" name="login-tel" required class="form__input">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__footer form__footer--user">
                  <div class="form__row">
                    <div class="form__item">
                      <button class="form__submit btn">
                        <span class="btn__text">Prisijungti</span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include '../partials/foot--user.php';
