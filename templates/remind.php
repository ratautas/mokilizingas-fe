<?php include '../partials/head--user.php'; ?>
<main class="app__page app__page--user user" data-page>
  <div class="user__container container">
    <div class="user__top">
      <h3>PRIMINTI SLAPTAŽODĮ</h3>
    </div>
    <div class="user__content">
      <form class="user__form form" action="http://localhost/mokilizingas-fe/endpoints/remind.php">
        <div class="form__content form__content--remind" data-form-content>
          <div class="form__row">
            <div class="form__item">
              <span class="form__label">Jūsų el. paštas*</span>
              <input type="email" name="remoind-email" required class="form__input">
              <span class="form__error">Error</span>
            </div>
          </div>
          <div class="form__row">
            <div class="form__item">
              <button class="form__submit btn btn--full">
                <span class="btn__text">Priminti</span>
              </button>
            </div>
          </div>
        </div>
        <div class="form__done form__done--success done done--success" data-form-done="success">
          <i class="done__close" data-done-close></i>
          <div class="done__top">
            <h2>Ačiū.</h2>
          </div>
          <div class="done__text">
            <p>Su jumis susisieks mūsų konsultantė</p>
          </div>
        </div>
        <div class="form__done form__done--error done done--error" data-form-done="error">
          <i class="done__close" data-done-close></i>
          <div class="done__top">
            <h2>Klaida!</h2>
          </div>
          <div class="done__text">
            <p>Su jumis susisieks mūsų konsultantė</p>
          </div>
        </div>
      </form>
    </div>
  </div>
  <?php include '../partials/foot--user.php'; ?>
