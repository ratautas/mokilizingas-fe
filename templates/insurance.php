<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--rose">
    <div class="default__container container">
      <div class="default__socials socials">
        <?php include '../partials/socials.php'; ?>
      </div>
      <div class="default__header">
        <h1>Pirkinių draudimas</h1>
        <div class="default__intro default__intro--push-down">
          <p>Lizingu įsigijote mobilųjį telefoną, planšetę ar kompiuterį? Apsidrauskite pirkinius – mes nuleidome
            draudimo kainą, kad ji būtų viena mažiausių rinkoje.</p>
        </div>
      </div>
  </section>
  <section class="default__section default__section--columns default__section--rose">
    <div class="default__container container">
      <div class="default__columns">
        <div class="default__column">
          <div class="default__block">
            <h4>KOKIUS PIRKINIUS GALIU APDRAUSTI?</h4>
            <div class="default__text">
              <p>Drausti galite visus MOKILIZINGU įsigytus pirkinius: buitinę techniką, vaizdo bei garso aparatūrą,
                kompiuterius, telefonus, dviračius, baldus ir kt.</p>
            </div>
            <div class="default__listing listing">
              <div class="listing__title">Draudimas negalioja tik šiems pirkiniams:</div>
              <div class="listing__list">
                <div class="listing__item">Transporto priemonėms (išskyrus dviračius, irklines valtis, kanojas, jėgos
                  aitvarus ir burlentes) ir jų dalims;būsto apdailos prekėms (grindų danga, durys, keramikinės plytelės
                  ir pan.);</div>
                <div class="listing__item">Pašto ženklams, monetoms, antikvariniams daiktams, meno kūriniams ar
                  kolekcijoms;</div>
                <div class="listing__item">Rekonstruotiems, suremontuotiems arba atgamintiems pirkiniams;</div>
                <div class="listing__item">Pirkiniams, įsigytiems kaip naudoti daiktai;</div>
                <div class="listing__item">Augalams ir gyvūnams;</div>
                <div class="listing__item">Prekių klastotėms;</div>
                <div class="listing__item">Daiktams, kurių apyvarta Lietuvoje yra draudžiama arba ribojama.</div>
              </div>
            </div>
          </div>
        </div>
        <div class="default__column default__column--push-left">
          <div class="default__block">
            <h4>KOKIUS PIRKINIUS GALIU APDRAUSTI?</h4>
            <div class="default__text">
              <p>Pirkiniai draudžiami nuo vagystės, užpylimo, gaisro, perdegimo dėl elektros įtampos svyravimų,
                netyčinio sudaužymo ir kt. netikėtumų, išskyrus tuos atvejus, kurie pirkinių draudimo sąlygose
                išvardinti kaip nedraudžiami. Jūsų pirkinių draudimas galios visose ES šalyse.</p>
            </div>
          </div>
          <div class="default__block">
            <h4>KĄ DARYTI, JEI ĮVYKSTA NETIKĖTUMAS,
              NUO KURIO ESATE APSIDRAUDĘS?</h4>
            <div class="default__text">
              <p>Per 5 kalendorines dienas kreipkitės į draudimo bendrovę „If“ ir praneškite apie patirtą žalą telefonu
                1620, el. paštu zalos@if.lt arba internetu <a href="www.if.lt">www.if.lt</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="default__columns">
        <div class="default__column">
          <div class="default__block">
            <h4>KADA ŽALA NEKOMPENSUOJAMA?</h4>
            <div class="default__listing listing">
              <div class="listing__title">Draudimas negalioja šiais atvejais:</div>
              <div class="listing__list">
                <div class="listing__item">Jei pirkinį naudojote ne pagal savo paskirtį ar nesilaikote gamintojo
                  instrukcijų;</div>
                <div class="listing__item">Jei pirkinį sugadinote jį skalbdami, lygindami, valydami, perdarydami,
                  montuodami ar kitaip modifikuodami ar taisydami;</div>
                <div class="listing__item">Jei pirkinys dingo (pvz., pametėte ar palikote be priežiūros);</div>
                <div class="listing__item">Jei pirkinys buvo pavogtas iš neužrakinto automobilio ar neužrakintų
                  patalpų, nebaigtų statyti statinių;</div>
                <div class="listing__item">Jei nuostolį patyrėte dėl natūralaus pirkinio nusidėvėjimo (pvz., kai
                  pirkinio dalys susidėvėjo nuo ilgalaikio naudojimo: atšipo trintuvo peilis, sudilo skalbyklės guolis
                  ir kt.);</div>
                <div class="listing__item">Jei už pirkinio netinkamą veikimą atsakingas gamintojas (tai atvejai,
                  kuriems galioja garantija);</div>
                <div class="listing__item">Jei pirkinys sugadintas dėl Jūsų ar Jūsų šeimos narių tyčinių veiksmų.</div>
                <div class="listing__item">Jei žala pasireiškia kaip pirkinio estetinio vaizdo sugadinimas (ištepimas,
                  įbrėžimas, įlinkimas, smulkus skilimas ir kt.), jei tai netrukdo pirkinio naudoti pagal tiesioginę
                  paskirtį.</div>
                <div class="listing__item">Jei žala sporto, žūklės, medžioklės ir bepiločiams įrenginiams pasireiškia
                  kaip dūžis, lūžis, skilimas, gedimas, perdegimas ar praradimas (išskyrus vagystę).
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="default__column default__column--push-left">
          <div class="default__block">
            <h4>KOKIA SUMA BUS APDRAUSTI MANO PIRKINIAI?</h4>
            <div class="default__text">
              <p>Kiek sumokėjote už prekę, tokia suma ją ir apdrausime. Visais atvejais yra taikoma 20% nuo nuostolių
                sumos, bet ne mažiau nei 50 Eur besąlyginė išskaita (frančizė) – tai suma, kurios draudėjas nedengia.
                Žalos atveju Jums bus atlyginama visa suma, viršijanti šią išskaitą.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <section class="default__section default__section--plain default__section--grey">
    <div class="default__container container">
      <h4>SĄLYGOS</h4>
      <div class="default__text">
        <p>Viršuje pateiktos mūsų pirkinių draudimo sąlygos negali būti laikomos teisinę galią turinčiu dokumentu. Su
          visomis pirkinių draudimo sąlygomis susipažinti galite čia.</p>
        <p>Jeigu lizingo sutartį sudarėte ir savo pirkinius apdraudėte iki 2016-12-5 dienos, Jūsų pirkiniams galioja
          kitos draudimo sąlygos. Su jomis galite susipažinti čia.</p>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php'; ?>
