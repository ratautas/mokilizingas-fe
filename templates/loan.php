<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--orange">
    <div class="default__container default__container--with-wizard container">
      <div class="default__header default__header--with-wizard default__header--high">
        <h1>Paskola automobiliui</h1>
        <div class="default__tag">Palūkanos tik nuo 4,9 %</div>
        <div class="default__media default__media--high media">
          <i class="media__image" style="background-image:url('../media/loan__media--2.png')"></i>
        </div>
        <div class="default__intro default__intro--narrow">
          <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti seną
            automobilį?
            Pasinaudokite MOKILIZINGO paskola automobiliui – dabar siūlome ypač geras sąlygas.
            Nepamirškite įvertinti
            visų būsimų išlaidų, įskaitant automobilio remontą, draudimą ir sezoninį padangų
            keitimą.</p>
        </div>
        <div class="default__listing default__listing--head listing">
          <div class="listing__list listing__list--head">
            <div class="listing__item">Nėra sutarties sudarymo mokesčio</div>
            <div class="listing__item">Palūkanos tik nuo 4,9 %</div>
            <div class="listing__item">Suma nuo 500 iki 15 000 Eur</div>
            <div class="listing__item">12 – 72 mėn. laikotarpiui</div>
            <div class="listing__item">Nereikia pradinio įnašo ir Kasko draudimo</div>
            <div class="listing__item">Atsakymas dėl paskolos per kelias minutes, o pinigai Jus
              pasieks per 30 min.
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="default__wizard wizard">
    <?php include '../partials/wizard--v1.php'; ?>
    <div class="wizard__socials socials">
      <?php include '../partials/socials.php'; ?>
    </div>
  </div>
  <section class="default__section default__section--methods">
    <div class="default__container container">
      <h4>TAIP PAT GALITE GAUTI PASKOLĄ</h4>
      <div class="default__methods">
        <div class="default__method method">
          <div class="method__content">
            <div class="method__title">Paypost</div>
            <div class="method__description">
              <p>Mūsų paskolą taip pat gausite užėję į artimiausią PAYPOST skyrių visoje Lietuvoje.</p>
            </div>
            <i class="method__provider" style="background-image:url('../media/paypost.png')"></i>
          </div>
        </div>
        <div class="default__method method">
          <div class="method__content">
            <div class="method__title">Telefonu</div>
            <div class="method__description">
              <p>O jei nenorite niekur eiti ar po ranka neturite kompiuterio –
                skambinkite mums telefonu +370 700 55 888!</p>
            </div>
            <form class="method__form form" action="http://localhost/mokilizingas-fe/endpoints/contact.php">
              <div class="form__content" data-form-content>
                <div class="form__row">
                  <div class="form__item">
                    <span class="form__label">Arba įveskite savo telefo numerį</span>
                    <input type="tel" name="tel" minlength="8" required class="form__input form__input--white">
                    <span class="form__error">Error</span>
                  </div>
                </div>
                <div class="form__row">
                  <div class="form__item">
                    <button class="form__submit btn">
                      <i class="btn__icon btn__icon--left">
                        <?php include '../assets/img/icon--phone.svg'; ?>
                      </i>
                      <span class="btn__text">Gauti skambutį</span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="form__done form__done--success done done--success" data-form-done="success">
                <i class="done__close" data-done-close></i>
                <div class="done__top">
                  <h2>Ačiū.</h2>
                </div>
                <div class="done__text">
                  <p>Su jumis susisieks mūsų konsultantė</p>
                </div>
              </div>
              <div class="form__done form__done--error done done--error" data-form-done="error">
                <i class="done__close" data-done-close></i>
                <div class="done__top">
                  <h2>Klaida!</h2>
                </div>
                <div class="done__text">
                  <p>Su jumis susisieks mūsų konsultantė</p>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--feedback">
    <div class="default__container container">
      <div class="default__feedback feedback">
        <h2>Atsiliepimai</h2>
        <div class="feedback__intro">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et tristique ipsum,
            vitae malesuada diam. Pellentesque habitant morbi tristique senectus et netus et
            malesuada fames ac turpis egestas. Sed laoreet sit</p>
        </div>
        <div class="feedback__reviews" data-reviews-masonry data-load-more-append>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item" data-reviews-item="visible">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--1.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta lizingu, iškilo problemų su pardavėju
                (kuris ne tik kad nenusimano ir nežino elementarių grąžinimo sąlygų vykdomų kartu
                su partneriais, bet ir palieka klientą likimo valiai). Tuo tarpu Mokilizingas
                pakonsultavo, paaiškino ir tarpininkavo problemos sprendimo metu ir
                supažindino pardavėją su jo </div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--3.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>
              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei (brokas), kuri buvo pirkta</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į klientą.
                Sugedus prekei
                (brokas),
                kuri buvo pirkta lizingu, iškilo problemų su pardavėju (kuris ne tik</div>
            </div>
          </div>
          <div class="feedback__item is-hidden" data-reviews-item="hidden">
            <div class="feedback__review review">
              <div class="review__top">
                <div class="review__image" style="background-image:url('../media/review__image--2.png')"></div>
                <div class="review__name">Paulius Povilas</div>
                <div class="review__stars">
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star review__star--filled">
                    <?php include '../assets/img/icon--star--filled.svg'; ?>
                  </i>
                  <i class="review__star">
                    <?php include '../assets/img/icon--star.svg'; ?>
                  </i>
                </div>

              </div>
              <div class="review__text">Esu patenkinta šios įmonės darbu ir požiūriu į</div>
            </div>
          </div>
        </div>
        <div class="feedback__more" data-reviews-reveal>
          <div class="feedback__more-label">
            Rodyti daugiau
          </div>
          <div class="feedback__more-dots">
            <i class="feedback__dot feedback__dot--1"></i>
            <i class="feedback__dot feedback__dot--2"></i>
            <i class="feedback__dot feedback__dot--3"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--partners">
    <div class="default__container container">
      <h3>MŪSŲ PARTNERIAI</h3>
      <div class="default__partners partners">
        <script>
          var regions = [{
              label: 'Vilnius',
              value: 'vilnius',
              customProperties: {
                zoom: 10,
                lng: 25.36,
                lat: 54.76,
              },
            },
            {
              label: 'Kaunas',
              value: 'kaunas',
              customProperties: {
                zoom: 10,
                lng: 23.85,
                lat: 54.9,
              }
            }, {
              label: 'Klaipėda',
              value: 'klaipeda',
              customProperties: {
                zoom: 10,
                lng: 21,
                lat: 55.7,
              }
            }, {
              label: 'Šiauliai',
              value: 'siauliai',
              customProperties: {
                zoom: 10,
                lng: 25.26,
                lat: 54.7,
              }
            }, {
              label: 'Panevėžys',
              value: 'panevezys',
              customProperties: {
                zoom: 10,
                lng: 25.26,
                lat: 54.7,
              }
            }
          ];
          var partners = {
            type: 'FeatureCollection',
            features: [{
                geometry: {
                  type: 'Point',
                  coordinates: [25.32, 54.72],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              }, {
                geometry: {
                  type: 'Point',
                  coordinates: [25.34, 54.74],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              }, {
                geometry: {
                  type: 'Point',
                  coordinates: [25.36, 54.76],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              }, {
                geometry: {
                  type: 'Point',
                  coordinates: [25.38, 54.78],
                },
                type: 'Feature',
                properties: {
                  region: 'vilnius',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Vilnius',
                },
              },
              {
                geometry: {
                  type: 'Point',
                  coordinates: [21.1, 55.7],
                },
                type: 'Feature',
                properties: {
                  region: 'klaipeda',
                  description: "Modern twists on classic pastries. We're part of a larger chain.",
                  name: 'Klaipeda',
                },
              },
              {
                geometry: {
                  type: 'Point',
                  coordinates: [23.3, 55.9],
                },
                type: 'Feature',
                properties: {
                  region: 'siauliai',
                  description: 'Come and try our award-winning cakes and pastries.',
                  name: 'Siauliai',
                },
              },
            ],
          };

        </script>
        <div class="partners__container">
          <div class="partners__content">
            <div class="partners__map" data-partners-map></div>
            <div class="partners__actions">
              <select class="partners__select" data-partners-select></select>
              <div class="partners__finder" data-partners-locate>Arčiausiai manęs</div>
            </div>
          </div>
          <div class="partners__logos">
            <div class="partners__logo">
              <img src="../media/logo--senukai.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--berry.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--egvile.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--senukai.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--berry.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
            <div class="partners__logo">
              <img src="../media/logo--egvile.png" alt="" class="partners__image">
              <a href="" class="partners__link"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--faq">
    <div class="default__container container">
      <div class="default__faq default__faq--loan faq" data-toggler>
        <h3>PAGALBA</h3>
        <div class="faq__list">
          <div class="faq__item" data-toggler-item="hash01">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip sumokėti lizingu?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
            </div>
          </div>
          <div class="faq__item is-open" data-toggler-item="hash05">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip pirkti lizingu?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash02">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kaip gauti paskolą?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash03">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Kodėl negaliu gauti paskolos?</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
            </div>
          </div>
          <div class="faq__item" data-toggler-item="hash04">
            <div class="faq__question" data-toggler-trigger>
              <div class="faq__label">Prašymų formos</div>
              <i class="faq__icon"></i>
            </div>
            <div class="faq__answer" data-toggler-content>
              <p>Dėl informacijos apie Mokipay el. pinigų sąskaitas ir pinigų išpirkimą
                kreipkitės el. paštu: <a href="mailto:info@mokilizingas.lt">info@mokilizingas.lt</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--services">
    <div class="default__container container">
      <h3>KITOS PASLAUGOS</h3>
      <div class="default__cards">
        <div class="default__card card card--half">
          <div class="card__media card__media--half media">
            <i class="media__image media__image--cover" style="background-image:url('../media/loan__media--5.png')"></i>
          </div>
          <div class="card__container card__container--half">
            <div class="card__content">
              <div class="card__title">Paskola automobiliui</div>
            </div>
            <div class="card__text">
              <p>Jei kaskart iš taško A į tašką B vykstate sukandę dantis, galbūt metas keisti seną
                automobilį?</p>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
          <a href="loan" class="card__link"></a>
        </div>
        <div class="default__card card card--half">
          <div class="card__media card__media--half media">
            <i class="media__image media__image--cover" style="background-image:url('../media/loan__media--6.png')"></i>
          </div>
          <div class="card__container card__container--half">
            <div class="card__content">
              <div class="card__title">Paskola būsto remontui</div>
            </div>
            <div class="card__text">
              <p>Norite atnaujinti namus, tačiau finansinės galimybės leidžia pasirūpinti tik vienu
                kambariu?</p>
            </div>
            <div class="card__bottom">
              <div class="card__tag">Ypatingai geros sąlygos</div>
              <a href="loan" class="card__trigger">Plačiau</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--plain default__section--grey">
    <div class="default__container container">
      <h3>SĄLYGOS</h3>
      <div class="default__text">
        <p>Pavyzdžiui, skolinantis 300 €, kai sutartis sudaroma 36 mėn. terminui, metinė palūkanų
          norma – 6,50 %,
          mėnesio sutarties mokestis – 0,50 %, BVKKMN – 18,40 %, bendra mokėtina suma – 385,03 €,
          mėnesio įmoka – 10,69
          €. </p>
        <p>Atsižvelgiant į lizingo bendrovės atliktą Jūsų kreditingumo ir rizikos vertinimą ar
          pasirinkus kitokį
          sutarties terminą bei įmokų mokėjimo dieną, Jums gali būti pasiūlytos ir kitokios sąlygos
          (pavyzdžiui,
          metinės palūkanų normos arba mėnesio sutarties mokesčio dydis).</p>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php'; ?>
