<?php include '../partials/head.php'; ?>
<main class="app__page app__page--default default" data-page>
  <section class="default__section default__section--header default__section--blue">
    <div class="default__container container">
      <div class="default__socials socials">
        <?php include '../partials/socials.php'; ?>
      </div>
      <div class="default__header default__header--high">
        <h1>Kortelė</h1>
        <div class="default__media default__media--high media">
          <i class="media__image" style="background-image:url('../media/card.png')"></i>
        </div>
        <div class="default__intro default__intro--narrow">
          <p>Tai kredito kortelė, su kuria patogiai valdysite savo asmeninius finansus. Įvertinę
            Jūsų kreditingumą, suteiksime vartojimo kredito limitą nuo 200 iki 5000 Eur, kurį
            panaudojus ir grąžinus iki kito mėn. pabaigos nereikės mokėti jokių palūkanų!</p>
        </div>
        <div class="default__actions default__actions--pull-down">
          <a href="" class="default__action btn btn--white-blue">
            <i class="btn__icon btn__icon--left">
              <?php include '../assets/img/icon--card.svg'; ?></i>
            <span class="btn__text">Užsisakyti kortelę</span>
          </a>
          <a class="default__action" href="tel:+370 700 55 888">
            <?php include '../assets/img/icon--phone.svg'; ?>&nbsp;&nbsp;<b>UŽSISAKYTI
              TELEFONU</b>&nbsp;&nbsp;+370 700 55 888</a>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section default__section--benefits">
    <div class="default__container container">
      <div class="default__columns default__columns--low">
        <div class="default__column">
          <div class="default__block">
            <h3>KODĖL VERTA ĮSIGYTI?</h3>
            <div class="default__text default__text--low">
              <p>MOKILIZINGO kortelė nieko nekainuos, jeigu ja nesinaudosite!</p>
              <ul>
                <li><b>LAISVĖ NUSPRĘSTI.</b><br />
                  Pirkiniais galėsite mėgautis iškart, o atsiskaityti arba iš karto, arba sumą
                  išskaidę dalimis. Periodas, per kurį neskaičiuojame palūkanų – net iki 60 dienų.
                </li>
                <li><b>LAISVĖ PATOGUMUI. </b><br />
                  Visada žinosite savo kredito limitą ir jį galėsite panaudoti skirtingose
                  situacijose, nes MOKILIZINGO kortele naudositės kaip įprasta mokėjimo kortele:
                  Lietuvoje, užsienyje, internetu – visur.</li>
                <li><b>LAISVĖ PLANUOTI. </b><br />
                  www.mokilizingas.lt/savitarna matysite savo pirkimo istoriją, galėsite didinti
                  įmoką ir pagal savo kreditingumą keisti kortelės kredito limitą.</li>
                <li><b>LAISVĖ GREIČIUI.</b><br />
                  Į kortelę įdiegta NFC (bekontaktė) funkcija, kuri leidžia už pirkinius
                  atsiskaityti greičiau ir paprasčiau.</li>
              </ul>

            </div>
          </div>
        </div>
        <div class="default__column">
          <div class="default__block">
            <h3>ĮVAIRIOS ATSISKAITYMO GALIMYBĖS</h3>
            <div class="default__text default__text--low">
              <p><b>Jei įsigiję kortelę ja nesinaudosite, ji Jums nieko nekainuos.</b> Naudodamiesi
                kortele, galite pasirinkti Jums tinkamiausią atsiskaitymo būdą:</p>
              <p><b>1.</b> Visiškai padengti išnaudotą kredito limitą iki kito mėn. paskutinės
                dienos ir
                nemokėti jokių palūkanų. Taikomas tik limito administravimo mokestis – 1 Eur/mėn.
              </p>
              <p><b>2.</b> Kortelės galiojimo laikotarpiu mokėti minimalias mėnesio įmokas, kurias
                sudaro:
              </p>
              <p>
                <ul>
                  <li>4,2 % gavėjo panaudotos vartojimo kredito sumos, bet ne mažiau kaip 5 Eur;
                  </li>
                  <li>Palūkanos, skaičiuojamos kiekvieną dieną nuo gavėjo panaudotos vartojimo
                    kredito sumos. Metinė palūkanų norma – 19,9 %;</li>
                  <li>Limito administravimo mokestis – 1 Eur/mėn.</li>
                </ul>
              </p>
              <p>Pasibaigus kortelės limito galiojimo terminui likusią panaudotą negrąžintą paskolos
                sumą (jei tokia yra) grąžinsite per 24 mėn. laikotarpį mokėdami lygiomis dalimis su
                palūkanomis.</p>
              <p><b>3.</b> Panaudotą kredito limito sumą galite dengti bet kokio dydžio suma – nuo
                minimalios įmokos iki padengto viso kredito.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="default__section">
    <div class="default__container container">
      <div class="default__columns default__columns--low">
        <div class="default__column">
          <div class="default__block">
            <h3>JOKIŲ NEPLANUOTŲ IŠLAIDŲ</h3>
            <div class="default__text default__text--low">
              <p>MOKILIZINGO kortelė nieko nekainuos, jeigu ja nesinaudosite!</p>
            </div>
          </div>
        </div>
        <div class="default__column">
          <!--leave this as a spacer -->
        </div>
        <div class="default__column">
          <div class="default__block">
            <div class="default__text default__text--low">
              <p><b>NEMOKAMOS PASLAUGOS:</b></p>
              <ul>
                <li>Kortelės išdavimas, pristatymas ir atsisakymas;</li>
                <li>Atsiskaitymai Lietuvoje ir užsienyje;</li>
                <li>Bekontakčiai atsiskaitymai;</li>
                <li>Limito keitimas.</li>
                <li>Saugaus atsiskaitymo internete paslauga. Nauja! Plačiau skaitykite čia.</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="default__column">
          <div class="default__block">
            <div class="default__text default__text--low">
              <p><b>MOKAMOS PASLAUGOS:</b></p>
              <ul>
                <li>Metinė palūkanų norma 19,9 %;</li>
                <li>Grynųjų pinigų išdavimo mokestis: 6 % + 1 Eur;</li>
                <li>Panaudoto limito administravimo mokestis – 1 Eur/mėn.;</li>
                <li>Kortelės keitimo mokestis – 10 Eur</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="default__text default__text--low">
        <p>Pateiksime pavyzdį. Tarkime, išnaudojus 1000 Eur kredito limitą, visą panaudotą sumą
          padengsite iki kito mėnesio paskutinės dienos – palūkanų neskaičiuosime, limito
          administravimo mokestis – 1 Eur/mėn., BVKKMN – 1,63 %, bendra mokėtina suma – 1002 Eur.
          Kitu atveju išnaudojus 1000 Eur kredito limitą, per mėnesį mokant įmoką, pirma įmoka bus
          59,58 Eur, atsiskaitysite per 24 mėnesius. Sekančios įmokos mažės, kadangi liks padengti
          vis mažesnė panaudoto kredito dalis, nuo kurio skaičiuojamos palūkanos, paskutinė įmoka
          bus – 35,56 Eur. Metinė palūkanų norma bus 19,9 %, limito administravimo mokestis – 1
          Eur/mėn., BVKKMN – 24,44 %, bendra mokėtina suma – 1229,76 Eur. Finansavimo sąlygos gali
          pasikeisti, kai atliksime Jūsų kreditingumo vertinimą.</p>
      </div>
    </div>
  </section>
  <section class="default__section default__section--conditions">
    <div class="default__container container">
      <div class="default__columns default__columns--low">
        <div class="default__column">
          <div class="default__block">
            <h3>PAPRASTOS KREDITO GALIOJIMO SĄLYGOS</h3>
            <div class="default__text default__text--low">
              <p>Kortelė galioja 3 metus. Pasibaigus šiam terminui, kortelę ir jos limitą galite
                atsinaujinti arba grąžinti likusią panaudotą ir negrąžintą kredito sumą kartu su
                palūkanomis bei kitais mokesčiais per 24 mėnesius.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="default__actions default__actions--bottom">
        <a href="" class="default__action btn btn--blue">
          <i class="btn__icon btn__icon--left">
            <?php include '../assets/img/icon--card.svg'; ?></i>
          <span class="btn__text">Užsisakyti kortelę</span>
        </a>
        <a class="default__action" href="tel:+370 700 55 888">
          <?php include '../assets/img/icon--phone.svg'; ?>&nbsp;&nbsp;<b>UŽSISAKYTI
            TELEFONU</b>&nbsp;&nbsp;+370 700 55 888</a>
      </div>
    </div>
  </section>
  <?php include '../partials/foot.php';
