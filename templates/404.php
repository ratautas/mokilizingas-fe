<?php include '../partials/head.php'; ?>
<main class="app__page app__page--notfound notfound" data-page>
  <div class="notfound__container container">
    <h1>Sed convallis, neque id vulputate rutrum, tellus diam dapibus lectus</h1>
    <div class="notfound__subtitle">Kodas: 404</div>
    <div class="notfound__footer">
      <div class="notfound__label">Naudingos nuorodos, kurios gali padėti:</div>
      <div class="notfound__list">
        <a href="home" class="notfound__link">Titulinis</a>
        <a href="loan" class="notfound__link">Paskola</a>
        <a href="leasing" class="notfound__link">Lizingas</a>
        <a href="faq" class="notfound__link">Pagalba</a>
      </div>
    </div>
  </div>
  <?php include '../partials/foot.php'; ?>
